# External Policy Activation Service

:::info
This service will be replaced by the new Navigate APIs. Please contact us for guidance before integrating using this service.
:::

## Summary

The Policy Activation Service is an XML Service. The fields are self-describing.
The service can be invoked either using a SOAP client (e.g. WCF, Web Services) or a
simple POST HTTP request with the required WS-A addressing header specified. Each call
to the request instantiate can request only one policy section, for policies with
multiple policy sections the service has to be invoked once for each section.

## Documentation endpoint

The best source of information on the service can be obtained by querying the WSDL
endpoint https://integration.autoprotect.net/TCF/RemotePolicyMaintenance.svc?singleWsdl

## Service Endpoint

| Environment | Endpoint                                                                    |
|-------------|-----------------------------------------------------------------------------|
| PROD        | https://integration.autoprotect.net/TCF/RemotePolicyMaintenance.svc         |
| UAT | https://dc-uat-integration.autoprotect.net/TCF/RemotePolicyMaintenance.svc  |
| UAT01 | https://uat-01-integration.autoprotect.net/TCF/RemotePolicyMaintenance.svc  |

Enable WS-A addressing with the following action http://tempuri.org/PostPolicyStringsReduced

## Supporting Documentation

The following information is provided as an addition to the WSDL endpoint. The WSDL endpoint
should be regarded as the master documentation for the service the following is provided
only for the purposes of clarification.

### Methods

The Service supports a single method PostPolicyStringsReduced which requires an XML 
document to be submitted as the payload. An example payload is show below.

## Example XML

### Request

```xml
<soap:Envelope soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
               xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
               xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
               xmlns:xsd="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soap:Body>
        <PostPolicyStringsReduced
                xmlns="http://tempuri.org">
            <strFinancePolicy_YN xsi:type="xsd:string">N</strFinancePolicy_YN>
            <strCustomerDOB xsi:type="xsd:string" />
            <strBankAccountName xsi:type="xsd:string" />
            <strBankAccountNumber xsi:type="xsd:string" />
            <strBankSortCode xsi:type="xsd:string" />
            <strPrefPaymentDate xsi:type="xsd:string" />
            <strUserName xsi:type="xsd:string">tcf12563567</strUserName>
            <strPassword xsi:type="xsd:string">**password**</strPassword>
            <strActivatePolicy_YN xsi:type="xsd:string">Y</strActivatePolicy_YN>
            <strUserReference xsi:type="xsd:string">Jamie Orr</strUserReference>
            <strDealerCode xsi:type="xsd:string">12566574</strDealerCode>
            <strPolPostingType xsi:type="xsd:string">R</strPolPostingType>
            <strPolicyNumber xsi:type="xsd:string" />
            <strCustomerType xsi:type="xsd:string">PRIV</strCustomerType>
            <strVehicleType xsi:type="xsd:string" />
            <strCustomerTitle xsi:type="xsd:string">Miss</strCustomerTitle>
            <strCustomerForename xsi:type="xsd:string">Firstname</strCustomerForename>
            <strCustomerSurname xsi:type="xsd:string">Lastname</strCustomerSurname>
            <strCompanyName xsi:type="xsd:string" />
            <strCustomerAddress1 xsi:type="xsd:string">******</strCustomerAddress1>
            <strCustomerAddress2 xsi:type="xsd:string">.</strCustomerAddress2>
            <strCustomerAddress3 xsi:type="xsd:string">LONDON,Greater London</strCustomerAddress3>
            <strCustomerAddress4 xsi:type="xsd:string" />
            <strCustomerAddress5 xsi:type="xsd:string" />
            <strCustomerPostCode xsi:type="xsd:string">N5 2JS</strCustomerPostCode>
            <strCustomerCountryCode xsi:type="xsd:string">GB</strCustomerCountryCode>
            <strCustomerTelephone xsi:type="xsd:string">*********</strCustomerTelephone>
            <strCustomerWorkTelephone xsi:type="xsd:string" />
            <strCustomerMobileTelephone xsi:type="xsd:string" />
            <strCustomerEmailAddress xsi:type="xsd:string">email@email.com</strCustomerEmailAddress>
            <strEuroTax xsi:type="xsd:string" />
            <strVehicleMake xsi:type="xsd:string">AUDI</strVehicleMake>
            <strVehicleModel xsi:type="xsd:string">a1</strVehicleModel>
            <strVehicleModelDesc xsi:type="xsd:string">Sportback</strVehicleModelDesc>
            <strVehicleModelCode xsi:type="xsd:string" />
            <strVehicleAge xsi:type="xsd:string">0</strVehicleAge>
            <strVehicleAgeInMonths xsi:type="xsd:string">0</strVehicleAgeInMonths>
            <strVehicleReg xsi:type="xsd:string">KE15XPN</strVehicleReg>
            <strVehicleRegDate xsi:type="xsd:string">23/06/2015</strVehicleRegDate>
            <strVehicleMileage xsi:type="xsd:string">5</strVehicleMileage>
            <strPurchasePrice xsi:type="xsd:string">22651</strPurchasePrice>
            <strVehicleEngineSize xsi:nil="true" xsi:type="xsd:string" />
            <strVehicleTransmission xsi:type="xsd:string" />
            <strVehicleFuelType xsi:type="xsd:string" />
            <strChassisNumber xsi:type="xsd:string" />
            <strPrivate_Plate_YN xsi:type="xsd:string">N</strPrivate_Plate_YN>
            <strLengthOfCover xsi:type="xsd:string">99</strLengthOfCover>
            <strCommencementDate xsi:type="xsd:string">24/06/2015</strCommencementDate>
            <strExpiryDate xsi:type="xsd:string" />
            <strPurchaseDate xsi:type="xsd:string">24/06/2015</strPurchaseDate>
            <strSellingPrice xsi:type="xsd:string">300.00</strSellingPrice>
            <strDealerPrice xsi:type="xsd:string" />
            <strEntryDate xsi:type="xsd:string" />
            <strPolicyStatus xsi:type="xsd:string" />
            <strProductDescription xsi:type="xsd:string" />
            <strProductTypeCode xsi:type="xsd:string">CHG</strProductTypeCode>
            <strTransactionDesc xsi:type="xsd:string" />
            <strOriginalPolicy xsi:type="xsd:string" />
            <strComment xsi:type="xsd:string" />
            <strInsurerCode xsi:type="xsd:string" />
            <strExcessValue xsi:type="xsd:string" />
            <strClaimLimitValue xsi:type="xsd:string" />
            <strFinance_YN xsi:nil="true" xsi:type="xsd:string" />
            <strFinanceCompanyName xsi:type="xsd:string" />
            <strStatusCode xsi:type="xsd:string" />
            <strCoverType xsi:type="xsd:string" />
            <strAddOnTurbo xsi:type="xsd:string">N</strAddOnTurbo>
            <strAddonCAT xsi:type="xsd:string">N</strAddonCAT>
            <strAddonABS xsi:type="xsd:string">N</strAddonABS>
            <strAddonAWD xsi:type="xsd:string">N</strAddonAWD>
            <strCoverCode xsi:type="xsd:string">10002142</strCoverCode>
            <strProductInfoHandOverDateTime xsi:type="xsd:string">10/06/2016</strProductInfoHandOverDateTime>
            <strOptinDate xsi:type="xsd:string">11/06/2016</strOptinDate >
        </PostPolicyStringsReduced>
    </soap:Body>
</soap:Envelope>
```

### Response

```xml
<s:Envelope
        xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
    <s:Body
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <PostPolicyStringsReducedResponse
                xmlns="http://tempuri.org">
            <PostPolicyStringsReducedResult>
                <Status>Success</Status>
                <PolicyDocURL>https://integration.autoprotect.net/Document/Download/************************************/Policy Pack.pdf</PolicyDocURL>
                <PolicyNumber>10196645</PolicyNumber>
            </PostPolicyStringsReducedResult>
        </PostPolicyStringsReducedResponse>
    </s:Body>
</s:Envelope>
```

## Key fields

For further clarification the following fields are explained in more detail.

| Field                       | Usage                                                                                                                                                                                         |
|-----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `strUserName` `strPassword` | The dealer's autoprotect user credentials                                                                                                                                                     |
| `strActivatePolicy_YN`       | if the policy has multiple sections, will be 'Y' on the last policy section received for the policy. In quoted state the policies are matched by quoted status, VRN and strPolicyNumber field |
| `strFinancePolicy_YN`        | Y for premium funding policy, strCustomerDOB, strBankAccountName, strBankAccountNumber, strBankAccountNumber, strPrefPaymentDate will be required for financed policies                       |
| `strVehicleReg`              | an Experian lookup will be executed for this registration number to resolve the glass model id of the vehicle                                                                                 |
| `strLengthOfCover`           | length of the cover in moths, it has to match the RangeTo value configured in ApAdmin for the product base rate card                                                                          |
| `strClaimLimitValue`         | claim limit in pounds, it has to match the RangeTo value configured in ApAdmin for the product base rate card                                                                                 |
| `strCoverCode`               | the policy type id configured in ApAdmin                                                                                                                                                      |

### Other Fields

All other field are described by their names for example strCustomerSurname is the
Customer Surname. And strChassisNumber is the ChassisNumber.

## Multiple Policy Sections or Covers

For multiple products per policy you have to make multiple SOAP requests to our service
endpoint https://integration.autoprotect.net/TCF/RemotePolicyMaintenance.svc.
`strActivatePolicy_YN` should be set to `Y` on the last product of the policy to indicate
the policy is complete and intimate activation and PDF document generation. This means
intermediary calls to the service, before activating the policy, will not return any
PolicyDocURL value in the result dataset.

Multiple calls for the same VRN and strPolicyNumber will update the customer name and
address with the most recent data received. If a cancelled policy is matched with the
above details, an error will be returned.

### Assumptions

* Requests will be aggregated using the strVehicleReg, strCustomerForename and strCustomerSurname request
* fields

### Example request

```xml
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <PostPolicyStringsReduced xmlns="http://tempuri.org">
            ...
            <strActivatePolicy_YN xsi:type="xsd:string" >Y</strActivatePolicy>
            <!-- Y/N values, Y for flagging the last product of the policy and activation, N for keeping the policy in Quoted state -->
            ...
        </PostPolicyStringsReduced>
    </soap:Body>
</soap:Envelope>
```

### Intermediary Response

```xml
<s:Envelope
        xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
    <s:Body
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <PostPolicyStringsReducedResponse xmlns="http://tempuri.org">
            <PostPolicyStringsReducedResult>
                <Status>Success</Status>
                <PolicyNumber>10196645</PolicyNumber>
                <!-- PolicyDocURL missing or empty value. Will be set only when the strActivatePolicy_YN = Y -->
            </PostPolicyStringsReducedResult>
        </PostPolicyStringsReducedResponse>
    </s:Body>
</s:Envelope>
```

## Premium credit finance

The following fields and assumptions apply to premium credit:

* Request will be sent on the final request (strActivatePolicy_YN = Y)
* On a premium credit request error the Status of the result will be set as Error and a Message will be returned.
* CreditAgreementDate – Our system will default this value to today's date if not supplied
* The following existing fields will have no impact over premium credit finance:

```xml
<strFinance_YN>Y</strFinance_YN>
<strFinanceCompanyName xsi:type="xsd:string" />
``` 

### Example request

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <PostPolicyStringsReduced
                xmlns="http://tempuri.org">...

            <strFinancePolicy_YN>Y</strFinancePolicy_YN>
            <!-- Y/N values, Y ? Submit premium credit request, N or blank for no premium credit -->
            <strBankAccountName xsi:type="xsd:string">Joe Bloggs</strBankAccountName>
            <!-- Customer Bank Account Name Must not be blank. Max Length: 18 -->
            <strBankAccountNumber xsi:type="xsd:string">12345678</strBankAccountNumber>
            <!-- Customer's Bank Account Number. Must be an 8-digit number. Zero fill to left. Max Length: 8 -->
            <strBankSortCode xsi:type="xsd:string">123456</strBankSortCode>
            <!-- Customer's Bank Sort Code. Must be a valid bank Sort Code. Max Length: 6 -->
            <strCustomerDOB xsi:type="xsd:string">01/01/1980</strCustomerDOB>
            <!-- Customer DOB. This is the customer's DOB for use as a means of authentication. -->
            <strPrefPaymentDate xsi:type="xsd:string">01</strPrefPaymentDate>
            <!-- Preferred Payment Date. The New Collection Day for Direct Debit Collections, to be applied to the customer's Master Record by PCL. If set, must be a number from '01' to '31' representing the day-of-the-month on which future Direct Debit Collections for the customer will be scheduled by PCL. A Collection Day matching that of the Inception (Contract) Date shown on the customer's Master Record will cause PCL to schedule collections on the same day-of- the-month as the Inception Date. -->
            <strCustomerTelephone xsi:type="xsd:string" />
            <!-- Existing field. Make sure it's filled in. Seems not to be required by premium credit, but all our dealers are required to fill in this field -->
            <!-- Existing fields. Make sure the values are present. -->
            <strCustomerTitle xsi:type="xsd:string">Mrs</strCustomerTitle>
            <strCustomerForename xsi:type="xsd:string">J</strCustomerForename>
            <strCustomerSurname xsi:type="xsd:string">Test</strCustomerSurname>
            <strCompanyName xsi:type="xsd:string" />
            <strCustomerAddress1 xsi:type="xsd:string">Stapeley House London Road</strCustomerAddress1>
            <strCustomerAddress2 xsi:type="xsd:string">Stapeley</strCustomerAddress2>
            <strCustomerAddress3 xsi:type="xsd:string">NANTWICH,Cheshire</strCustomerAddress3>
            <strCustomerAddress4 xsi:type="xsd:string" />
            <strCustomerAddress5 xsi:type="xsd:string" />
            <strCustomerPostCode xsi:type="xsd:string">CW5 7JW</strCustomerPostCode>...
        </PostPolicyStringsReduced>
    </soap:Body>
</soap:Envelope>
```

## Testing

The SoapUI application can be used for testing the service, please use the URL specified
in the Interface section above with the ?WSDL suffix
(e.g. https://dc-uat-integration.autoprotect.net/TCF/RemotePolicyMaintenance.svc?WSDL)
and enable WS-A addressing with the action specified in the same Interface section from
above.
