# APG Integrator Documentation

This is the repo for the APG customer-facing documentation, hosted at https://integrators.autoprotectgroup.co.uk

The documentation is written in Markdown and deployed using [HonKit](https://honkit.netlify.app/), a deriviative of GitBook.

Images should be SVG, not bitmap screenshots of PowerPoint slides etc.

## Running locally

Check out the code and run:

```
npm install
npx honkit serve
```

Then point your browser to http://localhost:4000

## Deploying to live

Merge your changes into the `main` branch and Gitlab CI will do the rest.

That's it.
