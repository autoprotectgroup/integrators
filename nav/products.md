# Products

:::info
If a 3rd party system maintains a list of pre-defined product codes, it is not necessary to call the provider products endpoint at all, and products may be directly [added to the deal](#adding-a-product-to-a-deal)
:::

Once a deal has been loaded via the [deal API](deal.md), the applicable products may
requested from the provider products endpoint.

There is currently only one provider, `9577f5a5-31e8-4504-9ba5-70639472d521`, which
represents AutoProtect.

The provider products endpoint is:

`/deals/{dealId}/applications/1/provider-products/{providerId}`

## The products response

:::info
Just one product is shown here for brevity
:::

The provider products endpoint returns a summary of all the available products applicable to
the dealer. These correspond to the "tiles" in the Navigate product selection UI. 

A product may consist of a number of agreement types. These are represented in the product's
`products` element, shown empty in the condensed example below.

The `description` element contains an embeddable HTML product description, which may include
links to video content or other media.

```json
{
  "products": [
    {
      "id": "42",
      "code": "ABCDABCD",
      "isInsured": false,
      "description": "An html description of the product",
      "name": "Admin Scheme AP Holding Fund",
      "taxType": {
        "code": "5",
        "value": "VAT"
      },
      "percentageDepositRequired": 0.2,
      "hasPremiumFunding": true,
      "category": "Warranty",
      "categoryId": "6",
      "backSaleDuration": 30,
      "labourRates": [],
      "products": [],
      "hasProductsWithSpecialAcceptance": false,
      "isEligible": false,
      "policyReference": "20481346/18383245"
    }
  ]
}
```

Products that have `"isEligible": false` cannot be added to the deal.

Where a product is insurance-based, the `isInsured` flag will be true. For dealers who carry out their
demands and needs process within Navigate (ie: they are an AR of AutoProtect), the underlying product
IDs must be passed to the [compliance process](compliance.md), and demands and needs must be completed
before the product can be added to the deal.

## Agreement types

Retail products each have one or more underlying agreement types. Agreement types themselves may
have multiple variants based on attributes such as claims limits or durations.

To add a product to a deal, the following identifiers must be derived from the product data:

* `productCode`
* `productTypeCode`
* `duration.code`
* `claimLimit.code` (where applicable)
* `labourRate.code` (where applicable)

Below is an example of an underlying agreement type encompassing several durations and
claim limits. In this case, claim limits are tied to the durations and are not independently
selectable.

```json
{
  "id": "10003701",
  "code": "SCMPO2",
  "name": "Shine!Protect",
  "description": "Shine!Protect",
  "productTypeName": "Shine!Protect",
  "insuranceProductType": {
    "id": "13",
    "code": "SMART"
  },
  "maxRetailPrice": {
    "amount": "500000",
    "currency": "GBP"
  },
  "durations": [
    {
      "name": "12 Months",
      "code": "10027046"
    },
    {
      "name": "24 Months",
      "code": "10027047"
    },
    {
      "name": "36 Months",
      "code": "10025722"
    }
  ],
  "claimLimits": [
    {
      "name": "7 Repairs",
      "code": "10016038"
    },
    {
      "name": "14 Repairs",
      "code": "10016039"
    },
    {
      "name": "21 Repairs",
      "code": "10014930"
    }
  ],
  "retailPrices": [],
  "fieldsByDuration": {
    "10025722": [
      {
        "claimLimitCode": "10014930",
        "additionalCoverCode": ""
      }
    ],
    "10027046": [
      {
        "claimLimitCode": "10016038",
        "additionalCoverCode": ""
      }
    ],
    "10027047": [
      {
        "claimLimitCode": "10016039",
        "additionalCoverCode": ""
      }
    ]
  },
  "additionalCovers": [],
  "productTypeId": "10000444",
  "isPack": false,
  "isEligible": true,
  "hasSpecialAcceptance": false,
  "rateCardId": "12159770",
  "complianceStatus": null
}
```

Because the `fieldsByDuration` section allows only a single, specific claim limit for each
duration, the following combinations are valid:

| productCode | productTypeCode | duration  | claimLimit | labourRate |
|-------------|-----------------|-----------|------------|------------|
| 10003701    | 10000444        | 10027046  | 10016038   | n/a        |
| 10003701    | 10000444        | 10027047  | 10016039   | n/a        |
| 10003701    | 10000444        | 10025722  | 10014930   | n/a        |

## Adding a product to a deal

To add a product to a deal, make a `POST` request to the VAPs endpoint at 
`deals/{dealId}/applications/1/vaps`. Multiple products may be added in a single
operation.

```json
[
	{
		"provider": {
			"id": "9577f5a5-31e8-4504-9ba5-70639472d521"
		},
		"name": "Shine!Protect",
		"category": "Chips & Dents",
		"categoryId": "11",
		"productCode": "10003701",
		"productTypeCode": "10000444",
		"productTypeName": "Shine!Protect",
		"hasPremiumFunding": false,
		"claimLimit": {
			"name": "21 Repairs",
			"code": "10014930"
		},
		"labourRate": {
			"name": "",
			"code": ""
		},
		"additionalCover": [

		],
		"duration": {
			"name": "36 Months",
			"code": "10025722"
		},
		"priceGross": {
			"amount": "0",
			"currency": "GBP"
		},
		"taxRate": 20,
		"taxType": "VAT",
		"paymentType": "CASH",
		"coverStartDate": "2022-06-23T15:56:05.513Z"
	}
]
```

### How to map products fields to the add product request

| Add Product Field   | Products Source Field                                                      |
|---------------------|----------------------------------------------------------------------------|
| `provider.id`       | `9577f5a5-31e8-4504-9ba5-70639472d521`                                     |
| `productCode`       | `products.products.id`                                                     |
| `name`              | `products.products.name`                                                   |
| `claimLimit`        | One of `products.product.claimLimits` (or empty)                           |
| `additionalCover`   | One of `products.products.additionalCovers` (or empty)                     |
| `duration`          | One of `products.products.durations`                                       |
| `category`          | `products.category`                                                        |
| `categoryId`        | `products.categoryId`                                                      |
| `productTypeCode`   | `products.id`                                                              |
| `productTypeName`   | `products.name`                                                            |
| `hasPremiumFunding` | `products.hasPremiumFunding`                                               |
| `labourRate`        | One of `products.labourRates` (or empty)                                   |
| `taxRate`           | The prevailing rate of VAT/IPT                                             |
| `taxType`           | `products.taxType.value`                                                   |
| `paymentType`       | [List](https://docs.api.navigate.autoprotect.co.uk/#tag/payment_type_enum) |
| `coverStartDate`    | The date the policy is intended to commence                                |

## Further example - warranty product

In this example, a labour rate is also available and the two claims limits can be selected
independently of the duration. This also includes recommended and minimum retail prices.

### Products section from the product response

The product below describes allows the following products to be registered:

| duration            | claimLimit       | labourRate | Min / Rec /Max Price |
|---------------------|------------------|------------|----------------------|
| 10023957 (12 month) | 10014011 (£1000) | 1          | £0 / £140 / £500     |
| 10023957 (12 month) | 10014012 (£2000) | 1          | £0 / £130 / £500     |
| 10023958 (24 month) | 10014011 (£1000) | 1          | £0 / £190 / £500     |
| 10023958 (24 month) | 10014012 (£2000) | 1          | £0 / £200 / £500     |

:::info
Labour rates are defined at retail product level, which is one level up from the agreement types level shown here
:::

```json
{
  "id": "10003546",
  "code": "UNCCP",
  "name": "CP Comprehensive",
  "description": "CP Comprehensive",
  "productTypeName": "Warranty",
  "insuranceProductType": {
    "id": "3",
    "code": "Warranty"
  },
  "maxRetailPrice": {
    "amount": "500000",
    "currency": "GBP"
  },
  "durations": [
    {
      "name": "12 Months",
      "code": "10023957"
    },
    {
      "name": "24 Months",
      "code": "10023958"
    }
  ],
  "claimLimits": [
    {
      "name": "£1000",
      "code": "10014011"
    },
    {
      "name": "£2000",
      "code": "10014012"
    }
  ],
  "retailPrices": [
    {
      "recommendedRetail": {
        "amount": "14000",
        "currency": "GBP"
      },
      "minimumRetail": {
        "amount": "0",
        "currency": "GBP"
      },
      "durationCode": "10023957",
      "claimLimitCode": "10014011",
      "labourRateCode": "1"
    },
    {
      "recommendedRetail": {
        "amount": "13000",
        "currency": "GBP"
      },
      "minimumRetail": {
        "amount": "0",
        "currency": "GBP"
      },
      "durationCode": "10023957",
      "claimLimitCode": "10014012",
      "labourRateCode": "1"
    },
    {
      "recommendedRetail": {
        "amount": "19000",
        "currency": "GBP"
      },
      "minimumRetail": {
        "amount": "0",
        "currency": "GBP"
      },
      "durationCode": "10023958",
      "claimLimitCode": "10014011",
      "labourRateCode": "1"
    },
    {
      "recommendedRetail": {
        "amount": "20000",
        "currency": "GBP"
      },
      "minimumRetail": {
        "amount": "0",
        "currency": "GBP"
      },
      "durationCode": "10023958",
      "claimLimitCode": "10014012",
      "labourRateCode": "1"
    }
  ],
  "fieldsByDuration": {
    "10023957": [
      {
        "claimLimitCode": "10014011",
        "additionalCoverCode": ""
      },
      {
        "claimLimitCode": "10014012",
        "additionalCoverCode": ""
      },
    ],
    "10023958": [
      {
        "claimLimitCode": "10014011",
        "additionalCoverCode": ""
      },
      {
        "claimLimitCode": "10014012",
        "additionalCoverCode": ""
      }
    ]
  },
  "additionalCovers": [],
  "productTypeId": "6",
  "isPack": false,
  "isEligible": true,
  "hasSpecialAcceptance": false,
  "rateCardId": "12159763",
  "complianceStatus": null
}
```

### Adding the warranty to the deal

Having selected the duration (24 months) and claim limit (£1000), the warranty can be added as follows.

We will need a few parameters from the top level product:

* `productCode` is 10003546
* `productTypeCode` is 6
* `labourRate` is `{ "name": "60", "code": "1" }`

The recommended price is £190, so we will add the product at that.

```json
[
  {
    "provider": {
      "id": "9577f5a5-31e8-4504-9ba5-70639472d521"
    },
    "name": "CP Comprehensive",
    "category": "Warranty",
    "categoryId": "3",
    "productCode": "10003546",
    "productTypeCode": "6",
    "productTypeName": "Warranty",
    "hasPremiumFunding": false,
    "claimLimit": {
      "name": "£1000",
      "code": "10014011"
    },
    "labourRate": {
      "name": "60",
      "code": "1"
    },
    "additionalCover": [],
    "duration": {
      "name": "24 Months",
      "code": "10023958"
    },
    "priceGross": {
      "amount": "19000",
      "currency": "GBP"
    },
    "taxRate": 20,
    "taxType": "VAT",
    "paymentType": "CASH",
    "coverStartDate": "2022-06-23T15:56:05.513Z"
  }
]
```

