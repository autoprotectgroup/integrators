# Navigate Platform API

## Introduction

Navigate is APG's new point-of-sale platform for insured/non-insured products,
compliance and finance transactions to support the funding and sale of a vehicle
and additional products.

The full OpenAPI spec is available here: https://docs.api.navigate.autoprotect.co.uk

Most API operations centre around the deal model, as this forms a representation of
the customer, vehicle, products and financials that describe the sale of a vehicle.

Other endpoints are available for performing tasks such as postcode and VRM lookups.

## Who should use these APIs?

* 3rd party DMS (Dealer Management System) suppliers
* Dealers who wish to integrate Navigate into their existing sales process
* Online retailers who need to fulfil registration of policies for their customers
* Compliance administrators and AR networks who need to activate products

You should consult the list of [suggested implementations](implementations.md) to
determine which workflow would be suitable for your intended use-case.

## Authentication

Requests are authenticated with a bearer token obtained by OAuth 2.0.

## Endpoints

### Production

| Endpoint       | URL                                                                                 |
|----------------|-------------------------------------------------------------------------------------|
| User Interface | https://showroom.navigate.autoprotect.co.uk/                                        |
| API Auth       | https://id.navigate.autoprotect.co.uk/realms/navigate/protocol/openid-connect/token |
| API Path       | https://api.navigate.autoprotect.co.uk/v1                                           |

### UAT

| Endpoint   | URL                                                                                     |
|------------|-----------------------------------------------------------------------------------------|
| User Login | https://navigate.uat.dtdev.co.uk/                                                       |
| API Auth   | https://id.uat.navigate.autoprotect.co.uk/realms/navigate/protocol/openid-connect/token |
| API Path   | https://api.dtdev.co.uk/uat/v1                                                          |

