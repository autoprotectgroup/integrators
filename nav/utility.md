# Utility Functions

A number of endpoints exist for authenticated clients to query Navigate's internal
and external data sources for data relating to:

* Dealer metadata
* Postcodes and addresses
* Other customer attributes, such as:
  * Employment types, sectors and statuses
  * Nationality and country
  * Marital status
  * Passport types
  * Residential status
* Live lookups of vehicle data by VRM
* Static vehicle (asset) attributes such as:
  * Vehicle types (eg: car, LCV)
  * Fuel types
  * Makes
  * Models
  * Styles
  * Transmission types
* Finance attributes, such as:
  * Lenders
  * Finance types
  * Payment methods

The various requests are self explanatory and are described in the 
[List endpoint documentation](https://docs.api.navigate.autoprotect.co.uk/#tag/List).