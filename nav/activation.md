# Activation

Activation consists of two steps, authorisation and activation.

## Authorisation

The authorisation endpoint is:

`/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/authorise`

No e-signature is required because the statement of demands and needs has already been e-signed by the customer,
so the request body can specify manual presentation of the document.

```json
{
  "isManual": true,
  "reason":"PRESENTED_MANUALLY"
}
```

## Activation
Activation occurs when the following endpoint is called using the `POST` method:

`/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/activate`

No request body is required.

## Retrieving the policy pack

Once the agreement has been activated the policy pack may be retrieved from the agreement details, which can
be obtained from the following endpoint using the `GET` method.

`/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}`

The documents section in the response appears as follows:

```json
{
  "documents": [
    {
      "filename": "Policy Pack.pdf",
      "url": "/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/documents/unsigned-policy-pack",
      "type": "UNSIGNED_POLICY_PACK",
      "size": 1218621,
      "options": null,
      "storageDate": "2022-09-30T15:28:19+00:00",
      "lastAccessDate": "2022-09-30T15:28:34+00:00",
      "versionId": "4JtV1_BNP1Bz_z9ML0rFUIfRVXtTkePY"
    }
  ]
}
```

Calling the URL given in the documents section in the response returns a pre-signed link to download the document:

```json
{
    "filename": "Policy Pack.pdf",
    "url": "<< AWS Pre-signed link here>>",
    "type": "UNSIGNED_POLICY_PACK",
    "size": 1218621
}
```

This URL is safe to either download the pack for storage locally, or use as a redirect in a customer portal for example.

The link is valid for one hour, so it should not be embedded in an email or stored for later retrieval.