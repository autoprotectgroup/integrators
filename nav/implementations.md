# Suggested Implementations

The workflow required for a full end-to-end integration depends on the regulatory status
of the dealer.

It is not necessary to complete the entire process within the APIs as they are
available in parallel with the Navigate user interface. For example, you may decide only
to send the customer and vehicle details in via API, but then complete the sales
process in the UI.

## Obligor product registration

:::success
Status: Fully supported
:::

No demands and needs, IDD or product information is required to be sent to the customer.

### API call sequence

1. `POST` to `/deals`
2. `GET` to `/deals/{dealId}/applications/1/provider-products/{providerId}`
3. `POST` to `/deals/{dealId}/applications/1/vaps`
4. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/authorise`
5. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/activate`

## FCA authorised registration only

:::success
Status: Fully supported
:::

### Remarks

* GAP moratorium must be disabled at product level by AutoProtect, but other insured policy registration works fine
* Demands and needs, and other relevant product information distributed by dealer
* No requirement to capture product information handover date
* No GAP moratorium
* Dealer FCA Status must be `FSA_REGISTERED`
* Calling compliance endpoint results in `"status": "DISABLED"`, `"canActivatePolicy": true`

### API call sequence

1. `POST` to `/deals`
2. `GET` to `/deals/{dealId}/applications/1/provider-products/{providerId}`
3. `POST` to `/deals/{dealId}/applications/1/vaps`
4. `POST` to `/deals/{dealId}/applications/1/not-taken-up` - once for each NTU product type
5. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/authorise`
6. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/activate`

The document pack may then be requested from the agreement document endpoint.

## Appointed representative of AutoProtect for general insurance

:::success
Status: Fully supported
:::

* Prescribed information must be distributed by Navigate
* No ability to back-date or populate product handover date in deal API
* AutoProtect's demands and needs are served
* D&N must be completed before products can be added to deal
* D&N must be signed before products can be activated

### API call sequence

1. `POST` to `/deals`
2. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/compliance/emails/email-initial-docs`
3. `GET` to `/deals/{dealId}/applications/1/providers/{providerId}/compliance/demands-and-needs/questions/first?products=xxxxx,yyyyy` - to obtain first D&N question
4. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/compliance/demands-and-needs/questions/{questionId}` - to answer question
5. `GET` to `/deals/{dealId}/applications/1/providers/{providerId}/compliance/demands-and-needs/questions/next` - to get next question, repeat steps 4 and 5 until next question endpoint returns 404 status
6. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/compliance/documents/demands-and-needs/sign` - to initiate e-sign process
7. Optional `GET` to `/deals/{dealId}/applications/1/providers/{providerId}/compliance/summary` - to check the e-sign status
8. `GET` to `/deals/{dealId}/applications/1/provider-products/{providerId}`
9. `POST` to `/deals/{dealId}/applications/1/vaps`
10. `POST` to `/deals/{dealId}/applications/1/not-taken-up` - for each NTU product type
11. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/authorise`
12. `POST` to `/deals/{dealId}/applications/1/providers/{providerId}/agreements/{agreementId}/activate`

## Fully authorised tailoring AutoProtect's demands and needs

:::success
Status: Fully supported
:::

### Remarks

* Prescribed information must be distributed by Navigate, previously it was possible to distribute the documentation separately and inform us of the handover date
* No product information handover date required may be specified
* Dealer’s own demands and needs are loaded into the compliance module
* GAP moratorium skipped if disabled on product by AutoProtect
* Compliance status remains `OPT_IN_PENDING` until email-initial-docs has been called

### API call sequence

Same as "Appointed representative of AutoProtect for general insurance"

## Fully authorised using AutoProtect's demands and needs

:::success
Status: Fully supported
:::

### Remarks

* Prescribed information must be distributed by Navigate
* No product information handover date required
* AutoProtect's demands and needs are used
* Single D&N process for all products
* GAP moratorium may be enforced depending on product configuration

### API call sequence

Same as "Appointed representative of AutoProtect for general insurance"

## Appointed representative - finance only

:::danger
Not supported due to no product to sell, there is no way to complete the process.
:::

Please contact us if you wish to use the finance qualification functionality in isolation.

## Appointed Representative of another firm

Same workflow as FCA authorised registration only.

## Exempt dealer

Same workflow as obligor dealers
