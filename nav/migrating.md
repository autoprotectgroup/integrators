# Migrating from legacy APIs

The legacy API endpoints are generally *one-shot* processes, however the
[Navigate API](README.md) is somewhat more *conversational*.

## AutoProcess quote API

The [quote API](../ap/quote.md) is used to pre-populate customers and vehicles into AutoProcess
so that the demands and needs process can be completed without the need to double-key the
customer's details.

The equivalent to this operation in Navigate is the [deal](deal.md) endpoint. Navigate APIs are
all JSON/REST whereas legacy APIs are XML/SOAP.

Dates and times are ISO 8601 format.

### Credentials and identifiers

Navigate uses different credentials to AutoProcess. Integrators should obtain new identifiers
as part of their migration process.

Authentication provides a bearer token which must be used in all subsequent requests. The token
should be cached and renewed prior to expiry. Many common HTTP client libraries support this
type of authentication mechanism.

### Initial deal creation

The initial deal creation should be performed with a `POST` request to the
[deal endpoint](deal.md#creating-a-deal).

At the very least, the following parameters must be provided:

* Application type (`PRIVATE`)
* Dealer ID
* Applicant type (`HIRER`)
* Customer type (`Person` / `Organisation`)
* Organisation name (if a business)
* Forename
* Surname

| AutoProcess XML Node | Navigate JSON Element                                                                                               |
|----------------------|---------------------------------------------------------------------------------------------------------------------|
| `Username`           | N/A Use OAuth bearer token                                                                                          |
| `Password`           | N/A Use OAuth bearer token                                                                                          |
| `DealerId`           | UUID in `dealer.id` - see [dealer list endpoint](https://docs.api.navigate.autoprotect.co.uk/#operation/getDealers) |
| `IsOrganisation`     | `applications[0].applicants[0].customerType` = `Person`                                                             |
| `FirstName`          | `applications[0].applicants[0].forename`                                                                            |
| `Surname`            | `applications[0].applicants[0].surname`                                                                             |

This will return the current deal, along with the deal ID in the top level `id` element.

### Customer/vehicle population

The rest of the deal may be populated via a `PATCH` request to the `/deals/{$dealId}` endpoint.

| AutoProcess XML Node             | Navigate JSON Element                                                                                                                                                                                                                       |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `QuoteNr`                        | Not supported                                                                                                                                                                                                                               |
| `DataProtection`                 | `applications[0].applicants[0].marketing.sms` (also `.telephone` / `.email`)                                                                                                                                                                |
| `Title`                          | `applications[0].applicants[0].title` [List](https://docs.api.navigate.autoprotect.co.uk/#tag/name_title_enum)                                                                                                                              |
| `Line1` (of address)             | `applications[0].applicants[0].addresses[0].flat` or<br/>`applications[0].applicants[0].addresses[0].houseName` or<br/>`applications[0].applicants[0].addresses[0].houseNumber`<br/>and `applications[0].applicants[0].addresses[0].street` |
| `Line2`                          | `applications[0].applicants[0].addresses[0].district`                                                                                                                                                                                       |
| `Line3`                          | `applications[0].applicants[0].addresses[0].city`                                                                                                                                                                                           |
| `PostalCode`                     | `applications[0].applicants[0].addresses[0].postcode`                                                                                                                                                                                       |
| `Telepone`                       | `applications[0].applicants[0].mobileNumber`                                                                                                                                                                                                |
| `EMail`                          | `applications[0].applicants[0].emailAddress`                                                                                                                                                                                                |
| `VehicleUse`                     | `applications[0].applicants[0].vehicleUsageType` [List](https://docs.api.navigate.autoprotect.co.uk/#tag/usage_type_enum)                                                                                                                   |
| `VehicleFinanced`                | TBC                                                                                                                                                                                                                                         |
| `VRN`                            | `applications[0].asset.registrationNumber`                                                                                                                                                                                                  |
| `VIN`                            | `applications[0].asset.vin`                                                                                                                                                                                                                 |
| `DateOfFirstRegistration`        | `applications[0].asset.firstRegistered`                                                                                                                                                                                                     |
| `DeliveryDate`                   | `applications[0].asset.deliveryDate`                                                                                                                                                                                                        |
| `ManufacturerWarrantyExpiryDate` | `applications[0].asset.mfrWarrantyExpiryDate`                                                                                                                                                                                               |
| `GlassModelId`                   | `applications[0].asset.glassesCode`                                                                                                                                                                                                         |                                                                                                                                                                                                                                            
| `QualifiedModelId`               | `applications[0].asset.glassesFamily`                                                                                                                                                                                                       |
| `PurchaseDate`                   | `applications[0].asset.purchaseDate`                                                                                                                                                                                                        |
| `MOTDueDate`                     | `applications[0].asset.motDueDate`                                                                                                                                                                                                          |
| `Mileage`                        | `applications[0].asset.mileage`                                                                                                                                                                                                             |
| `PurchasePrice`                  | `applications[0].financials.assetPriceGross.amount` See [money type](https://docs.api.navigate.autoprotect.co.uk/#tag/money_type_model)                                                                                                     |
| `IsNew`                          | `applications[0].asset.isNew`                                                                                                                                                                                                               |
| `IsUKSpecification`              | `applications[0].asset.isUkSpecification`                                                                                                                                                                                                   |
| `Weight`                         | `applications[0].asset.grossWeight`                                                                                                                                                                                                         |
| `VehicleClass`                   | `applications[0].asset.type` [List](https://docs.api.navigate.autoprotect.co.uk/#tag/vehicle_type_enum)                                                                                                                                     |
| `TempVRN`                        | Not supported                                                                                                                                                                                                                               |
| `SalespersonId`                  | UUID `assignedSalesUser.id` from [list endpoint](https://docs.api.navigate.autoprotect.co.uk/#operation/getDealerUsers)                                                                                                                     |
| `ProductInfoHandoverDateTime`    | TBC                                                                                                                                                                                                                                         |
| `QuoteStatus`                    | Set automatically                                                                                                                                                                                                                           |

The two operations may be combined into a one-shot `POST` request, an example is given below:

```json
{
  "type": "PRIVATE",
  "dealer": {
    "id": "{{dealerId}}"
  },
  "applications": [
    {
      "applicants": [
        {
          "type": "HIRER",
          "customerType": "Person",
          "marketing": {
            "sms": false,
            "email": false,
            "telephone": false
          },
          "assetUsage": "PRIVATE",
          "addresses": [
            {
              "flat": "Apartment 37",
              "houseName": "McClure House",
              "street": "The Boulevard",
              "city": "LEEDS",
              "county": "LEEDS",
              "countryCode": "GB",
              "postcode": "LS10 1LR"
            }
          ],
          "forename": "Oneshot",
          "surname": "Postman",
          "title": "MR",
          "mobileNumber": "07700900456",
          "emailAddress": "test.customer@example.com"
        }
      ],
      "asset": {
        "type": "PRIVATE_LIGHT_GOODS_VEHICLE",
        "registrationNumber": "AB12CDE",
        "glassesCode": "253779",
        "glassesFamily": "001",
        "transmissionType": "MANUAL",
        "fuelType": "PETROL",
        "isNew": false,
        "make": "VAUXHALL",
        "makeValue": "VAUXHALL",
        "model": "ASTRA HATCHBACK",
        "modelValue": "ASTRA HATCHBACK",
        "style": "1.4T 16V 150 SRi 5dr",
        "styleValue": "1.4T 16V 150 SRi 5dr",
        "engineSize": 1400,
        "firstRegistered": "2017-06-19T00:00:00+00:00",
        "vin": "W0LBF6EC2HG138041",
        "yearOfManufacture": 2017,
        "purchaseDate": "2022-05-25T00:00:00+00:00",
        "isUkSpecification": true,
        "mileage": 25000
      },
      "financials": {
        "assetPriceGross": {
          "amount": "2500000",
          "currency": "GBP"
        }
      }
    }
  ]
}
```

### Other required fields

The following fields are also required, but may be populated with placeholder values if they
are not available.

| Navigate JSON Element                    | Suggested Default |
|------------------------------------------|-------------------|
| `applications[0].asset.make`             | TBC               |
| `applications[0].asset.makeValue`        | TBC               |
| `applications[0].asset.model`            | TBC               |
| `applications[0].asset.modelValue`       | TBC               |
| `applications[0].asset.style`            | TBC               |
| `applications[0].asset.styleValue`       | TBC               |
| `applications[0].asset.fuelType`         | PETROL            |
| `applications[0].asset.transmissionType` | MANUAL            |

### Checking the compliance status

Currently, API users may check whether the demands and needs process has been completed by checking
that the quote status is `Quoted`.

In Navigate, the [compliance summary endpoint](compliance.md#requesting-the-demands-and-needs-summary)
will provide the following response.

```json
{
    "status": "COMPLETED",
    "questions": [],
    "productSummary": [],
    "products": [],
    "createdAt": "2022-06-23T11:41:31+00:00",
    "completedAt": "2022-06-23T11:41:31+00:00"
}
```

The status of `COMPLETED` indicates that the D&N process has been completed successfully. 

## AutoProcess activation API

AutoProcess has a separate API for registering products. In Navigate, the same can be achieved by
first [creating a deal](#initial-deal-creation) as above, then
[adding products](products.md#adding-a-product-to-a-deal).

Once the deal has been created and products added, the policy can be activated.

## DealTrak Load Lead / Load Application

The Navigate deal model is conceptually very similar to the Load Lead and Load Application
APIs. Further information on how to migrate will be provided at a later date.
