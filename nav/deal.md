# The Deal Model

The Navigate deal model is a JSON data structure to capture the
retail sale of a vehicle (or other asset) in its entirety.

The `/deals` endpoint provides a means of creating and modifying
deals via the `POST` and `PATCH` methods. It is not necessary to 
overwrite the entire deal when making a `PATCH` request.

When updating the following elements, only the ID is required - the rest
of the data will be populated by the Navigate platform:

* `assignedUser`
* `assignedSalesUser`
* `dealer`

The `client` element is inferred from the dealer ID and cannot be updated.

The response to the deals endpoint is always the entire deal in
its current state. Where an enumeration exists, a descriptive equivalent
is also returned. It is not necessary to provide this description when making
requests.

```json
...
  "assetUsage": "PRIVATE",
  "assetUsageDesc": "Private",
...
```

Once the deal has been fully populated, you may proceed to use the
[products](products.md) endpoint to retrieve available products, or if the
dealer is configured to require demands and needs, the
[compliance](compliance.md) endpoint must be used to qualify the customer
unless the dealer has an external compliance process.

## Basic Structure

```json
{
  "id": "56f297b4-c4d4-40a2-8ba2-242ffe499045",
  "applications": [
    {
      "applicants": [
        {
          "addresses": [],
          "employments": {}
        }
      ],
      "asset": {},
      "financials": {
        "assetPriceGross": {}
      },
      "additionalDetails": [],
      "vaps": [
        {
          "provider": {},
          "claimLimit": {},
          "duration": {},
          "additionalCover": [],
          "priceNet": {},
          "priceGross": {}
        }
      ],
      "policy": {}
    }
  ],
  "assignedUser": {},
  "assignedSalesUser": {},
  "dealer": {},
  "client": {}
}
```

## Creating a deal

`POST /deals`

Dealer IDs are fixed, but may be looked up dynamically from the
[dealer endpoint](https://docs.api.navigate.autoprotect.co.uk/#tag/Dealer).

Various aspects of the deal are configured according to the specified
dealer. The newly created deal ID is returned in the `id` response
element.

### Private Individual

```json
{
  "type": "PRIVATE",
  "dealer": {
    "id": "1e06faf2-055a-52ae-b14f-d14043ef64a1"
  },
  "applications": [
    {
      "applicants": [
        {
          "type": "HIRER",
          "customerType": "Person",
          "forename": "Forename",
          "surname": "Surname"
        }
      ]
    }
  ]
}
```

### Organisation

```json
{
  "type": "PRIVATE",
  "dealer": {
    "id": "1e06faf2-055a-52ae-b14f-d14043ef64a1"
  },
  "applications": [
    {
      "applicants": [
        {
          "type": "HIRER",
          "customerType": "Organisation",
          "organisationName": "Acme Co",
          "forename": "Wiley",
          "surname": "Coyote"
        }
      ]
    }
  ]
}
```

## Populating the deal part-by-part

You may make a series of `PATCH` requests to populate the deal incrementally
or in one go. The key elements are described below.

There is only one application per deal at this time. Multiple applications
may be supported in the future.

Omitting elements results in the value being unchanged. Removing elements
requires an explicit request to the relevant endpoint with the `DELETE`
method.

### Applicant

:::info
Multiple applicants are only required for joint finance applications and business proposals (where applicants represent company officers)
:::

```json
{
  "type": "HIRER",
  "marketing": {
    "sms": false,
    "email": false,
    "telephone": false
  },
  "assetUsage": "PRIVATE",
  "addresses": [],
  "employments": [],
  "forename": "Test",
  "surname": "Customer",
  "title": "MR",
  "mobileNumber": "07700900456",
  "emailAddress": "test.customer@example.com"
}
```

Applicants may be amended using either the [amend applicant](https://docs.api.navigate.autoprotect.co.uk/#operation/updateApplicant)
endpoint or by sending a `PATCH` request containing the relevant fragment
to the `/deals` endpoint.

### Addresses

Addresses may be added to applicants via a `POST` request to the
[create applicant](https://docs.api.navigate.autoprotect.co.uk/#operation/createApplicantAddresses)
endpoint or provided as part of the applicant.

Multiple addresses (covering a minimum of three years) are only required
where the deal will be financed via Navigate.

```json
{
  "flat": "Apartment 37",
  "houseName": "McClure House",
  "street": "The Boulevard",
  "city": "LEEDS",
  "county": "LEEDS",
  "countryCode": "GB",
  "postcode": "LS10 1LR"
}
```

### Employments

Employments are only required where the deal will be financed via
Navigate. This functionality is expected in 2023.

### Asset / Vehicle

Vehicle details may be looked up by VRM from the
[get vehicle by VRM endpoint](https://docs.api.navigate.autoprotect.co.uk/#operation/getVehicleByVRM).
New vehicles may be describing using values obtained from the asset
makes/models/styles endpoints in the list service.

An application has exactly one asset, which is shown below:

```json
{
  "type": "PRIVATE_LIGHT_GOODS_VEHICLE",
  "registrationNumber": "AB12CDE",
  "glassesCode": "253779",
  "glassesFamily": "001",
  "transmissionType": "MANUAL",
  "fuelType": "PETROL",
  "isNew": false,
  "make": "VAUXHALL",
  "makeValue": "VAUXHALL",
  "model": "ASTRA HATCHBACK",
  "modelValue": "ASTRA HATCHBACK",
  "style": "1.4T 16V 150 SRi 5dr",
  "styleValue": "1.4T 16V 150 SRi 5dr",
  "engineSize": 1400,
  "firstRegistered": "2017-06-19T00:00:00+00:00",
  "vin": "W0LBF6EC2HG138041",
  "yearOfManufacture": 2017,
  "purchaseDate": "2022-05-25T00:00:00+00:00",
  "isUkSpecification": true,
  "mileage": 25000
}
```

### Financials

The financials section details the vehicle price, along with any figures
relating to funding the vehicle. The minimum requirement is the vehicle price:

```json
{
  "assetPriceGross": {
    "amount": "2500000",
    "currency": "GBP"
  }
}
```

:::info
Figures are specified in pence, the above example being £25,000
:::

## Populating the deal in one go

The above requests may be combined into a single operation, which may
be sent as a `PATCH` request to the deal endpoint.

If required, this can be combined with the initial `POST` request to create the entire deal in
a single operation.

:::info
It is not possible to include any VAPs at the point of initial deal creation.
:::

```json
{
  "id": "56f297b4-c4d4-40a2-8ba2-242ffe499045",
  "applications": [
    {
      "applicants": [
        {
          "type": "HIRER",
          "marketing": {
            "sms": false,
            "email": false,
            "telephone": false
          },
          "assetUsage": "PRIVATE",
          "addresses": [
            {
              "flat": "Apartment 37",
              "houseName": "McClure House",
              "street": "The Boulevard",
              "city": "LEEDS",
              "county": "LEEDS",
              "countryCode": "GB",
              "postcode": "LS10 1LR"
            }
          ],
          "forename": "Test",
          "surname": "Customer",
          "title": "MR",
          "mobileNumber": "07700900456",
          "emailAddress": "test.customer@example.com"
        }
      ],
      "asset": {
        "type": "PRIVATE_LIGHT_GOODS_VEHICLE",
        "registrationNumber": "AB12CDE",
        "glassesCode": "253779",
        "glassesFamily": "001",
        "transmissionType": "MANUAL",
        "fuelType": "PETROL",
        "isNew": false,
        "make": "VAUXHALL",
        "makeValue": "VAUXHALL",
        "model": "ASTRA HATCHBACK",
        "modelValue": "ASTRA HATCHBACK",
        "style": "1.4T 16V 150 SRi 5dr",
        "styleValue": "1.4T 16V 150 SRi 5dr",
        "engineSize": 1400,
        "firstRegistered": "2017-06-19T00:00:00+00:00",
        "vin": "W0LBF6EC2HG138041",
        "yearOfManufacture": 2017,
        "purchaseDate": "2022-05-25T00:00:00+00:00",
        "isUkSpecification": true,
        "mileage": 25000
      },
      "financials": {
        "assetPriceGross": {
          "amount": "2500000",
          "currency": "GBP"
        }
      }
    }
  ]
}
```