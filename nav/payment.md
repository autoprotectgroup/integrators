# Payment

Currently only the "pay in dealership" payment method is available to API integrators.

This is requested when the `paymentType` is `CASH` when adding the product to the deal.