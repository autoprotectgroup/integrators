# Rosetta example response

```xml
<?xml version="1.0"?>
<rosetta_response>
  <status_id>2</status_id><!-- see enumerations -->
  <original_lender_status>Pending E-Sign</original_lender_status><!-- optional -->
  <error>Something went wrong</error><!-- only required if status_id 8 (error) is returned -->  
  <reference>15488475</reference><!-- must be unique -->
  <conditions><!-- optional -->
    <condition>Service history required</condition>
    <condition>Subject to successful affordability check</condition>
  </conditions>
  <finances><!-- optional -->
    <finance name="APR">16.9</finance><!-- name attribute is arbitrary -->
    <finance name="Monthly Repayment">267.01</finance>
    <finance name="Total Repayable">12816.48</finance>
    <finance name="Term">48</finance>
  </finances>
  <tasks><!-- optional -->
    <task status="complete" time="2022-05-04T12:34:56">
      <title>Provide proof of income</title>
      <description>Copies of customer latest bank statement and payslip required</description>
    </task>
    <task status="incomplete" time="2022-05-05T12:34:56">
      <title>Dealer invoice</title>
      <description>Upload the dealer invoice</description>
    </task>
  </tasks>
  <lender_questions><!-- optional -->
    <question id="asdf1234">
      <title>Contra Settlement</title>
      <type>multiple</type>
      <description>Which of these existing agreements does this application contra-settle?</description>
      <options>
          <option value="827373737">Agreement 827373737, AB12CDE</option>
          <option value="928374743">Agreement 928374743, CD24EFG</option>
      </options>
    </question>
  </lender_questions>
</rosetta_response>
```

## Element Descriptions

| Section                    | Purpose                                                                               |
|----------------------------|---------------------------------------------------------------------------------------|
| `<status_id>`              | DealTrak [destination status](enumerations.md#destination-status)                     |
| `<note>`                   | Optional free-text note displayed with "info" styling                                 |
| `<original_lender_status>` | Free-text status, used to convey additional information                               |
| `<error>`                  | Error message displayed in lender conditions with error styling                       |
| `<reference>`              | Your own system reference, which must be globally unique                              |
| `<conditions>`             | Optional free-text conditions or requests for additional information                  |
| `<finances>`               | Arbitrary key-value pairs of figures or other details, not necessarily numeric        |
| `<tasks>`                  | Tasks are actions that you require a user to perform                                  |
| `<lender_questions>`       | Arbitrary questions and multiple choice answers relating to this specific application |

Lender decision elements are cumulative, and any items provided in previous responses are retained
until superceded by a later decision. For example, sending conditions followed by a decision with no `<conditions>`
element will retain the previous conditions. Returning an empty conditions tag, ie: `<conditions/>`,
will clear the previous conditions.

Completion of tasks, and answering of lender questions results in a lender action being submitted to
the lender action endpoint.

If no update to the destination status is required or available, status 0 may be sent. The response to
the initial send must contain a valid destination status ID, usually 4 (Referred) or 10 (Received OK) if
no immediate decision is available.