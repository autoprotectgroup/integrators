# Rosetta SOAP Application - Load Application Example

To make use of this example in our test environment, you will need to:

- Change the username/password at the end to the details provided to you
- Change the <reference> node near the top to something unique
- Load it into Postman or other web service client
- Set the content type header to application/xml
- Fire it at https://master.staging2.dealtrak123.co.uk/rosetta/webservices/application

```xml
<soapenv:Envelope
    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:app="https://www.dealtrak123.co.uk/rosetta/webservices/application"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Header/>
    <soapenv:Body>
        <app:load_application>
            <request>
                <reference>external-ref-0001</reference>
                <!-- application_id is only needed if you're updating an existing prop -->
                <application_id xsi:nil="true" />
                <application_section>
                	<organisation xsi:nil="true" />
                	<assigned_dealer_contact xsi:nil="true" />
                	<assigned_introducer_contact xsi:nil="true" />
                	<sales_staff_id xsi:nil="true" />
                	<sales_staff xsi:nil="true" />
                	<sales_manager_id xsi:nil="true" />
                	<sales_manager xsi:nil="true" />
                	<finance_amount xsi:nil="true" />
                	<referrer xsi:nil="true" />
                	<distance_marketed xsi:nil="true" />
                    <applicant_section>
                        <item>
                            <residences>
                                <item>
                                    <flat></flat>
                                    <house_number>123</house_number>
                                    <house_name></house_name>
                                    <street>Testing Street</street>
                                    <district>Testville</district>
                                    <towncity>Testington</towncity>
                                    <county>Testshire</county>
                                    <postcode>TE5 5ST</postcode>
                                    <years>5</years>
                                    <months>4</months>
                                    <uk_address>true</uk_address>
                                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#residential-status -->
                                    <residential_status_id>1</residential_status_id>
                                </item>
                            </residences>
                            <employments>
                                <!--Zero or more repetitions:-->
                                <item>
                                    <occupation>Test Pilot</occupation>
                                    <company>DealTrak Ltd</company>
                                    <building>Block F</building>
                                    <subbuilding></subbuilding>
                                    <street>The Boulevard</street>
                                    <district>Leeds Dock</district>
                                    <towncity>Leeds</towncity>
                                    <county>West Yorkshire</county>
                                    <postcode>LS10 1LR</postcode>
                                    <years>6</years>
                                    <months>2</months>
                                    <uk_trading_address>true</uk_trading_address>
                                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#employment-type -->
                                    <employment_type_id>62</employment_type_id>
                                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#employment-status -->
                                    <employment_status_id>1</employment_status_id>
                                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#employment-sector -->
                                    <employment_sector_id>1</employment_sector_id>
                                    <gross_income>60000</gross_income>
                                    <income>45000</income>
                                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#income-frequency -->
                                    <income_frequency_id>6</income_frequency_id>
                                	 <occupation_id>0</occupation_id>
                                </item>
                            </employments>
                            <bank>
                                <account_name>Mr Testy Tester</account_name>
                                <account_number>91137018</account_number>
                                <sortcode>40-28-26</sortcode>
                                <bank>HSBC Bank PLC</bank>
                                <branch>Leicester</branch>
                                <address>Gallowtree Gate, Leicester</address>
                                <years>7</years>
                                <months>2</months>
                                <amex>false</amex>
                                <chequecard>false</chequecard>
                                <dinersclub>false</dinersclub>
                                <mastercard>true</mastercard>
                                <visa>true</visa>
                                <debit_card>true</debit_card>
                                <other>false</other>
                            </bank>
                            <!-- The affordability section is only required for sending to Black Horse -->
                            <affordability>
                                <replacement_loan>true</replacement_loan>
                                <sustainability>false</sustainability>
                                <monthly_mortgage>500</monthly_mortgage>
                                <other_expenditure>350</other_expenditure>
                            </affordability>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#applicant-type -->
                            <type_id>1</type_id>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#name-title -->
                            <name_title_id>1</name_title_id>
                            <forename>Testy</forename>
                            <middlename></middlename>
                            <surname>Tester</surname>
                            <alias></alias>
                            <maiden_name></maiden_name>
                            <dob>1967-02-19</dob>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#gender -->
                            <gender_type_id>1</gender_type_id>
                            <home_phone>01234567890</home_phone>
                            <mobile>07986543210</mobile>
                            <work_phone>01234098765</work_phone>
                            <email>testy@gmail.com</email>
                            <dependants>0</dependants>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#marital-status -->
                            <marital_status_id>1</marital_status_id>
                            <passport_id>0</passport_id>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#driving-license -->
                            <driving_license_id>1</driving_license_id>
                            <driving_license_number></driving_license_number>
                            <!-- Not required for private individuals -->
                            <relationship_type_id>0</relationship_type_id>
                            <optin_post>0</optin_post>
                            <optin_email>0</optin_email>
                            <optin_phone>0</optin_phone>
                            <optin_sms>0</optin_sms>
                            <uk_resident>true</uk_resident>
                        </item>
                    </applicant_section>
                    <vehicle_section>
                        <item>
                            <quotation_section>
                                <item>
                                    <options>
                                        <!-- Options are only valid for new vehicles -->
                                        <!-- Zero or more repetitions:-->
                                        <item>
                                            <description>Sat Nav</description>
                                            <net>200</net>
                                            <vat>40</vat>
                                        </item>
                                    </options>
                                    <accessories>
                                        <!-- Zero or more repetitions:-->
                                        <item>
                                            <description>Boot Liner</description>
                                            <net>50</net>
                                            <vat>10</vat>
                                            <accessory_type_id>0</accessory_type_id>
                                        </item>
                                    </accessories>
                                    <vaps>
                                        <!--Zero or more repetitions:-->
                                        <item>
                                            <!-- Provided on a per-client basis -->
                                            <vap_package_id>10</vap_package_id>
                                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#vap-types -->
                                            <vap_type_id>12</vap_type_id>
                                            <product>Super Shine</product>
                                            <premium>145</premium>
                                            <payment_id>1</payment_id>
                                            <net_cost>45</net_cost>
                                            <tax_rate>20</tax_rate>
                                            <vap_term>36</vap_term>
                                            <policy_number></policy_number>
                                        </item>
                                    </vaps>
                                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#finance-types -->
                                    <finance_type_id>1</finance_type_id>
                                    <vat_qualifying>false</vat_qualifying>
                                    <vehicle>8000</vehicle>
                                    <vehicle_vat>0</vehicle_vat>
                                    <vehicle_discount>0</vehicle_discount>
                                    <delivery>0</delivery>
                                    <delivery_vat>0</delivery_vat>
                                    <rfl>0</rfl>
                                    <rfl_term>12</rfl_term>
                                    <reg_fee>0</reg_fee>
                                    <part_ex>1500</part_ex>
                                    <settlement>0</settlement>
                                    <deposit>250</deposit>
                                    <deposit_sterling>0</deposit_sterling>
                                    <fda>0</fda>
                                    <!-- Income promise scheme -->
                                    <ips>0</ips>
                                    <!-- Either flat rate or APR may be specified -->
                                    <!-- flat_rate>?</flat_rate -->
                                    <apr_rate>12.9</apr_rate>
                                    <flat_rate>0</flat_rate>
                                    <!-- "Flat Rate" / "APR" -->
                                    <rate_type>APR</rate_type>
                                    <term>36</term>
                                    <!-- Can be passed in, or calculated in UI later -->
                                    <balloon>0</balloon>
                                    <!-- Not generally required, calculated at point of submission to lender -->
                                    <monthly_payment>0</monthly_payment>
                                    <annual_mileage>8000</annual_mileage>
                                    <!-- Will default to prevailing rate if omitted -->
                                    <vat_rate>20</vat_rate>
                                    <settlement_lender_id>0</settlement_lender_id>
                                    <px_reg_no/>
                                   
                                </item>
                            </quotation_section>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#vehicle-type -->
                            <vehicle_type_id>1</vehicle_type_id>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#vehicle-new-used -->
                            <vehicle_newused_id>2</vehicle_newused_id>
                            <asset_type_id>0</asset_type_id>
                            <make>PEUGEOT</make>
                            <model>308 DIESEL SW ESTATE</model>
                            <style>1.6 HDi 92 Active 5dr</style>
                            <engine_size>1590</engine_size>
                            <doors>5</doors>
                            <reg_no>LP14HNL</reg_no>
                            <first_reg>2014-06-30</first_reg>
                            <vin>VF3LC9HPAES114842</vin>
                            <colour>Grey</colour>
                            <year_man>2014</year_man>
                            <import>false</import>
                            <mileage>40000</mileage>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#fuel-types -->
                            <fuel_type_id>2</fuel_type_id>
                            <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#transmission-types -->
                            <transmission_type_id>1</transmission_type_id>
                            <capcode>PE3816AV 5EDTM  1   </capcode>
                            <!-- Leisure assets only -->
                            <glasses_code>0</glasses_code>
                            <glasses_code_family>0</glasses_code_family>
                            <!-- Non-vehicle assets only -->
                            <stock_number>0</stock_number>
                            <description>0</description>
                        </item>
                    </vehicle_section>
                    <notes>
                        <!--Zero or more repetitions:-->
                        <item>
                            <visible_to_lender>false</visible_to_lender>
                            <note>This is a proposal note that lenders will not see</note>
                        </item>
                    </notes>
                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#organisation-types -->
                    <application_type_id>0</application_type_id>
                    <!-- https://integrators.autoprotectgroup.co.uk/dt/enumerations.html#sales-type -->
                    <sales_type_id>1</sales_type_id>
                    <delivery_date>2018-07-05</delivery_date>
                </application_section>
                <authentication>
                    <!--You may enter the following 4 items in any order-->
                    <username>Provided by DealTrak</username>
                    <password>Provided by DealTrak</password>
                    <user_id>123456</user_id>
                    <dealer_id>123456</dealer_id>
                </authentication>
            </request>
        </app:load_application>
    </soapenv:Body>
</soapenv:Envelope>
```

## Response

```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="https://www.dealtrak123.co.uk/rosetta/webservices/application">
   <SOAP-ENV:Body>
      <ns1:load_applicationResponse>
         <load_applicationResult>
            <application_id>123457</application_id>
         </load_applicationResult>
      </ns1:load_applicationResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```
