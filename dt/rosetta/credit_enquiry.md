# Rosetta SOAP Credit Enquiry Module

:::warning
Equifax methods are described for historical information only. Experian is the currently supported soft search provider.
:::

## equifax_credit_enquiry

Performs an Equifax soft search on an existing proposal, using details already present on the prop.

## equifax_credit_enquiry_application

Performs an Equifax soft search on a new applicant and creates a proposal with the customer's details and the search result.

## equifax_credit_enquiry_band

Returns the dealer-defined band associated with a proposal that has already had an Equifax search performed.

## equifax_credit_enquiry_get_details

Returns the full details of a previous Equifax search associated with a proposal.

## experian_credit_enquiry

Performs an Experian soft search on an existing proposal.

## experian_credit_enquiry_application

Performs an Experian soft search on a new applicant and creates a proposal with the customer's details and the search result.

### Required inputs

| Element                                                              | Required | Description                                                                                   |
|----------------------------------------------------------------------|----------|-----------------------------------------------------------------------------------------------|
| request → title                                                      | Y        | Customer's title, using the ID from the [title enumeration](../enumerations.md#name-title)    |
| request → forename                                                   | Y        | Customer's forename                                                                           |
| request → middlename                                                 | N        | Customer's middle name                                                                        |
| request → surname                                                    | Y        | Customer's surname                                                                            |
| request → date_of_birth                                              | Y        | Customer's birth date in YYYY-MM-DD format                                                    |
| request → flat <br> request → house_number <br> request → house_name | Y        | Customer's flat number, house number or house name (minimum of one required)                  |
| request → street                                                     | Y        | Customer's street name                                                                        |
| request → district                                                   | N        | Customer's district                                                                           |
| request -> towncity                                                  | Y        | Customer's town or city                                                                       |
| request → county                                                     | N        | Customer's county                                                                             |
| request → postcode                                                   | Y        | Customer's postal code                                                                        |
| request → application_id                                             | N        | Existing proposal ID to perform a search on, but using different details                      |
| request → authentication → username                                  | Y        | API username                                                                                  |
| request → authentication → password                                  | Y        | API password                                                                                  |
| request → authentication → user_id                                   | Y        | User ID to associate the newly created proposal with (typically a dedicated "web leads" user) |
| request → authentication → dealer_id                                 | Y        | Branch ID to associate the newly created proposal with                                        |

### Response format

| Element                                  | Description                                                     |
|------------------------------------------|-----------------------------------------------------------------|
| response → application_id                | The newly created proposal ID, which will be in "New Proposals" |
| response → credit_enquiry → band         | One of: Very Poor / Poor / Fair / Good / Excellent              |
| response → credit_enquiry → success_flag | 0 for an unsuccessful search, 1 for a successful search         |
| response → error → code                  | The alphanumeric error code, if applicable                      |
| response → error → message               | A human-readable error message                                  |
