# Rosetta SOAP Application Module

The Application module allows a 3rd party application to create and edit a finance application within DealTrak.

For the expected field types, please refer to the auto-generated documentation here: https://www.dealtrak123.co.uk/rosetta/webservices/application

For numeric values, please refer to the [system-wide enumerations](../enumerations.md).

## Application level functions

* create_application

Creating an application allocates an application ID. Subsequent calls must be made to fully populate it.

* edit_application
* get_application

Editing an application permits modification of the sales agent, introducer and dealer contacts and other application parameters. The same details may be retrieved with the get_application function.

* load_application

Load application permits and entire finance application to be loaded into DealTrak in one go. **Please note: This is the simplest and preferred method for most use cases.**

You should start with the [example private individual request](examples/loadApplication.md)
or  [example business request](examples/loadApplicationBusiness.md) if you do not intend to use a SOAP client library.

* get_application_details

Returns a complete snapshot of the application in its current state.

* edit_proposal

Permits a 3rd party application to update the [status](../enumerations.md#proposal-status) of the application. For example, if the application is no longer required, it should be set to "Cancelled" status.

## Applicant functions

* add_applicant
* edit_applicant
* get_applicant
* remove_applicant

The applicant functions allow creation, editing and removal of the applicants associated with a proposal. Multiple applicants are identified by sequence ID, with 1 being the primary applicant.

When creating a business application, be sure to set the applicant type ID to the correct value to indicate the applicant's role within the business, eg: director, company secretary, partner etc.

### Residence functions

* add_residence
* edit_residence
* get_residence
* remove_residence

### Employments functions

* add_employment
* edit_employment
* get_employment
* remove_employment

Business applicants do not require employments to be added.

## Bank functions

* add_bank
* edit_bank
* get_bank
* remove_bank

An applicant should have a maximum of one bank account. Many lenders require bank details in order to process an application.

Business applications do not require applicant bank details, as the company bank details are contained with the organisation object.

## Document upload

* add_document

Documents, typically PDFs, may be uploaded to the application's documents area. The data should be provided base64 encoded.

## Business application functions

* add_organisation
* edit_organisation
* get_organisation
* remove_organisation

## Vehicle functions

* add_vehicle
* edit_vehicle
* get_vehicle
* remove_vehicle

An application will always require a vehicle to be added, although the [[fsuk:enumerations#vehicle_type|vehicle types]] permit the addition of no specific vehicle by giving a vehicle type in conjunction with a new/used type of "No Vehicle".

More than one vehicle per application is not currently supported.

For road vehicles, most lenders require a CAP code. This can be obtained from the vehicle registration via the vrm_lookup call in the [[fsuk:rosetta:vehicledata|vehicle data]] module or for new vehicles via the list_makes, list_models, list_styles calls, also in the vehicle data module.

Non-vehicle assets may also be financed by setting the vehicle type to "other" and sending an appropriate asset code.

## Quotation functions

* add_quotation
* edit_quotation
* get_quotation
* remove_quotation

Vehicles require a quotation, which holds the figures relating to the sale, particularly:

* Rate and rate type (eg: APR)
* Sale price, deposits, settlement.
* Finance type

## Vehicle options and accessories

* add_vehicle_option
* edit_vehicle_option
* get_vehicle_option
* remove_vehicle_option

* add_vehicle_accessory
* edit_vehicle_accessory
* get_vehicle_accessory
* remove_vehicle_accessory

Options and accessories may be added to the vehicle. Options are only valid for new vehicles, whereas accessories can be added for both new and used vehicles.

## VAPs

* add_vehicle_vap
* edit_vehicle_vap
* get_vehicle_vap
* remove_vehicle_vap

Insurance and non-insurance products and services can be added to a quotation. The correct [[fsuk:enumerations#vap_types|VAP type]] must be specified, from which the product tax type is inferred.

Lenders typically treat insurance products differently from non-insurance products (such as a dealer-backed guarantee). Products may be required to be disbursed in some cases.

## Application notes

* add_application_note
* edit_application_note
* get_application_note
* remove_application_note

Notes can be added to and removed from applications. Notes where the visible_to_lender flag is set may be passed through to underwriters, where a lender supports it.