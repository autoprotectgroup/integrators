# Load Lead

## Example Request


```xml
<?xml version="1.0" encoding="utf-8"?>
<load_lead>
  <header_section>
    <user>123456</user>
    <client_branch>123456</client_branch>
    <client_reference>uniquereference0001</client_reference>
    <timestamp>2015-11-13T12:12:12</timestamp>
  </header_section>
  <application_section>
    <applicant_section sequence="1">
      <details_section>
        <title>1</title>
        <forename>John</forename>
        <surname>Doe</surname>
        <dob>1981-05-03</dob>
        <gender>1</gender>
        <home_phone>01924356027</home_phone>
        <mobile>07700900555</mobile>
        <work_phone>01924648800</work_phone>
        <email>testint@frontline-solutions.co.uk</email>
        <marital_status>1</marital_status>
        <driving_license>1</driving_license>
        <relationship>1</relationship>
        <uk_resident>1</uk_resident>
      </details_section>
      <residence_section sequence="1">
        <house_number>1</house_number>
        <street>Red Hall Court</street>
        <towncity>Wakefield</towncity>
        <county>West Yorkshire</county>
        <postcode>WF1 2UY</postcode>
        <years>6</years>
        <months>6</months>
        <residential_status>1</residential_status>
        <uk_address>1</uk_address>
        <validated>1</validated>
      </residence_section>
      <employment_section sequence="1">
        <occupation>Software Engineer</occupation>
        <company>Frontline Solutions UK Ltd</company>
        <building>Unit 12</building>
        <street>Red Hall Court</street>
        <towncity>Wakefield</towncity>
        <county>West Yorkshire</county>
        <postcode>WF1 2UY</postcode>
        <years>10</years>
        <months>0</months>
        <employment_status>1</employment_status>
        <employment_sector>1</employment_sector>
        <gross_income>100000</gross_income>
        <income>80000</income>
        <income_freq>6</income_freq>
        <uk_trading_address>1</uk_trading_address>
      </employment_section>
      <bank_section>
        <account_name>MR TEST CASE</account_name>
        <account_number>91137018</account_number>
        <sortcode>40-28-26</sortcode>
        <bank_name>HSBC</bank_name>
        <branch>Leicester, Uppingham Road</branch>
        <address>289 Uppingham Road, Leicester, LE5 4DG</address>
        <years>10</years>
        <months>0</months>
      </bank_section>
    </applicant_section>
    <vehicle_section sequence="1">
      <new_used>2</new_used>
      <vehicle_type>1</vehicle_type>
			<make>MERCEDES-BENZ</make>
      <model>C CLASS DIESEL SALOON</model>
      <style>C250 BlueTEC SE Executive 4dr Auto</style>
      <reg_no>CE15VMU</reg_no>
      <first_reg>2015-03-31</first_reg>
      <vin>WDD2050082R021022</vin>
      <capcode>MECC2125X4SDTA  5</capcode>
      <colour>Black</colour>
      <engine_size>2143</engine_size>
      <doors>4</doors>
      <fuel_type>2</fuel_type>
      <transmission_type>2</transmission_type>
      <year_man>2015</year_man>
      <mileage>9000</mileage>
      <stock_number>123456</stock_number>
      <delivery_date>2015-11-15</delivery_date>
      <quotation_section sequence="1">
        <finance_type>1</finance_type>
        <vat_qualifying>0</vat_qualifying>
        <vehicle>29500</vehicle>
        <delivery>0</delivery>
        <rfl>0</rfl>
        <reg_fee>0</reg_fee>
        <part_ex>3000</part_ex>
        <settlement>0</settlement>
        <deposit>1200</deposit>
        <rate_type>Flat Rate</rate_type>
        <flat_rate>5</flat_rate>
        <term>36</term>
        <vat_rate>20</vat_rate>
      </quotation_section>
    </vehicle_section>
    <extras_section />
  </application_section>
</load_lead>
```