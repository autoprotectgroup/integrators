# DealTrak Enumerations

## Sales Type

| ID  | Type       |
|-----|------------|
| 1   | Retail     |
| 2   | Fleet      |
| 3   | Motability |
| 4   | Trade      |

## Organisation Types

| ID  | Type                          |
|-----|-------------------------------|
| 0   | Private Individual            |
| 1   | Private Limited Company       |
| 2   | Public Limited Company        |
| 3   | Partnership                   |
| 4   | Sole Trader                   |
| 6   | Limited Liability Partnership |

## Name Title

| ID   | Title |
|:-----|:------|
| 1    | Mr    |
| 2    | Mrs   |
| 3    | Ms    |
| 4    | Miss  |
| 5    | Dr    |

## Gender

| ID   | Gender |
|:-----|:-------|
| 1    | Male   |
| 2    | Female |

## Applicant Type

| ID  | Type  |
|-----|-------|
| 1   | Hirer |
| 2   | Joint |


## Marital Status

| ID  | Type           |
|-----|----------------|
| 1   | Single         |
| 2   | Married        |
| 3   | Common Law     |
| 4   | Civil Partners |
| 5   | Separated      |
| 6   | Divorced       |
| 7   | Widowed        |

## Driving License

| ID  | Type        |
|-----|-------------|
| 1   | Full        |
| 2   | Non-UK      |
| 3   | Provisional |
| 4   | None        |
| 5   | EU          |

## Passport Types

| ID  | Type                   |
|-----|------------------------|
| 1   | UK Passport            |
| 2   | UK Citizen No Passport |
| 3   | EU Passport            |
| 4   | Other                  |

## Relationship Types

| ID  | Type        |
|-----|-------------|
| 1   | Parent      |
| 2   | Spouse      |
| 3   | Partner     |
| 4   | Sibling     |
| 5   | Friend      |
| 6   | Grandparent |
| 7   | Child       |
| 8   | Grandchild  |
| 9   | Other       |
| 10  | Partner     |
| 11  | Director    |
| 12  | Sole Trader |

## Nationality

[Full list of nationalities](nationalities.md)

## Residential Status

| ID  | Type                |
|-----|---------------------|
| 1   | Home Owner          |
| 2   | Co-Home Owner       |
| 3   | Council Tenant      |
| 4   | Housing Association |
| 5   | Living With Parents |
| 6   | Private Tenant      |

## Business Tenure Types

| ID  | Type      |
|-----|-----------|
| 1   | Leased    |
| 2   | Licensed  |
| 3   | Mortgaged |
| 4   | Owned     |



## Employment Sector

| ID  | Type           |
|-----|----------------|
| 1   | Private        |
| 2   | Government     |
| 3   | Military       |
| 4   | Self Employed  |
| 5   | Student        |
| 6   | Home Maker     |
| 7   | Retired        |
| 8   | Unable to Work |
| 9   | Unemployed     |

## Employment Type

[Full list of employment types](employment.md)

## Occupations

[Full list of occupations](occupations.md)


## Employment Status

| ID  | Status              |
|-----|---------------------|
| 1   | Full Time Permanent |
| 2   | Part Time Permanent |
| 3   | Full Time Temporary |
| 4   | Part Time Temporary |
| 5   | Full Time Agency    |
| 6   | Part Time Agency    |
| 7   | Sub-Contractor      |
| 8   | Self Employed       |
| 9   | Unemployed          |
| 10  | Retired             |
| 11  | Student             |
| 12  | Home Maker          |
| 13  | Unable to Work      |

## Income Frequency

| ID  | Frequency   |
|-----|-------------|
| 1   | Weekly      |
| 2   | Fortnightly |
| 3   | Four Weekly |
| 4   | Monthly     |
| 5   | Quarterly   |
| 6   | Annually    |



## Finance Types

Note that HP and PCP are more fully supported than other types, which are present for compatibility and reporting purposes.

| ID  | Type                 |
|-----|----------------------|
| 1   | Hire Purchase        |
| 2   | PCP                  |
| 3   | Motor Loan           |
| 4   | Lease Purchase       |
| 6   | Savings              |
| 7   | Equity Release       |
| 8   | Secured Loan         |
| 9   | Direct Loan          |
| 10  | Manufacturers Scheme |
| 11  | Contract Hire        |
| 12  | Lease                |
| 13  | Finance Lease        |
| 14  | Cash                 |
| 15  | Motability Scheme    |
| 16  | Variable Rate        |
| 17  | Top Up Loan          |
| 18  | Fast Proposal        |
| 19  | Non Qualifier        |
| 20  | Back Sold Product    |

## Vehicle Type

| ID  | Type                        |
|-----|-----------------------------|
| 1   | Private Light Goods Vehicle |
| 2   | Light Commercial Vehicle    |
| 3   | Caravan                     |
| 4   | Motor Home                  |
| 5   | Motor Bike                  |
| 7   | Heavy Goods Vehicle         |
| 6   | Other                       |

## Vehicle New/Used

| ID  | Type           |
|-----|----------------|
| 1   | New            |
| 2   | Used           |
| 3   | Credit Line    |
| 4   | Pre-Registered |
| 5   | Demonstrator   |
| 6   | No vehicle     |


## Fuel Types

| ID  | Type                          |
|-----|-------------------------------|
| 1   | Petrol                        |
| 2   | Diesel                        |
| 3   | Petrol/LPG                    |
| 4   | Petrol/CNG                    |
| 5   | Electric                      |
| 6   | Petrol/Electric Hybrid        |
| 7   | Petrol/Bio-Ethanol (E85)      |
| 8   | Diesel/Electric Hybrid        |
| 9   | Petrol/PlugIn Electric Hybrid |
| 10  | Diesel/PlugIn Electric Hybrid |

## Transmission Types

| ID  | Type           |
|-----|----------------|
| 1   | Manual         |
| 2   | Automatic      |
| 3   | Semi-Automatic |


## VAP Types

| ID  | Type                   | Tax Type |
|-----|------------------------|----------|
| 2   | Finance GAP            | IPT      |
| 3   | Warranty               | IPT      |
| 4   | MOT Insurance          | IPT      |
| 5   | Tyre Insurance         | IPT      |
| 6   | Key Insurance          | IPT      |
| 8   | Breakdown Cover        | IPT      |
| 9   | Smart Insurance        | IPT      |
| 10  | Combined GAP & VRI     | IPT      |
| 11  | Guarantee              | VAT      |
| 12  | Paint Protection       | VAT      |
| 15  | Service Plan           | VAT      |
| 16  | Alloy Wheel Protection | IPT      |
| 24  | Smart Protection       | VAT      |
| 25  | Tyre Protection        | VAT      |

## VAP Suppliers

TBC

## Destinations

TBC

## Proposal Status

| ID  | Name                          |
|-----|-------------------------------|
| 1   | New Proposal                  |
| 2   | Unsent Proposal               |
| 3   | Awaiting Decision             |
| 4   | Awaiting Delivery             |
| 5   | Docs Sent to Lender           |
| 6   | Document Error                |
| 7   | Paid Out (Delivered)          |
| 8   | Declined All Lenders          |
| 9   | Cancelled                     |
| 10  | Unsuitable Offer              |
| 11  | Referred To Broker            |
| 12  | Awaiting Invoice              |
| 13  | Awaiting Quote                |
| 14  | Paid Out (Not Delivered)      |
| 15  | Docs Sent To Customer         |
| 16  | New Order                     |
| 17  | Delivered (Not Paid Out)      |
| 18  | Sourcing Vehicle              |
| 21  | Awaiting Dealer               |
| 22  | Awaiting Customer             |
| 23  | Appointed At Dealership       |
| 24  | Awaiting Income & Expenditure |
| 25  | Awaiting Customer Contact     |
| 26  | Loan Provisionally Approved   |

The ability to create custom proposal statuses can be enabled. These always have status IDs above 10000.

## Destination Status

| Code | Indicates            |
|------|----------------------|
| 1    | Unknown              |
| 2    | Approved             |
| 3    | Conditional Approval |
| 4    | Referred             |
| 5    | Declined             |
| 6    | Cancelled            |
| 7    | NTU                  |
| 8    | Error                |
| 9    | Received OK          |
| 10   | Info Required        |
| 11   | Docs Received        |
| 12   | Paid Out             |
| 13   | Resubmit Required    |

## Accessory Types

| ID  | Type         |
|-----|--------------|
| 1   | Fuel         |
| 2   | Admin Fee    |
| 3   | Delivery Fee |

