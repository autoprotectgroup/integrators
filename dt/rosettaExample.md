# Rosetta example request

```xml
<?xml version="1.0" encoding="UTF-8"?>
<rosetta live="1">
  <routing>
    <destination>6</destination>
    <client>17</client>
    <branch_id>123467</branch_id>
    <contact_forename>Bob</contact_forename>
    <contact_surname>Salesman</contact_surname>
    <contact_phone>01174960567</contact_phone>
    <contact_email>bob.salesman@abc-carsales.co.uk</contact_email>
    <identifier>56888702</identifier>
    <username>abc-carsales</username>
    <password>n1cep4sswd</password>
    <client_reference>420317</client_reference>
    <source_reference>ATV293489</source_reference>
    <proposal_created_at>2019-03-27 18:03:18</proposal_created_at>
    <created_at>2019-03-27T18:05:30+00:00</created_at>
  </routing>
  <application>
    <applicant_section sequence="1" type="1">
      <details_section>
        <title>4</title>
        <forename>Shirley</forename>
        <middlename/>
        <surname>Temple</surname>
        <alias/>
        <maiden_name/>
        <dob>1928-04-23</dob>
        <gender>2</gender>
        <home_phone>01174960133</home_phone>
        <mobile>07700900676</mobile>
        <work_phone/>
        <email>shirleyt@gmail.com</email>
        <dependants>2</dependants>
        <marital_status>3</marital_status>
        <passport>1</passport>
        <driving_license>1</driving_license>
        <driving_license_number/>
        <nationality>1</nationality>
        <distance_marketed>1</distance_marketed>
        <optin_post>0</optin_post>
        <optin_email>0</optin_email>
        <optin_phone>0</optin_phone>
        <optin_sms>0</optin_sms>
        <uk_resident>1</uk_resident>
      </details_section>
      <residence_section sequence="1">
        <flat/>
        <house_name/>
        <house_number>38</house_number>
        <street>Hazelmere Close</street>
        <district/>
        <towncity>Gloucester</towncity>
        <county>Gloucestershire</county>
        <postcode>GL4 6WN</postcode>
        <years>10</years>
        <months>0</months>
        <residential_status>4</residential_status>
        <uk_address>1</uk_address>
      </residence_section>
      <employment_section sequence="1">
        <occupation>Bus Conductor</occupation>
        <occupation_id/>
        <company>Bristol Buses</company>
        <building>Bus Depot</building>
        <subbuilding/>
        <street>Westgate Retail Park</street>
        <district/>
        <towncity>Bristol</towncity>
        <county>Brustol</county>
        <postcode>BS1 2RU</postcode>
        <years>8</years>
        <months>0</months>
        <employment_status>1</employment_status>
        <employment_sector>1</employment_sector>
        <employment_type>0</employment_type>
        <gross_income>40000.00</gross_income>
        <income>0.00</income>
        <income_freq>6</income_freq>
        <uk_trading_address>1</uk_trading_address>
      </employment_section>
      <affordability_section>
        <replacement_loan>0</replacement_loan>
        <sustainability>0</sustainability>
        <monthly_mortgage>280</monthly_mortgage>
        <other_expenditure>0</other_expenditure>
      </affordability_section>
      <bank_section>
        <account_name>Miss S Temple</account_name>
        <account_number>14457846</account_number>
        <sortcode>30-80-88</sortcode>
        <bankname>LLOYDS BANK PLC</bankname>
        <branch>Bristol High St</branch>
        <address>High Street, Bristol, BS1 1BN</address>
        <years>15</years>
        <months>0</months>
        <amex>0</amex>
        <cheque_card>0</cheque_card>
        <diners_club>0</diners_club>
        <master_card>0</master_card>
        <visa>1</visa>
        <debit_card>1</debit_card>
        <other>0</other>
        <direct_debit>1</direct_debit>
      </bank_section>
    </applicant_section>
    <vehicle_section>
      <newused>2</newused>
      <make>FORD</make>
      <model>KUGA DIESEL ESTATE</model>
      <engine_size>2000</engine_size>
      <style>2.0 TDCi 180 Titanium 5dr</style>
      <asset_type_id/>
      <description/>
      <doors>5</doors>
      <reg_no>VA16AUX</reg_no>
      <first_reg>2016-07-29</first_reg>
      <vin>WF0AXXWPMAFJ47773</vin>
      <capcode>FOKG20T805EDTM4 1</capcode>
      <trade_value/>
      <retail_value/>
      <glasses_code/>
      <glasses_code_family/>
      <colour>Orange</colour>
      <fuel>2</fuel>
      <transmission>1</transmission>
      <usage>0</usage>
      <year_man>2016</year_man>
      <import>0</import>
      <mileage>16187</mileage>
      <stock_number>2136924</stock_number>
      <delivery_date>2019-03-29</delivery_date>
      <type>1</type>
    </vehicle_section>
    <finance_section>
      <finance_type>2</finance_type>
      <vat_qualifying>0</vat_qualifying>
      <interest_method>APR</interest_method>
      <acceptance_fee>0.00</acceptance_fee>
      <acceptance_method>up front</acceptance_method>
      <acceptance_fee_interest_charged>1</acceptance_fee_interest_charged>
      <closure_fee>0.00</closure_fee>
      <closure_method>at end</closure_method>
      <closure_fee_interest_charged>1</closure_fee_interest_charged>
      <vehicle>15000.00</vehicle>
      <vehicle_vat>0.00</vehicle_vat>
      <options>0.00</options>
      <options_vat>0.00</options_vat>
      <accessories>16.66</accessories>
      <accessories_vat>3.34</accessories_vat>
      <delivery>0.00</delivery>
      <delivery_vat>0.00</delivery_vat>
      <guarantee>0.00</guarantee>
      <guarantee_vat>0.00</guarantee_vat>
      <rfl>140.00</rfl>
      <rfl_term>0</rfl_term>
      <reg_fee>0.00</reg_fee>
      <fhbr/>
      <rob/>
      <profile1/>
      <profile2/>
      <profile3/>
      <profile1_payment/>
      <profile2_payment/>
      <profile3_payment/>
      <payment_frequency>12</payment_frequency>
      <monthly_payment/>
      <reg_deal_type/>
      <opted_out>0</opted_out>
      <vaps>
        <vap>
          <product_id>12</product_id>
          <supplier_id>3</supplier_id>
          <product_code>Paint Protection</product_code>
          <product>Diamondbrite</product>
          <premium>299.00</premium>
          <ipt>20.00</ipt>
          <vap_term>0</vap_term>
          <tax_type>VAT</tax_type>
        </vap>
        <vap>
          <product_id>5</product_id>
          <supplier_id>3</supplier_id>
          <product_code>Tyre Insurance</product_code>
          <product>Tyre &amp; Alloy 200 pound  single claim limit-36mth</product>
          <premium>249.00</premium>
          <ipt>20.00</ipt>
          <vap_term>0</vap_term>
          <tax_type>IPT</tax_type>
        </vap>
        <vap>
          <product_id>11</product_id>
          <supplier_id>3</supplier_id>
          <product_code>Guarantee</product_code>
          <product>Motor Assured 36 Month Premium Warranty</product>
          <premium>799.00</premium>
          <ipt>20.00</ipt>
          <vap_term>36</vap_term>
          <tax_type>VAT</tax_type>
        </vap>
      </vaps>
      <part_ex>600.00</part_ex>
      <settlement>0.00</settlement>
      <deposit>1500.00</deposit>
      <deposit_sterling>0.00</deposit_sterling>
      <income_promise_scheme>0</income_promise_scheme>
      <fda>0</fda>
      <rate_type>APR</rate_type>
      <flat_rate>6.19</flat_rate>
      <apr_rate>11.9</apr_rate>
      <term>48</term>
      <payment_frequency>12</payment_frequency>
      <pause_type>None</pause_type>
      <balloon>6365.00</balloon>
      <annual_mileage>7000</annual_mileage>
      <vat_rate>20</vat_rate>
      <finance_package>VMI</finance_package>
      <finance_package_id>1237276</finance_package_id>
      <ltv>0.960787</ltv>
      <ltv_glass>0</ltv_glass>
      <credit_score/>
    </finance_section>
    <extras_section>
      <notes>
		<note>This is a helpful note.</note>
      </notes>
      <data>
        <field>nationality</field>
        <value>British</value>
      </data>
    </extras_section>
  </application>
</rosetta>
```