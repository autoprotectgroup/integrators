# API services for finance providers

Requests are sent to an endpoint of your choice. HTTP auth can be enabled, as can mutual TLS authentication.
Contact us to discuss additional authentication mechanisms such as OAuth etc.

## Example requests

Here is an example of a private individual proposal: [rosetta_example.xml](rosettaExample.md)

An example response is detailed [here](rosettaExampleResponse.md)

## Structure

The structure of a private individual proposal is shown below. Examples are given in fields that are typically completed on a normal proposal.

A base level of validation is applied, but specific validation can be implemented on request.

### /rosetta

An attribute named "live" indicates whether the proposal has originated from a system configured to be live (1) or test (0).

| Node        | Contents      | Notes                     |
|-------------|---------------|---------------------------|
| routing     | <routing>     | Proposal metadata         |
| application | <application> | The main proposal details |

#### /rosetta/application

| Node              | Contents                                  | Notes                                            |
|-------------------|-------------------------------------------|--------------------------------------------------|
| applicant_section | <applicant_section sequence="1" type="1"> | One or more applicants                           |
| vehicle_section   | <vehicle_section>                         | Exactly one vehicle section                      |
| finance_section   | <finance_section>                         | Exactly one finance section                      |
| extras_section    | <extras_section>                          | Providing optional notes and extra data sections |

#### /rosetta/routing

| Node                | Example                         | Notes                                                 |
|---------------------|---------------------------------|-------------------------------------------------------|
| destination         | 6                               | Assigned by DealTrak [See List](destinations.md)      |
| client              | 17                              | Internal use                                          |
| branch_id           | 123467                          | Internal use                                          |
| contact_forename    | Bob                             | Details of the DealTrak user who sent the proposal .  |
| contact_surname     | Salesman                        |                                                       |
| contact_phone       | 01174960567                     |                                                       |
| contact_email       | bob.salesman@abc-carsales.co.uk |                                                       |
| identifier          | 56888702                        | Identifies the branch/rooftop, assigned by the lender |
| username            | abc-carsales                    | Assigned by the lender                                |
| password            | n1cep4sswd                      | Assigned by the lender                                |
| client_reference    | 420317                          | DealTrak proposal number, unique with dealer group    |
| source_reference    | ATV293489                       | External reference, from a DMS for example            |
| proposal_created_at | 2019-03-27 18:03:18             | Proposal creation time                                |
| created_at          | 2019-03-27T18:05:30+00:00       | Proposal send time                                    |

The proposal can be uniquely identified by the **client** and **client_reference** taken together.
#### /rosetta/application/applicant_section

Multiple applicant sections may be provided. The following attributes are also required:

| Attribute | Example | Notes                                      |
|-----------|---------|--------------------------------------------|
| sequence  | 1       | Monotonically increasing, starting at 1    |
| type      | 1       | [See List](enumerations.md#applicant-type) |

| Node                  | Example                           | Notes                                                                                             |
|-----------------------|-----------------------------------|---------------------------------------------------------------------------------------------------|
| details_section       | <details_section>                 | See below - the basic applicant details                                                           |
| residence_section     | <residence_section sequence="1">  | Multiple sections may be provided, increasing sequence numbers                                    |
| employment_section    | <employment_section sequence="1"> | Multiple sections may be provided, increasing sequence numbers                                    |
| affordability_section | <affordability_section>           | Relevant to Black Horse only                                                                      |
| bank_section          | <bank_section>                    | Only one to be provided, this is the account that the agreement direct debit should be drawn from |

#### /rosetta/application/applicant_section/details_section

| Node                   | Example            | Notes                                       |
|------------------------|--------------------|---------------------------------------------|
| title                  | 4                  | [See List](enumerations.md#name-title)      |
| forename               | Shirley            |                                             |
| middlename             |                    |                                             |
| surname                | Temple             |                                             |
| alias                  |                    |                                             |
| maiden_name            |                    |                                             |
| dob                    | 1928-04-23         |                                             |
| gender                 | 2                  | [See List](enumerations.md#gender)          |
| home_phone             | 01174960133        |                                             |
| mobile                 | 07700900676        |                                             |
| work_phone             |                    |                                             |
| email                  | shirleyt@gmail.com |                                             |
| dependants             | 2                  |                                             |
| marital_status         | 3                  | [See List](enumerations.md#marital-status)  |
| passport               | 1                  | Boolean                                     |
| driving_license        | 1                  | [See List](enumerations.md#driving-license) |
| driving_license_number |                    | 18 digits including issue number            |
| nationality            | 1                  | [See List](nationalities.md)                |
| distance_marketed      | 1                  | Boolean                                     |
| optin_post             | 0                  | Deprecated since GDPR, will always be zero  |
| optin_email            | 0                  | Deprecated since GDPR, will always be zero  |
| optin_phone            | 0                  | Deprecated since GDPR, will always be zero  |
| optin_sms              | 0                  | Deprecated sine GDPR, will always be zero   |
| uk_resident            | 1                  | Boolean                                     |


#### /rosetta/application/applicant_section/residence_section

| Node               | Example         | Notes                                          |
|--------------------|-----------------|------------------------------------------------|
| flat               |                 |                                                |
| house_name         |                 |                                                |
| house_number       | 38              |                                                |
| street             | Hazelmere Close |                                                |
| district           |                 |                                                |
| towncity           | Gloucester      |                                                |
| county             | Gloucestershire |                                                |
| postcode           | GL4 6WN         |                                                |
| years              | 10              |                                                |
| months             | 0               |                                                |
| residential_status | 4               | [See List](enumerations.md#residential-status) |
| uk_address         | 1               | Boolean                                        |

At least one of flat / house_name / house number is required by the base validation.

#### /rosetta/application/applicant_section/employment_section

| Node               | Example              | Notes                                         |
|--------------------|----------------------|-----------------------------------------------|
| occupation         | Bus Conductor        |                                               |
| occupation_id      |                      | [See List](occupations.md)                    |
| company            | Bristol Buses        |                                               |
| building           | Bus Depot            |                                               |
| subbuilding        |                      |                                               |
| street             | Westgate Retail Park |                                               |
| district           |                      |                                               |
| towncity           | Bristol              |                                               |
| county             | Brustol              |                                               |
| postcode           | BS1 2RU              |                                               |
| years              | 8                    |                                               |
| months             | 0                    |                                               |
| employment_status  | 1                    | [See List](enumerations.md#employment-status) |
| employment_sector  | 1                    | [See List](enumerations.md#employment-sector) |
| employment_type    | 0                    | [See List](enumerations.md#employment-type)   |
| gross_income       | 40000.00             | Before tax                                    |
| income             | 0.00                 | After tax                                     |
| income_freq        | 6                    | [See List](enumerations.md#income-frequency)  |
| uk_trading_address | 1                    | Boolean                                       |


#### /rosetta/application/applicant_section/affordability_section

| Node              | Example | Notes |
|-------------------|---------|-------|
| replacement_loan  | 0       |       |
| sustainability    | 0       |       |
| monthly_mortgage  | 280     |       |
| other_expenditure | 0       |       |

This section relates to Black Horse **only**.

#### /rosetta/application/applicant_section/bank_section

| Node           | Example                       | Notes   |
|----------------|-------------------------------|---------|
| account_name   | Miss S Temple                 |         |
| account_number | 14457846                      |         |
| sortcode       | 30-80-88                      |         |
| bankname       | LLOYDS BANK PLC               |         |
| branch         | Bristol High St               |         |
| address        | High Street, Bristol, BS1 1BN |         |
| years          | 15                            |         |
| months         | 0                             |         |
| amex           | 0                             | Boolean |
| cheque_card    | 0                             | Boolean |
| diners_club    | 0                             | Boolean |  
| master_card    | 0                             | Boolean |
| visa           | 1                             | Boolean |
| debit_card     | 1                             | Boolean |
| other          | 0                             | Boolean |
| direct_debit   | 1                             | Boolean |


#### /rosetta/application/vehicle_section

| Node                | Example                   | Notes                                                |
|---------------------|---------------------------|------------------------------------------------------|
| newused             | 2                         | [See List](enumerations.md#vehicle-newused)          |
| make                | FORD                      |                                                      |
| model               | KUGA DIESEL ESTATE        |                                                      |
| engine_size         | 2000                      |                                                      |
| style               | 2.0 TDCi 180 Titanium 5dr |                                                      |
| asset_type_id       |                           | [See List](assetType.md) - non-vehicular assets only |
| description         |                           | Non-vehicular assets only                            |
| doors               | 5                         |                                                      |
| reg_no              | VA16AUX                   |                                                      |
| first_reg           | 2016-07-29                |                                                      |
| vin                 | WF0AXXWPTGFJ47773         |                                                      |
| capcode             | FOKG20T805EDTM4 1         |                                                      |
| trade_value         |                           |                                                      |
| retail_value        |                           |                                                      |
| glasses_code        |                           |                                                      |
| glasses_code_family |                           |                                                      |
| colour              | Orange                    |                                                      |
| fuel                | 2                         | [See List](enumerations.md#fuel-types)               |
| transmission        | 1                         | [See List](enumerations.md#transmission-types)       |
| usage               | 0                         |                                                      |
| year_man            | 2016                      |                                                      |
| import              | 0                         |                                                      |
| mileage             | 16187                     |                                                      |
| stock_number        | 2136924                   |                                                      |
| delivery_date       | 2019-03-29                |                                                      |
| type                | 1                         | [See List](enumerations.md#vehicle-type)             |


#### /rosetta/application/finance_section

| Node                            | Example     | Notes                                                                                     |
|---------------------------------|-------------|-------------------------------------------------------------------------------------------|
| finance_type                    | 2           | [See List](enumerations.md#finance-types)                                                 |
| vat_qualifying                  | 0           | Used vehicles only                                                                        |
| interest_method                 | APR         | "APR" / "Flat Rate"                                                                       |
| acceptance_fee                  | 0.00        |                                                                                           |
| acceptance_method               | up front    |                                                                                           |
| acceptance_fee_interest_charged | 1           | Boolean                                                                                   |
| closure_fee                     | 0.00        |                                                                                           |
| closure_method                  | at end      |                                                                                           |
| closure_fee_interest_charged    | 1           | Boolean                                                                                   |
| vehicle                         | 15000.00    |                                                                                           |
| vehicle_vat                     | 0.00        |                                                                                           |
| options                         | 0.00        |                                                                                           |
| options_vat                     | 0.00        |                                                                                           |
| accessories                     | 16.66       |                                                                                           |
| accessories_vat                 | 3.34        |                                                                                           |
| delivery                        | 0.00        |                                                                                           |
| delivery_vat                    | 0.00        |                                                                                           |
| guarantee                       | 0.00        |                                                                                           |
| guarantee_vat                   | 0.00        |                                                                                           |
| rfl                             | 140.00      |                                                                                           |
| rfl_term                        | 0           | In months                                                                                 |
| reg_fee                         | 0.00        |                                                                                           |
| fhbr                            |             | Deprecated                                                                                |
| rob                             |             | Deprecated                                                                                |
| profile1                        |             | Finance lease only, manually entered by user                                              |
| profile2                        |             |                                                                                           |
| profile3                        |             |                                                                                           |
| profile1_payment                |             | Finance lease only, manually entered by user                                              |
| profile2_payment                |             |                                                                                           |
| profile3_payment                |             |                                                                                           |
| payment_frequency               | 12          | Payments per year, ie: 12 = monthly                                                       |
| monthly_payment                 |             | We will not generally populate this, as the lender would normally calculate from the rate |
| reg_deal_type                   |             |                                                                                           |
| opted_out                       | 0           |                                                                                           |
| **options_section**             | <option>    | Optional, may contain one or more **option** sections detailed below                      |
| **accessories_section**         | <accessory> | Optional, may contain one or more **accessory** sections detailed below                   |
| **vaps**                        | <vap>       | Optional, may contain one or more **vap** sections detailed below                         |
| part_ex                         | 600.00      |                                                                                           |
| settlement                      | 0.00        |                                                                                           |
| deposit                         | 1500.00     |                                                                                           |
| deposit_sterling                | 0.00        |                                                                                           |
| income_promise_scheme           | 0           | Deprecated                                                                                |
| fda                             | 0           | Finance Deposit Allowance - OEM lenders only                                              |
| rate_type                       | APR         |                                                                                           |
| flat_rate                       | 6.19        |                                                                                           |
| apr_rate                        | 11.9        |                                                                                           |
| term                            | 48          |                                                                                           |
| payment_frequency               | 12          | Payment per calendar year, so 12 = monthly                                                |
| pause_type                      | None        |                                                                                           |
| balloon                         | 6365.00     | Calculated by DealTrak at 85% of CAP                                                      |
| annual_mileage                  | 7000        |                                                                                           |
| vat_rate                        | 20          |                                                                                           |
| finance_package                 | ABC123      | Lender's finance product code                                                             |
| finance_package_id              | 1237276     | Internal to DealTrak                                                                      |
| ltv                             | 0.960787    | Loan to value - CAP                                                                       |
| ltv_glass                       | 0           |                                                                                           |
| credit_score                    |             | Equifax RN score, if search performed by dealer                                           |

#### /rosetta/application/finance_section/options_section/option

| Node         | Example            | Notes |
|--------------|--------------------|-------|
| de scription | Navigation upgrade |       |
| net          | 500                |       |
| vat          | 100                |       |

#### /rosetta/application/finance_section/accessories_section/accessory

|  Node  | Example  | Notes   |
|---|---|---|
| description  | Fluffy dice | |
| net | 5 | |
| vat | 1 | |

#### /rosetta/application/finance_section/vaps/vap

| Node         | Example          | Notes                                 |
|--------------|------------------|---------------------------------------|
| product_id   | 12               | [See List](enumerations.md#vap-types) |
| supplier_id  | 3                | [See List](vapSuppliers.md)           |
| product_code | Paint Protection |                                       |
| product      | Diamondbrite     |                                       |
| premium      | 299.00           |                                       |
| ipt          | 20.00            | Tax rate - VAT only products          |
| vap_term     | 0                |                                       |
| tax_type     | VAT              |                                       |

VAPs will only appear in the Rosetta XML if they are set to be added to the proposal as opposed to being funded separately. If a lender does not intend to fund some or all VAPs, it is up to the lender to re-stack the proposal to cover the VAPs cost from the customer deposit, if sufficient.


#### /rosetta/application/extras_section/notes

| Node | Example                 | Notes |
|------|-------------------------|-------|
| note | This is a helpful note. |       |


#### /rosetta/application/extras_section/data

| Node  | Example     | Notes |
|-------|-------------|-------|
| field | nationality |       |
| value | British     |       |

Extra data values are configured per-lender and can be free-text or fixed values from a drop-down.

A common use case for extra data is to capture additional information at the point of sending, such as
affordability questions.

## Send decision

Lender decisions can be either pushed or polled. For pushed decisions, the lender should make a request into
the send decision endpoint at `/html/services/rosetta/send_decision.php` with the following request body as a minimum.

```xml
<?xml version="1.0"?>
<send_decision>
  <username>provided by dealtrak</username>
  <password>provided by dealtrak</password>
  <reference>lender's application reference</reference>
  <status_id>2</status_id><!-- from the list of destination status IDs -->
</send_decision>
```

A full example with all optional sections:

```xml
<?xml version="1.0"?>
<send_decision>
  <username>provided by dealtrak</username>
  <password>provided by dealtrak</password>
  <reference>lender's application reference</reference>
  <status_id>2</status_id><!-- from the list of destination status IDs -->
  <original_lender_status>Pending E-Sign</original_lender_status><!-- optional -->
  <note>A free text note</note><!-- optional -->
  <conditions><!-- optional -->
    <condition>Service history required</condition>
    <condition>Subject to successful affordability check</condition>
  </conditions>
  <finances><!-- optional -->
    <finance name="APR">16.9</finance><!-- name attribute is arbitrary -->
    <finance name="Monthly Repayment">267.01</finance>
    <finance name="Total Repayable">12816.48</finance>
    <finance name="Term">48</finance>
  </finances>
  <tasks><!-- optional -->
    <task status="complete" time="2022-05-04T12:34:56">
      <title>Provide proof of income</title>
      <description>Copies of customer latest bank statement and payslip required</description>
    </task>
    <task status="incomplete" time="2022-05-05T12:34:56">
      <title>Dealer invoice</title>
      <description>Upload the dealer invoice</description>
    </task>
  </tasks>
  <lender_questions><!-- optional -->
    <question id="asdf1234">
      <title>Contra Settlement</title>
      <type>multiple</type>
      <description>Which of these existing agreements does this application contra-settle?</description>
        <options>
          <option value="827373737">Agreement 827373737, AB12CDE</option>
          <option value="928374743">Agreement 928374743, CD24EFG</option>
        </options>
      </question>
    </lender_questions>
</send_decision>
```

## Send document

To send PDF documents or URLs into DealTrak, use the following request to `/html/services/rosetta/send_document.php`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<send_document>
    <username>provided by dealtrak</username>
    <password>provided by dealtrak</password>
    <reference>lender's application reference</reference>
    <description>Document/link filename or description</description>
    <status>New</status><!-- free text status description, eg: New, Completed etc -->
    <!-- for URLs -->
    <document type="url">https://www.youtube.com/watch?v=dQw4w9WgXcQ</document>
    <!-- OR for PDFs -->
    <document type="pdf">base64 encoded data</document>
    <!-- send only one <document> at a time -->
</send_document>
```

Multiple requests may be made in order to provide multiple documents or links. Documents pushed with the same
name as a previous document will replace older versions.