# Occupations

| ID   | Occupation                                    | SOC Code |
|------|-----------------------------------------------|----------|
| 1    | Abattoir worker                               | 5431     |
| 2    | Abbot                                         | 2463     |
| 3    | Abstractor                                    | 4135     |
| 4    | Accomodation officer                          | 3223     |
| 5    | Accompanist                                   | 3415     |
| 6    | Account director                              | 1132     |
| 7    | Account executive                             | 3554     |
| 8    | Account manager                               | 2139     |
| 9    | Accountant                                    | 2421     |
| 10   | Accounts assistant                            | 4122     |
| 11   | Accounts clerk                                | 4122     |
| 12   | Accounts manager                              | 3556     |
| 13   | Accounts staff                                | 4122     |
| 14   | Accoustic engineer                            | 2129     |
| 15   | Acrobat                                       | 3413     |
| 16   | Actor                                         | 3413     |
| 17   | Actress                                       | 3413     |
| 18   | Actuary                                       | 2433     |
| 19   | Acupuncturist                                 | 2229     |
| 20   | Administration assistant                      | 4151     |
| 21   | Administration clerk                          | 4132     |
| 22   | Administration manager                        | 4141     |
| 23   | Administration officer                        | 4151     |
| 24   | Administration staff                          | 4136     |
| 25   | Administrator                                 | 4113     |
| 26   | Advertising agent                             | 3554     |
| 27   | Advertising assistant                         | 4159     |
| 28   | Advertising buyer                             | 3551     |
| 29   | Advertising clerk                             | 4131     |
| 30   | Advertising contractor                        | 3554     |
| 31   | Advertising controller                        | 3554     |
| 32   | Advertising director                          | 1132     |
| 33   | Advertising executive                         | 3554     |
| 34   | Advertising manager                           | 2494     |
| 35   | Advertising staff                             | 3554     |
| 36   | Advocate                                      | 3229     |
| 37   | Aerial erector                                | 8139     |
| 38   | Aerobic instructor                            | 3433     |
| 39   | Aeronautical engineer                         | 2126     |
| 40   | After dinner speaker                          | 3414     |
| 41   | Agister                                       | 4131     |
| 42   | Agricultural consultant                       | 2112     |
| 43   | Agricultural contractor                       | 5111     |
| 44   | Agricultural engineer                         | 2129     |
| 45   | Agricultural merchant                         | 7131     |
| 46   | Agricultural worker                           | 8229     |
| 47   | Agriculturalist                               | 5111     |
| 48   | Agronomist                                    | 2112     |
| 49   | Air traffic controller                        | 3511     |
| 50   | Aircraft buyer                                | 3551     |
| 51   | Aircraft cabin crew                           | 6219     |
| 52   | Aircraft designer                             | 2126     |
| 53   | Aircraft engineer                             | 3113     |
| 54   | Aircraft fitter                               | 5234     |
| 55   | Aircraft hand                                 | 8233     |
| 56   | Aircraft Maintenance engineer                 | 5234     |
| 57   | Aircraft surface finisher                     | 8143     |
| 58   | Airline broker                                | 3531     |
| 59   | Airline check-in staff                        | 6213     |
| 60   | Airman                                        | 3311     |
| 61   | Airport controller                            | 1241     |
| 62   | Airport manager                               | 1241     |
| 63   | Alarm fitter                                  | 5245     |
| 64   | Almoner                                       | 2461     |
| 65   | Ambulance controller                          | 7213     |
| 66   | Ambulance crew                                | 6132     |
| 67   | Ambulance driver                              | 6132     |
| 68   | Amusement arcade worker                       | 9267     |
| 69   | Anaesthetist                                  | 2212     |
| 70   | Analytical chemist                            | 2111     |
| 71   | Animal breeder                                | 6129     |
| 72   | Animator                                      | 2142     |
| 73   | Announcer                                     | 3413     |
| 74   | Anthropologist                                | 2115     |
| 75   | Antique dealer                                | 7131     |
| 76   | Antique renovator                             | 5442     |
| 77   | Appeal/tribunal chairman                      | 3520     |
| 78   | Applications engineer                         | 2134     |
| 79   | Applications programmer                       | 2134     |
| 80   | Apprentice                                    | 4159     |
| 81   | Aquarist                                      | 6129     |
| 82   | Arbitrator                                    | 2419     |
| 83   | Arborist                                      | 5119     |
| 84   | Archaeologist                                 | 2115     |
| 85   | Archbishop                                    | 2463     |
| 86   | Archdeacon                                    | 2463     |
| 87   | Architect                                     | 2133     |
| 88   | Architects Technician                         | 3120     |
| 89   | Architectural surveyor                        | 2454     |
| 90   | Archivist                                     | 2472     |
| 91   | Area manager                                  | 3556     |
| 92   | Armourer                                      | 5223     |
| 93   | Aromatherapist                                | 3214     |
| 94   | Art buyer                                     | 3551     |
| 95   | Art critic                                    | 2494     |
| 96   | Art dealer                                    | 7131     |
| 97   | Art director                                  | 6221     |
| 98   | Art historian                                 | 2115     |
| 99   | Art restorer                                  | 3411     |
| 100  | Artexer                                       | 5323     |
| 101  | Articled clerk                                | 2421     |
| 102  | Artificial inseminator                        | 9119     |
| 103  | Artificial limb fitter                        | 3213     |
| 104  | Artist                                        | 3413     |
| 105  | Artist - commercial                           | 2142     |
| 106  | Artist - freelance                            | 3413     |
| 107  | Artist - portrait                             | 3417     |
| 108  | Artist - technical                            | 2142     |
| 109  | Asbestos remover                              | 8159     |
| 110  | Asphalter                                     | 8152     |
| 111  | Assembly worker                               | 8142     |
| 112  | Assessor                                      | 3574     |
| 113  | Assistant accounts manager                    | 2494     |
| 114  | Assistant caretaker                           | 6232     |
| 115  | Assistant cook                                | 9263     |
| 116  | Assistant manager                             | 4215     |
| 117  | Assistant nurse                               | 6131     |
| 118  | Assistant teacher                             | 2319     |
| 119  | Associate director                            | 1137     |
| 120  | Astrologer                                    | 9269     |
| 121  | Astronomer                                    | 2114     |
| 122  | Athlete                                       | 7111     |
| 123  | Au pair                                       | 6116     |
| 124  | Auction worker                                | 9259     |
| 125  | Auctioneer                                    | 3555     |
| 126  | Audiologist                                   | 2259     |
| 127  | Audit clerk                                   | 2421     |
| 128  | Audit manager                                 | 3534     |
| 129  | Auditor                                       | 2421     |
| 130  | Author                                        | 3412     |
| 131  | Auto electrician                              | 5231     |
| 132  | Auxiliary nurse                               | 6131     |
| 133  | Bacon curer                                   | 8111     |
| 134  | Bacteriologist                                | 2112     |
| 135  | Baggage handler                               | 8233     |
| 136  | Bailiff                                       | 9129     |
| 137  | Baker                                         | 8111     |
| 138  | Bakery assistant                              | 7111     |
| 139  | Bakery manager                                | 7132     |
| 140  | Bakery operator                               | 8111     |
| 141  | Ballistics expert                             | 2114     |
| 142  | Balloonist                                    | 9267     |
| 143  | Bank clerk                                    | 4123     |
| 144  | Bank manager                                  | 1131     |
| 145  | Bank messenger                                | 4123     |
| 146  | Bank note checker                             | 8143     |
| 147  | Bank staff                                    | 4123     |
| 148  | Banking correspondant                         | 4123     |
| 149  | Baptist minister                              | 2363     |
| 150  | Bar manager                                   | 5436     |
| 151  | Bar staff                                     | 9265     |
| 152  | Bar steward                                   | 9265     |
| 153  | Barber                                        | 6221     |
| 154  | Bargeman                                      | 8232     |
| 155  | Barmaid                                       | 9265     |
| 156  | Barman                                        | 9265     |
| 157  | Barrister                                     | 2411     |
| 158  | Basket worker                                 | 5449     |
| 159  | Beautician                                    | 6222     |
| 160  | Beauty therapist                              | 6222     |
| 161  | BeeKeeper                                     | 5119     |
| 162  | Betting shop clerk                            | 4129     |
| 163  | Bill poster                                   | 9249     |
| 164  | Bingo caller                                  | 9269     |
| 165  | Bingo hall staff                              | 9269     |
| 166  | Biochemist                                    | 2113     |
| 167  | Biologist                                     | 2112     |
| 168  | Biometrician                                  | 2433     |
| 169  | Biophysicist                                  | 2114     |
| 170  | Bishop                                        | 2463     |
| 171  | Blacksmith                                    | 5212     |
| 172  | Blind assembler                               | 5449     |
| 173  | Blind fitter                                  | 8159     |
| 174  | Blinds installer                              | 8159     |
| 175  | Boat builder                                  | 5235     |
| 176  | Boatswain                                     | 9119     |
| 177  | Body fitter                                   | 5232     |
| 178  | Bodyguard                                     | 9231     |
| 179  | Bodyshop manager                              | 1252     |
| 180  | Boiler maker                                  | 5212     |
| 181  | Boiler man                                    | 8111     |
| 182  | Book binder                                   | 5423     |
| 183  | Book seller                                   | 7111     |
| 184  | Bookfinisher                                  | 5423     |
| 185  | Booking agent                                 | 6212     |
| 186  | Booking clerk                                 | 6212     |
| 187  | Booking office clerk                          | 4131     |
| 188  | Book-keeper                                   | 4122     |
| 189  | Boom operator                                 | 3417     |
| 190  | Botanist                                      | 2112     |
| 191  | Bottler                                       | 9132     |
| 192  | Box maker                                     | 8131     |
| 193  | Box office clerk                              | 4129     |
| 194  | Branch manager                                | 1150     |
| 195  | Brewer                                        | 2129     |
| 196  | Brewery manager                               | 1121     |
| 197  | Brewery worker                                | 8111     |
| 198  | Bricklayer                                    | 5313     |
| 199  | Bridgeman                                     | 8239     |
| 200  | Bridgmaster                                   | 8239     |
| 201  | Broadcaster                                   | 3413     |
| 202  | Broadcasting engineer                         | 2124     |
| 203  | Brother (church)                              | 2463     |
| 204  | Builder                                       | 5319     |
| 205  | Builders labourer                             | 9129     |
| 206  | Builders merchant                             | 7131     |
| 207  | Building advisor                              | 3114     |
| 208  | Building contractor                           | 5319     |
| 209  | Building control officer                      | 3581     |
| 210  | Building engineer                             | 2121     |
| 211  | Building estimator                            | 3541     |
| 212  | Building foreman                              | 5330     |
| 213  | Building inspector                            | 3581     |
| 214  | Building maintenance                          | 5319     |
| 215  | Building manager                              | 1251     |
| 216  | Building site inspector                       | 3581     |
| 217  | Building society agent                        | 4123     |
| 218  | Building society clerk                        | 4123     |
| 219  | Building society staff                        | 4123     |
| 220  | Building surveyor                             | 2454     |
| 221  | Bursar                                        | 2329     |
| 222  | Bus conductor                                 | 6219     |
| 223  | Bus driver                                    | 8212     |
| 224  | Bus mechanic                                  | 5231     |
| 225  | Bus valeter                                   | 9226     |
| 226  | Business analyst                              | 2133     |
| 227  | Business consultant                           | 2431     |
| 228  | Butcher                                       | 5433     |
| 229  | Butchery manager                              | 5431     |
| 230  | Butler                                        | 6240     |
| 231  | Buyer                                         | 3551     |
| 232  | Cabinet maker                                 | 5223     |
| 233  | Cable contractor                              | 5242     |
| 234  | Cable jointer                                 | 5242     |
| 235  | Cable TV installer                            | 5242     |
| 236  | Cafe owner                                    | 1222     |
| 237  | Cafe staff                                    | 9263     |
| 238  | Cafe worker                                   | 9263     |
| 239  | Calibration manager                           | 5224     |
| 240  | Call centre manager                           | 4143     |
| 241  | Call centre staff                             | 7211     |
| 242  | Calligrapher                                  | 3411     |
| 243  | Camera repairer                               | 5224     |
| 244  | Cameraman                                     | 3417     |
| 245  | Canal boat broker                             | 3531     |
| 246  | Canon (church)                                | 2463     |
| 247  | Canvasser                                     | 7129     |
| 248  | Captain merchant ship                         | 1161     |
| 249  | Car body repairer                             | 5232     |
| 250  | Car builder                                   | 5232     |
| 251  | Car dealer                                    | 7131     |
| 252  | Car delivery driver                           | 8214     |
| 253  | Car park attendant                            | 6312     |
| 254  | Car salesman                                  | 7115     |
| 255  | Car valet                                     | 9226     |
| 256  | Car wash attendant                            | 9269     |
| 257  | Cardinal                                      | 2463     |
| 258  | Cardiographer                                 | 3213     |
| 259  | Cardiologist                                  | 2212     |
| 260  | Care assistant                                | 6111     |
| 261  | Care manager                                  | 1232     |
| 262  | Careers advisor                               | 3572     |
| 263  | Careers officer                               | 3572     |
| 264  | Carer - non professional                      | 9223     |
| 265  | Carer - professional                          | 6135     |
| 266  | Caretaker                                     | 9129     |
| 267  | Cargo handler                                 | 8233     |
| 268  | Cargo operator                                | 9259     |
| 269  | Carpenter                                     | 5316     |
| 270  | Carpenters assistant                          | 9129     |
| 271  | Carpet cleaner                                | 9224     |
| 272  | Carpet retailer                               | 7111     |
| 273  | Carpetfitter                                  | 5322     |
| 274  | Carphone fitter                               | 5223     |
| 275  | Cartographer                                  | 3120     |
| 276  | Cartoonist                                    | 3411     |
| 277  | Cash point fitter                             | 5241     |
| 278  | Cashier                                       | 7112     |
| 279  | Casual worker                                 | 7111     |
| 280  | Cataloguer                                    | 4131     |
| 281  | Caterer                                       | 5436     |
| 282  | Catering consultant                           | 5436     |
| 283  | Catering manager                              | 5436     |
| 284  | Catering staff                                | 9263     |
| 285  | Caulker                                       | 5212     |
| 286  | Ceiling contractor                            | 8159     |
| 287  | Ceiling fitter                                | 8159     |
| 288  | Ceiling fixer                                 | 8159     |
| 289  | Cellarman                                     | 9265     |
| 290  | Centre lathe operator                         | 8120     |
| 291  | Certified accountant                          | 2421     |
| 292  | Chambermaid                                   | 9223     |
| 293  | Chandler                                      | 7131     |
| 294  | Chaplain                                      | 1161     |
| 295  | Charge hand                                   | 5250     |
| 296  | Charity worker                                | 3229     |
| 297  | Chartered accountant                          | 2421     |
| 298  | Chartered engineer                            | 2121     |
| 299  | Chartered surveyor                            | 2454     |
| 300  | Chartered valuer                              | 3541     |
| 301  | Charterer                                     | 6212     |
| 302  | Chauffeur                                     | 8213     |
| 303  | Check-out assistant                           | 7112     |
| 304  | Chef                                          | 5434     |
| 305  | Chemical engineer                             | 2125     |
| 306  | Chemist                                       | 2251     |
| 307  | Chicken chaser                                | 9119     |
| 308  | Chicken sexer                                 | 9119     |
| 309  | Chief cashier                                 | 3560     |
| 310  | Chief chemist                                 | 2111     |
| 311  | Chief executive                               | 1111     |
| 312  | Child care officer (local government)         | 3222     |
| 313  | Child minder                                  | 6114     |
| 314  | Childrens entertainer                         | 3413     |
| 315  | Chimney sweep                                 | 9229     |
| 316  | China restorer                                | 5441     |
| 317  | Chiropodist                                   | 2256     |
| 318  | Chiropractor                                  | 2229     |
| 319  | Choirmaster                                   | 3416     |
| 320  | Choreographer                                 | 3414     |
| 321  | Chorister                                     | 3413     |
| 322  | Church army worker                            | 3229     |
| 323  | Church dean                                   | 2322     |
| 324  | Church officer                                | 4159     |
| 325  | Church warden                                 | 6232     |
| 326  | Cinema assistant                              | 9267     |
| 327  | Cinema manager                                | 1224     |
| 328  | Circus proprietor                             | 1224     |
| 329  | Ciurcus worker                                | 9267     |
| 330  | Civil engineer                                | 2121     |
| 331  | Civil servant                                 | 4111     |
| 332  | Claims adjustor                               | 3541     |
| 333  | Claims assessor                               | 4111     |
| 334  | Claims manager                                | 3534     |
| 335  | Clairvoyant                                   | 9269     |
| 336  | Clapper                                       | 9269     |
| 337  | Classical musician                            | 3415     |
| 338  | Classroom aide                                | 6112     |
| 339  | Clay pigeon instructor                        | 3432     |
| 340  | Cleaner                                       | 9131     |
| 341  | Cleaning contractor                           | 9223     |
| 342  | Cleaning supervisor                           | 6240     |
| 343  | Clergymen                                     | 2463     |
| 344  | Cleric                                        | 4159     |
| 345  | Clerical assistant                            | 4123     |
| 346  | Clerical officer                              | 4123     |
| 347  | Clerk                                         | 4131     |
| 348  | Clerk of court                                | 2419     |
| 349  | Clerk of works                                | 2455     |
| 350  | Clinical psychoologist                        | 2259     |
| 351  | Clock maker                                   | 5224     |
| 352  | Clothing design cutter                        | 5413     |
| 353  | Coach builder                                 | 5232     |
| 354  | Coach driver                                  | 8212     |
| 355  | Coach sprayer                                 | 5233     |
| 356  | Coal merchant                                 | 7131     |
| 357  | Coalman                                       | 8211     |
| 358  | Coastguard                                    | 3319     |
| 359  | Cobbler                                       | 5412     |
| 360  | Coffee merchant                               | 7131     |
| 361  | Coin dealer                                   | 8120     |
| 362  | College dean                                  | 2322     |
| 363  | College lecturer                              | 2312     |
| 364  | College principal                             | 2319     |
| 365  | College scout                                 | 5231     |
| 366  | Colourist (maps/photographs)                  | 5423     |
| 367  | Comedian                                      | 3413     |
| 368  | Commercial artist                             | 2142     |
| 369  | Commercial manager                            | 2455     |
| 370  | Commercial traveller                          | 7121     |
| 371  | Commission agent                              | 3549     |
| 372  | Commissionaire                                | 9269     |
| 373  | Commissioned officer                          | 1161     |
| 374  | Commissioner of police                        | 1171     |
| 375  | Commissioning engineer                        | 3113     |
| 376  | Commodity broker                              | 3531     |
| 377  | Commodity dealer                              | 3531     |
| 378  | Communications officer                        | 2493     |
| 379  | Communications supervisor                     | 7220     |
| 380  | Community craft instructor                    | 3574     |
| 381  | Community nurse                               | 2232     |
| 382  | Community worker                              | 3221     |
| 383  | Companion                                     | 6231     |
| 384  | Companion nurse                               | 2237     |
| 385  | Company chairman                              | 1111     |
| 386  | Company director                              | 1121     |
| 387  | Company search agent                          | 2435     |
| 388  | Company secretary                             | 1131     |
| 389  | Compiler                                      | 4131     |
| 390  | Complementary therapist                       | 3214     |
| 391  | Compliance officer                            | 2423     |
| 392  | Composer                                      | 3415     |
| 393  | Compositor                                    | 2142     |
| 394  | Computer analyst                              | 2133     |
| 395  | Computer consultant                           | 2139     |
| 396  | Computer editor                               | 2491     |
| 397  | Computer engineer                             | 2133     |
| 398  | Computer manager                              | 2132     |
| 399  | Computer network controller                   | 5242     |
| 400  | Computer operator                             | 4152     |
| 401  | Computer programmer                           | 2134     |
| 402  | Computer technician                           | 3131     |
| 403  | Conductor                                     | 3415     |
| 404  | Confectioner                                  | 8111     |
| 405  | Conference manager                            | 3557     |
| 406  | Conference organiser                          | 3557     |
| 407  | Conservationist                               | 2151     |
| 408  | Conservator                                   | 2472     |
| 409  | Construction engineer                         | 2121     |
| 410  | Construction worker                           | 9129     |
| 411  | Consultant                                    | 2139     |
| 412  | Consultant engineer                           | 2125     |
| 413  | Consumer scientist                            | 2119     |
| 414  | Contract cleaner                              | 9223     |
| 415  | Contract furnisher                            | 7131     |
| 416  | Contract manager                              | 2432     |
| 417  | Contractor                                    | 5313     |
| 418  | Contracts supervisor                          | 5330     |
| 419  | Conveyancer                                   | 2419     |
| 420  | Cook                                          | 8111     |
| 421  | Cooper                                        | 5442     |
| 422  | Copier                                        | 2142     |
| 423  | Coppersmith                                   | 5211     |
| 424  | Copy typist                                   | 4217     |
| 425  | Copywriter                                    | 3412     |
| 426  | Coroner                                       | 2411     |
| 427  | Correspondant press                           | 2492     |
| 428  | Corrosion consultant                          | 2129     |
| 429  | Costume designer                              | 3422     |
| 430  | Costume jeweller                              | 7131     |
| 431  | Costumier                                     | 5413     |
| 432  | Council worker                                | 9119     |
| 433  | Counsellor                                    | 4123     |
| 434  | Counter staff                                 | 4123     |
| 435  | Countryside ranger                            | 5119     |
| 436  | County councillor                             | 4113     |
| 437  | Courier                                       | 6219     |
| 438  | Court officer                                 | 4131     |
| 439  | Court reporter                                | 2492     |
| 440  | Coxswain                                      | 3512     |
| 441  | Craft dealer                                  | 5449     |
| 442  | Craftsman                                     | 5249     |
| 443  | Craftswoman                                   | 5249     |
| 444  | Crane driver                                  | 8221     |
| 445  | Crane erector                                 | 5223     |
| 446  | Crane operator                                | 8221     |
| 447  | Crash assessor                                | 3581     |
| 448  | Creative director                             | 2494     |
| 449  | Creche worker                                 | 6111     |
| 450  | Credit broker                                 | 3531     |
| 451  | Credit controller                             | 4121     |
| 452  | Credit draper                                 | 7121     |
| 453  | Credit manager                                | 3534     |
| 454  | Credit researcher                             | 2431     |
| 455  | Crematorium attendant                         | 6138     |
| 456  | Cricketer                                     | 3431     |
| 457  | Crime examiner                                | 3319     |
| 458  | Criminologist                                 | 2115     |
| 459  | Crofter                                       | 5111     |
| 460  | Croupier                                      | 6211     |
| 461  | Crown prosecutor                              | 2411     |
| 462  | Curate                                        | 2463     |
| 463  | Curator                                       | 2472     |
| 464  | Currency trader                               | 3531     |
| 465  | Curtain hanger                                | 5411     |
| 466  | Curtain maker                                 | 5411     |
| 467  | Customer advisor                              | 4123     |
| 468  | Customer liaison officer                      | 7219     |
| 469  | Customer service controller                   | 4123     |
| 470  | Customs & excise officer                      | 3319     |
| 471  | Cutler                                        | 8139     |
| 472  | Cutter                                        | 5449     |
| 473  | Cycle repairer                                | 5231     |
| 474  | Cyclist                                       | 3431     |
| 475  | Dairy engineer                                | 5223     |
| 476  | Dairy worker                                  | 8111     |
| 477  | Dance teacher                                 | 2312     |
| 478  | Dancer                                        | 3414     |
| 479  | Dark room technician                          | 5423     |
| 480  | Data administrator                            | 4152     |
| 481  | Data co-ordinator                             | 2139     |
| 482  | Data processing manager                       | 2132     |
| 483  | Data processor                                | 4152     |
| 484  | Day care officer                              | 6135     |
| 485  | Day centre officer                            | 6135     |
| 486  | Dealer - general                              | 6211     |
| 487  | Debt collector                                | 7122     |
| 488  | Debt councellor                               | 3224     |
| 489  | Decorator                                     | 5441     |
| 490  | Delivery courier                              | 8214     |
| 491  | Delivery driver                               | 8214     |
| 492  | Delivery roundsman                            | 8211     |
| 493  | Demolition contractor                         | 8159     |
| 494  | Demolition worker                             | 8159     |
| 495  | Demonstrator                                  | 2311     |
| 496  | Dendrochronologist                            | 2253     |
| 497  | Dental assistant                              | 6133     |
| 498  | Dental hygienist                              | 3213     |
| 499  | Dental mechanic                               | 3213     |
| 500  | Dental nurse                                  | 6133     |
| 501  | Dental surgeon                                | 2253     |
| 502  | Dental technician                             | 3213     |
| 503  | Dental therapist                              | 3213     |
| 504  | Dentist                                       | 2253     |
| 505  | Deputy head teacher                           | 2315     |
| 506  | Deputy manager                                | 4129     |
| 507  | Deputy principal                              | 2315     |
| 508  | Dermatologist                                 | 2212     |
| 509  | Design copier                                 | 2142     |
| 510  | Design director                               | 2494     |
| 511  | Design engineer                               | 2126     |
| 512  | Design manager                                | 2161     |
| 513  | Designer                                      | 2126     |
| 514  | Despatch driver                               | 8214     |
| 515  | Despatch rider                                | 8214     |
| 516  | Despatch worker                               | 9252     |
| 517  | Development manager                           | 2132     |
| 518  | Diamond dealer                                | 3531     |
| 519  | Diecaster                                     | 5212     |
| 520  | Dietician                                     | 2259     |
| 521  | Dinner lady                                   | 9263     |
| 522  | Diplomat                                      | 1111     |
| 523  | Director of environment                       | 1251     |
| 524  | Director of housing                           | 1251     |
| 525  | Director of planning                          | 1251     |
| 526  | Disc jockey                                   | 3413     |
| 527  | Dispenser (pharmacy)                          | 7114     |
| 528  | Distillery worker                             | 8111     |
| 529  | Distribution manager                          | 1123     |
| 530  | Distributor (leaflets/circ)                   | 9211     |
| 531  | District nurse                                | 2232     |
| 532  | District valuer                               | 3541     |
| 533  | Diver                                         | 5319     |
| 534  | Dock pilot                                    | 3512     |
| 535  | Docker                                        | 9259     |
| 536  | Dockyard worker                               | 9139     |
| 537  | Doctor                                        | 2211     |
| 538  | Document controller                           | 4131     |
| 539  | Dog beautician                                | 6129     |
| 540  | Dog breeder                                   | 6129     |
| 541  | Dog groomer                                   | 6129     |
| 542  | Dog handler                                   | 6129     |
| 543  | Dog trainer                                   | 6129     |
| 544  | Dog walker                                    | 6129     |
| 545  | Dog warden                                    | 6129     |
| 546  | Doll maker                                    | 8149     |
| 547  | Domestic staff                                | 9223     |
| 548  | Door fitter                                   | 5223     |
| 549  | Door to door collector                        | 7121     |
| 550  | Doorman                                       | 9139     |
| 551  | Double glazing fitter                         | 5317     |
| 552  | Double glazing salesman                       | 3552     |
| 553  | Draper                                        | 7131     |
| 554  | Draughtsman                                   | 3120     |
| 555  | Draughtswoman                                 | 3120     |
| 556  | Drayman                                       | 8211     |
| 557  | Dredge master                                 | 8229     |
| 558  | Dredgerman                                    | 8229     |
| 559  | Dresser theatre/films                         | 3429     |
| 560  | Dressmaker                                    | 5413     |
| 561  | Driller (civil engineering)                   | 2129     |
| 562  | Drilling technician                           | 2121     |
| 563  | Driver                                        | 8229     |
| 564  | Driving examiner                              | 3581     |
| 565  | Driving instructor                            | 8215     |
| 566  | Driving instructor - advanced                 | 8215     |
| 567  | Driving instructor (hgv)                      | 8215     |
| 568  | Drug addiction counsellor                     | 3224     |
| 569  | Dry cleaner                                   | 9224     |
| 570  | Dryliner                                      | 5321     |
| 571  | Dustman                                       | 9225     |
| 572  | Dye polisher                                  | 8120     |
| 573  | Dyer                                          | 8114     |
| 574  | Earth moving contractor                       | 8229     |
| 575  | Ecologist                                     | 2151     |
| 576  | Economist                                     | 2433     |
| 577  | Editor                                        | 3412     |
| 578  | Editorial consultant                          | 3412     |
| 579  | Editorial staff                               | 3412     |
| 580  | Education advisor                             | 2323     |
| 581  | Education officer                             | 2259     |
| 582  | Electrical contractor                         | 5241     |
| 583  | Electrical contracts manager                  | 1242     |
| 584  | Electrical engineer                           | 5241     |
| 585  | Electrical fitter                             | 5241     |
| 586  | Electrical process worker                     | 8141     |
| 587  | Electrician                                   | 5241     |
| 588  | Electrologist                                 | 6222     |
| 589  | Electronic engineer                           | 2124     |
| 590  | Electronics supervisor                        | 5250     |
| 591  | Electronics technician                        | 3112     |
| 592  | Electroplater                                 | 8115     |
| 593  | Electrotyper                                  | 5421     |
| 594  | Embalmer                                      | 6138     |
| 595  | Embassy staff                                 | 2439     |
| 596  | Embroiderer                                   | 5419     |
| 597  | Embryologist                                  | 2113     |
| 598  | Endocrinologist                               | 2113     |
| 599  | Energy analyst                                | 7129     |
| 600  | Engine room man                               | 8232     |
| 601  | Engineer                                      | 5234     |
| 602  | Engineer stoker                               | 8234     |
| 603  | Engineman                                     | 8231     |
| 604  | Engraver                                      | 5441     |
| 605  | Enquiry agent                                 | 7211     |
| 606  | Entertainer                                   | 3413     |
| 607  | Enotomologist                                 | 2112     |
| 608  | Environmental chemist                         | 2152     |
| 609  | Environmental consultant                      | 2152     |
| 610  | Environmental health officer                  | 2483     |
| 611  | Equity agent                                  | 3531     |
| 612  | Erector                                       | 5241     |
| 613  | Ergonomist                                    | 2129     |
| 614  | Escort                                        | 9269     |
| 615  | Estate agent                                  | 3555     |
| 616  | Estate manager                                | 1251     |
| 617  | Estimator                                     | 3541     |
| 618  | Evangelist                                    | 3229     |
| 619  | Events organiser                              | 3557     |
| 620  | Examiner                                      | 2421     |
| 621  | Excursion manager                             | 6212     |
| 622  | Executive officer                             | 4111     |
| 623  | Exhaust fitter                                | 8145     |
| 624  | Exhibition designer                           | 2142     |
| 625  | Exhibition organiser                          | 3557     |
| 626  | Exotic dancer                                 | 9269     |
| 627  | Expedition leader                             | 6212     |
| 628  | Explosives worker                             | 8113     |
| 629  | Export consultant                             | 3542     |
| 630  | Exporter                                      | 3542     |
| 631  | Extra                                         | 3413     |
| 632  | Extrusion operator                            | 8113     |
| 633  | Fabricator                                    | 5317     |
| 634  | Factory canteen manager                       | 5436     |
| 635  | Factory inspector                             | 8143     |
| 636  | Factory manager                               | 1121     |
| 637  | Factory worker                                | 9132     |
| 638  | Fairground worker                             | 9267     |
| 639  | Falconer                                      | 5119     |
| 640  | Farm manager                                  | 1211     |
| 641  | Farm worker                                   | 9119     |
| 642  | Farmer                                        | 5111     |
| 643  | Farrier                                       | 5212     |
| 644  | Fashion designer                              | 3422     |
| 645  | Fashion photographer                          | 3417     |
| 646  | Fast food delivery driver                     | 8214     |
| 647  | Fast food proprieter                          | 1222     |
| 648  | Fence erector                                 | 5319     |
| 649  | Fettler (foundry)                             | 8120     |
| 650  | Fibre glass moulder                           | 8114     |
| 651  | Field officer                                 | 2112     |
| 652  | Figure painter                                | 5441     |
| 653  | Filing clerk                                  | 4131     |
| 654  | Film director                                 | 3416     |
| 655  | Film gaffer                                   | 5441     |
| 656  | Film producer                                 | 3416     |
| 657  | Film technician                               | 3417     |
| 658  | Finance director                              | 1131     |
| 659  | Finance manager                               | 1131     |
| 660  | Finance officer                               | 4124     |
| 661  | Financial advisor                             | 2422     |
| 662  | Financial analyst                             | 2422     |
| 663  | Financial consultant                          | 2422     |
| 664  | Financier                                     | 2422     |
| 665  | Finisher                                      | 5234     |
| 666  | Fire officer                                  | 3313     |
| 667  | Fire prevention officer                       | 3313     |
| 668  | Fire protection consultant                    | 3582     |
| 669  | Firefighter                                   | 3313     |
| 670  | Fireplace fitter                              | 5319     |
| 671  | Firewood merchant                             | 7124     |
| 672  | First aid instructor                          | 6131     |
| 673  | First aid worker                              | 6131     |
| 674  | Fish buyer                                    | 5433     |
| 675  | Fish filleter                                 | 5433     |
| 676  | Fish fryer                                    | 5435     |
| 677  | Fish merchant                                 | 7131     |
| 678  | Fish worker                                   | 9259     |
| 679  | Fisheries inspector                           | 3581     |
| 680  | Fisherman                                     | 5119     |
| 681  | Fishery manager                               | 1212     |
| 682  | Fishmonger                                    | 5433     |
| 683  | Fitness instructor                            | 3433     |
| 684  | Fitter                                        | 5234     |
| 685  | Fitter tyre/exhaust                           | 8145     |
| 686  | Flagger                                       | 8112     |
| 687  | Fleet manager                                 | 1241     |
| 688  | Flight deck crew                              | 3511     |
| 689  | Flight dispatcher                             | 8233     |
| 690  | Floor layer                                   | 5322     |
| 691  | Floor manager                                 | 3416     |
| 692  | Florist                                       | 5443     |
| 693  | Flour miller                                  | 8111     |
| 694  | Flower arranger                               | 5443     |
| 695  | Flying instructor                             | 3511     |
| 696  | Foam converter                                | 8149     |
| 697  | Food processor                                | 5433     |
| 698  | Footballer - semi professional                | 3431     |
| 699  | Footman                                       | 6231     |
| 700  | Foreman                                       | 9132     |
| 701  | Forensic scientist                            | 2112     |
| 702  | Forest ranger                                 | 9112     |
| 703  | Forester                                      | 9112     |
| 704  | Fork lift truck driver                        | 8222     |
| 705  | Fortune teller                                | 9269     |
| 706  | Forwarding agent                              | 4134     |
| 707  | Foster parent                                 | 6134     |
| 708  | Foundry moulder                               | 5212     |
| 709  | Foundry worker                                | 9139     |
| 710  | Fraud investigator                            | 3319     |
| 711  | Freelance photographer                        | 3417     |
| 712  | French polisher                               | 5323     |
| 713  | Fruiterer                                     | 7124     |
| 714  | Fuel merchant                                 | 7131     |
| 715  | Fund manager                                  | 3534     |
| 716  | Fund raiser                                   | 3554     |
| 717  | Funeral director                              | 6138     |
| 718  | Funeral furnisher                             | 6138     |
| 719  | Furnace man                                   | 8115     |
| 720  | Furniture dealer                              | 7111     |
| 721  | Furniture remover                             | 9253     |
| 722  | Furniture restorer                            | 5442     |
| 723  | Furrier                                       | 5413     |
| 724  | Gallery owner                                 | 1255     |
| 725  | Gambler                                       | 6211     |
| 726  | Gamekeeper                                    | 5119     |
| 727  | Gaming board inspector                        | 3581     |
| 728  | Gaming club manager                           | 1222     |
| 729  | Gaming club proprietor                        | 1256     |
| 730  | Garage attendant                              | 7112     |
| 731  | Garage foreman                                | 5250     |
| 732  | Garage manager                                | 1252     |
| 733  | Garda                                         | 3312     |
| 734  | Garden designer                               | 5113     |
| 735  | Gardener                                      | 5112     |
| 736  | Gas fitter                                    | 5315     |
| 737  | Gas mechanic                                  | 5315     |
| 738  | Gas technician                                | 5315     |
| 739  | Gate keeper                                   | 8234     |
| 740  | Genealogist                                   | 2115     |
| 741  | General manager                               | 1111     |
| 742  | General practitioner                          | 2211     |
| 743  | Geneticist                                    | 2113     |
| 744  | Geographer                                    | 2115     |
| 745  | Geologist                                     | 2114     |
| 746  | Geophysicist                                  | 2114     |
| 747  | Gereatrician                                  | 2212     |
| 748  | Gilder                                        | 5423     |
| 749  | Glass worker                                  | 9119     |
| 750  | Glazier                                       | 5317     |
| 751  | Goldsmith                                     | 5449     |
| 752  | Golf caddy                                    | 5114     |
| 753  | Golf club professional                        | 3432     |
| 754  | Golf coach                                    | 3432     |
| 755  | Golfer                                        | 3431     |
| 756  | Goods despatcher                              | 9252     |
| 757  | Goods handler                                 | 9252     |
| 758  | Governor                                      | 1163     |
| 759  | Granite technician                            | 5312     |
| 760  | Graphic desginer                              | 2142     |
| 761  | Graphologist                                  | 3549     |
| 762  | Grave digger                                  | 9129     |
| 763  | Gravel merchant                               | 7131     |
| 764  | Green keeper                                  | 5114     |
| 765  | Greengrocer                                   | 7124     |
| 766  | Grocer                                        | 7123     |
| 767  | Groom                                         | 6129     |
| 768  | Ground worker                                 | 9121     |
| 769  | Groundsman                                    | 5114     |
| 770  | Guard                                         | 9231     |
| 771  | Guest house proprietor                        | 6250     |
| 772  | Guide                                         | 6211     |
| 773  | Gun smith                                     | 5223     |
| 774  | Gynaecologist                                 | 2212     |
| 775  | Haemotologist                                 | 2212     |
| 776  | Hair stylist                                  | 6221     |
| 777  | Hairdresser                                   | 6221     |
| 778  | Handyman                                      | 8132     |
| 779  | Harbour master                                | 1241     |
| 780  | Hardware dealer                               | 1254     |
| 781  | Haulage contractor                            | 8211     |
| 782  | Hawker                                        | 7124     |
| 783  | Head accurist                                 | 5224     |
| 784  | Head greenkeeper                              | 5114     |
| 785  | Head lad                                      | 6129     |
| 786  | Head of arts                                  | 1255     |
| 787  | Head of trade                                 | 1150     |
| 788  | Head of traffic                               | 1241     |
| 789  | Headteacher                                   | 2321     |
| 790  | Health advisor                                | 3229     |
| 791  | Health and safety consultant                  | 3582     |
| 792  | Health and safety officer                     | 3582     |
| 793  | Health care assistant                         | 6131     |
| 794  | Health planner                                | 2215     |
| 795  | Health therapist                              | 3219     |
| 796  | Health visitor                                | 2232     |
| 797  | Hearing aid technician                        | 3213     |
| 798  | Hearing therapist                             | 3213     |
| 799  | Heating & ventilation engineer                | 5315     |
| 800  | Heating engineer                              | 2129     |
| 801  | Heating/ventilation engineer                  | 5315     |
| 802  | Hedger/ditcher                                | 9119     |
| 803  | Helmsman                                      | 8232     |
| 804  | Herbalist                                     | 3214     |
| 805  | Herdsman                                      | 9111     |
| 806  | HGV driver                                    | 8211     |
| 807  | HGV mechanic                                  | 5231     |
| 808  | Highway inspector                             | 5330     |
| 809  | Hire car driver                               | 7129     |
| 810  | Hirer                                         | 7129     |
| 811  | Histologist                                   | 2113     |
| 812  | Historian                                     | 2115     |
| 813  | Hod carrier                                   | 9129     |
| 814  | Home economist                                | 3219     |
| 815  | Home help                                     | 9223     |
| 816  | Homecare manager                              | 1232     |
| 817  | Homeopath                                     | 2212     |
| 818  | Home Maker                                    | 6134     |
| 819  | Hop merchant                                  | 7131     |
| 820  | Horse breeder                                 | 7131     |
| 821  | Horse dealer (non sport)                      | 7131     |
| 822  | Horse dealer (sport)                          | 7131     |
| 823  | Horse riding instructor                       | 3432     |
| 824  | Horse trader                                  | 7131     |
| 825  | Horse trainer                                 | 6129     |
| 826  | Horticultural consultant                      | 2112     |
| 827  | Horticulturalist                              | 2112     |
| 828  | Hosiery mechanic                              | 5223     |
| 829  | Hosiery worker                                | 8112     |
| 830  | Hospital consultant                           | 2212     |
| 831  | Hospital doctor                               | 2211     |
| 832  | Hospital manager                              | 3557     |
| 833  | Hospital orderly                              | 6131     |
| 834  | Hospital photographer                         | 2254     |
| 835  | Hospital technician                           | 3213     |
| 836  | Hospital warden                               | 6131     |
| 837  | Hospital worker                               | 6131     |
| 838  | Hostess                                       | 6213     |
| 839  | Hot foil printer                              | 5422     |
| 840  | Hotel consultant                              | 3557     |
| 841  | Hotel worker                                  | 9269     |
| 842  | Hotelier                                      | 1221     |
| 843  | House parent                                  | 6134     |
| 844  | House sitter                                  | 9269     |
| 845  | Househusband                                  | 6134     |
| 846  | Housekeeper                                   | 9223     |
| 847  | Houseman                                      | 9223     |
| 848  | Housewife                                     | 6134     |
| 849  | Housing assistant                             | 4112     |
| 850  | Housing officer                               | 3223     |
| 851  | Housing supervisor                            | 3223     |
| 852  | Human resources manager                       | 1136     |
| 853  | Human resources staff                         | 4136     |
| 854  | Hunt master                                   | 6129     |
| 855  | Huntsman                                      | 6129     |
| 856  | Hydro geologist                               | 2114     |
| 857  | Hygienist                                     | 9131     |
| 858  | Hypnotherapist                                | 3214     |
| 859  | Hypnotist                                     | 3413     |
| 860  | Ice cream vendor                              | 7123     |
| 861  | Illustrator                                   | 3411     |
| 862  | Immigiration officer                          | 3319     |
| 863  | Immunologist                                  | 2212     |
| 864  | Import consultant                             | 3556     |
| 865  | Import/export dealer                          | 3542     |
| 866  | Importer                                      | 3542     |
| 867  | Independent means                             | 2422     |
| 868  | Induction moulder                             | 8114     |
| 869  | Industrial chemist                            | 2111     |
| 870  | Industrial consultant                         | 2125     |
| 871  | Industrial designer                           | 3429     |
| 872  | Injection moulder                             | 5412     |
| 873  | Inland revenue officer                        | 4111     |
| 874  | Inslovency practitioner                       | 2421     |
| 875  | Inspector                                     | 3582     |
| 876  | Inspector - insurance                         | 3532     |
| 877  | Installation worker                           | 8113     |
| 878  | Instructor                                    | 2313     |
| 879  | Instrument engineer                           | 2125     |
| 880  | Instrument maker                              | 5449     |
| 881  | Instrument supervisor                         | 5250     |
| 882  | Instrument technician                         | 3113     |
| 883  | Insurance assessor                            | 3541     |
| 884  | Insurance collector                           | 7121     |
| 885  | Insurance consultant                          | 2422     |
| 886  | Insurance inspector                           | 3532     |
| 887  | Insurance representative                      | 7121     |
| 888  | Interior decorator                            | 5323     |
| 889  | Interior designer                             | 3421     |
| 890  | Interpreter                                   | 3412     |
| 891  | Interviewer                                   | 7214     |
| 892  | Inventor                                      | 2161     |
| 893  | Investigator                                  | 2472     |
| 894  | Investment advisor                            | 2422     |
| 895  | Investment banker                             | 1131     |
| 896  | Investment manager                            | 3534     |
| 897  | Invigilator                                   | 9233     |
| 898  | Ironer                                        | 9224     |
| 899  | Ironer finisher                               | 5412     |
| 900  | Ironer presser                                | 5412     |
| 901  | Ironmonger                                    | 7131     |
| 902  | IT consultant                                 | 2139     |
| 903  | IT manager                                    | 2132     |
| 904  | IT trainer                                    | 3573     |
| 905  | Itinerant labourer                            | 9119     |
| 906  | Itinerant trader                              | 7124     |
| 907  | Janitor                                       | 6232     |
| 908  | Jazz composer                                 | 3415     |
| 909  | Jeweller                                      | 5449     |
| 910  | Jewellery consultant                          | 3422     |
| 911  | Jockey                                        | 3431     |
| 912  | Joiner                                        | 5442     |
| 913  | Joinery consultant                            | 5316     |
| 914  | Jointer                                       | 5242     |
| 915  | Journalist                                    | 2492     |
| 916  | Journalist - freelance                        | 2492     |
| 917  | Journalistic agent                            | 2492     |
| 918  | Judge                                         | 3413     |
| 919  | Juggler                                       | 3413     |
| 920  | Junk shop proprietor                          | 7131     |
| 921  | Justice of the peace                          | 2411     |
| 922  | Keep fit instructor                           | 3433     |
| 923  | Kennel hand                                   | 6129     |
| 924  | Kennel maid                                   | 6129     |
| 925  | Kiln setter                                   | 8119     |
| 926  | Kilnman (glass/ceramics)                      | 8119     |
| 927  | Kissagram person                              | 9269     |
| 928  | Kitchen worker                                | 9263     |
| 929  | Knitter                                       | 5419     |
| 930  | Labelling operator                            | 9132     |
| 931  | Laboratory analyst                            | 2111     |
| 932  | Laboratory assistant                          | 3111     |
| 933  | Laboratory attendant                          | 3111     |
| 934  | Laboratory manager                            | 2161     |
| 935  | Laboratory operative                          | 3111     |
| 936  | Laboratory supervisor                         | 3111     |
| 937  | Laboratory technician                         | 3111     |
| 938  | Labourer                                      | 9111     |
| 939  | Laminator                                     | 8114     |
| 940  | Lampshade maker                               | 5449     |
| 941  | Land agent                                    | 3555     |
| 942  | Land surveyor                                 | 2454     |
| 943  | Landlady                                      | 6250     |
| 944  | Landlord                                      | 6250     |
| 945  | Landowner                                     | 1251     |
| 946  | Landscape architect                           | 2451     |
| 947  | Landscape gardner                             | 5113     |
| 948  | Landworker                                    | 9111     |
| 949  | Lathe operator                                | 8139     |
| 950  | Laundry worker                                | 9224     |
| 951  | Lavatory attendant                            | 9229     |
| 952  | Law clerk                                     | 2419     |
| 953  | Lawn mower repairer                           | 5223     |
| 954  | Lawyer                                        | 2412     |
| 955  | Leaflet distributor                           | 9211     |
| 956  | Leather worker                                | 5412     |
| 957  | Lecturer                                      | 2253     |
| 958  | Ledger clerk                                  | 4122     |
| 959  | Legal advisor                                 | 2419     |
| 960  | Legal assistant                               | 3520     |
| 961  | Legal executive                               | 2419     |
| 962  | Legal secretary                               | 4212     |
| 963  | Leisure centre attendant                      | 6211     |
| 964  | Leisure centre manager                        | 1224     |
| 965  | Lengthman                                     | 9129     |
| 966  | Lens grinder & polish                         | 5441     |
| 967  | Letting agent                                 | 3555     |
| 968  | Lexicographer                                 | 3412     |
| 969  | Liaison officer                               | 4111     |
| 970  | Librarian                                     | 2471     |
| 971  | Library manager                               | 1259     |
| 972  | Licensee                                      | 7131     |
| 973  | Licensing consultant                          | 3556     |
| 974  | Life assurance salesman                       | 7129     |
| 975  | Lifeguard                                     | 6211     |
| 976  | Lift attendant                                | 9269     |
| 977  | Lift engineer                                 | 5223     |
| 978  | Lighterman                                    | 8232     |
| 979  | Lighthouse keeper                             | 8239     |
| 980  | Lighting designer                             | 3417     |
| 981  | Lighting director                             | 3417     |
| 982  | Lighting technician                           | 5241     |
| 983  | Lime kiln attendant                           | 8119     |
| 984  | Line manager                                  | 9211     |
| 985  | Line worker                                   | 8111     |
| 986  | Linesman                                      | 9132     |
| 987  | Linguist                                      | 3412     |
| 988  | Linotype operator                             | 5421     |
| 989  | Literary agent                                | 3412     |
| 990  | Literary editor                               | 2491     |
| 991  | Lithographer                                  | 5441     |
| 992  | Litigation manager                            | 3520     |
| 993  | Loader                                        | 5449     |
| 994  | Loans manager                                 | 4123     |
| 995  | Local government officer                      | 4112     |
| 996  | Lock keeper                                   | 8239     |
| 997  | Locksmith                                     | 5223     |
| 998  | Locum doctor                                  | 2211     |
| 999  | Locum pharmacist                              | 2251     |
| 1000 | Log merchant                                  | 7124     |
| 1001 | Lorry driver                                  | 8211     |
| 1002 | Loss adjustor                                 | 3541     |
| 1003 | Loss Assessor                                 | 3541     |
| 1004 | Lumberjack                                    | 9112     |
| 1005 | Machine Fitters Mate                          | 5223     |
| 1006 | Machine Minder                                | 8135     |
| 1007 | Machine Operator                              | 5221     |
| 1008 | Machine Setter                                | 5449     |
| 1009 | Machine Technician                            | 3113     |
| 1010 | Machine Tool Engineer                         | 2122     |
| 1011 | Machine Tool Fitter                           | 5223     |
| 1012 | Machinist                                     | 8120     |
| 1013 | Magician                                      | 3413     |
| 1014 | Magistrate                                    | 2411     |
| 1015 | Magistrate Clerk                              | 2419     |
| 1016 | Maid                                          | 9223     |
| 1017 | Mail Order Worker                             | 9132     |
| 1018 | Maintenance Engineer                          | 5234     |
| 1019 | Maintenance Fitter                            | 5234     |
| 1020 | Maintenance Man                               | 5234     |
| 1021 | Maintenance Manager                           | 5319     |
| 1022 | Maintenance Staff                             | 8159     |
| 1023 | Maitre d Hotel                                | 9261     |
| 1024 | Make Up Artist                                | 3429     |
| 1025 | Make Up Supervisor                            | 6222     |
| 1026 | Management Consultant                         | 2431     |
| 1027 | Management Trainee                            | 2431     |
| 1028 | Manager                                       | 1222     |
| 1029 | Managing Clerk                                | 2412     |
| 1030 | Managing Director                             | 1122     |
| 1031 | Manicurist                                    | 6222     |
| 1032 | Manufacturing Agent                           | 4133     |
| 1033 | Manufacturing Engineer                        | 3113     |
| 1034 | Manufacturing Technician                      | 8141     |
| 1035 | Map Maker                                     | 8135     |
| 1036 | Map Mounter                                   | 8149     |
| 1037 | Marble Finisher                               | 5312     |
| 1038 | Marble Mason                                  | 5312     |
| 1039 | Marine Broker                                 | 3531     |
| 1040 | Marine Consultant                             | 3549     |
| 1041 | Marine Electrician                            | 8232     |
| 1042 | Marine Engineer                               | 3512     |
| 1043 | Marine Geologist                              | 2151     |
| 1044 | Marine Pilot                                  | 3512     |
| 1045 | Marine Surveyor                               | 3541     |
| 1046 | Market Gardener                               | 5112     |
| 1047 | Market Research Assistant                     | 2162     |
| 1048 | Market Research                               | 7214     |
| 1049 | Market Trader                                 | 7124     |
| 1050 | Marketing Assistant                           | 3554     |
| 1051 | Marketing Consultant                          | 3554     |
| 1052 | Marketing Co-ordinator                        | 3554     |
| 1053 | Marketing Director                            | 1132     |
| 1054 | Marketing Executive                           | 3554     |
| 1055 | Marketing Manager                             | 2432     |
| 1056 | Marquee Erector                               | 9129     |
| 1057 | Marquee Therapist                             | 9129     |
| 1058 | Masseur                                       | 3214     |
| 1059 | Masseuse                                      | 3214     |
| 1060 | Master Marine                                 | 1212     |
| 1061 | Master of Ceremonies                          | 6211     |
| 1062 | Master of Foxhounds                           | 6129     |
| 1063 | Materials Controller                          | 4133     |
| 1064 | Materials Manager                             | 1242     |
| 1065 | Mathematician                                 | 2433     |
| 1066 | Matron                                        | 2237     |
| 1067 | Matress Maker                                 | 5411     |
| 1068 | Mature Student - Living At Home               | 9219     |
| 1069 | Mature Student - Living Away                  | 9219     |
| 1070 | Meat Inspector                                | 3581     |
| 1071 | Meat Wholesalor                               | 7131     |
| 1072 | Mechanic                                      | 5234     |
| 1073 | Mechanical Designer                           | 2122     |
| 1074 | Mechanical Engineer                           | 2126     |
| 1075 | Mechanical Technician                         | 3113     |
| 1076 | Medal Dealer                                  | 8120     |
| 1077 | Media Critic                                  | 2492     |
| 1078 | Media Planner                                 | 3554     |
| 1079 | Medical Advisor                               | 2211     |
| 1080 | Medical Assistant                             | 3311     |
| 1081 | Medical Consultant                            | 2212     |
| 1082 | Medical Diagnostician                         | 3213     |
| 1083 | Medical Officer                               | 1161     |
| 1084 | Medical Physicist                             | 2259     |
| 1085 | Medical Practitioner                          | 2211     |
| 1086 | Medical Researcher                            | 2113     |
| 1087 | Medical Secretary                             | 4211     |
| 1088 | Medical Student - Living At Home              | 2237     |
| 1089 | Medical Student - Living Away                 | 2237     |
| 1090 | Medical Supplier                              | 7131     |
| 1091 | Medical Technician                            | 3213     |
| 1092 | Member Of Parliament                          | 1112     |
| 1093 | Merchandiser                                  | 9241     |
| 1094 | Merchant Banker                               | 1131     |
| 1095 | Merchant Seaman                               | 8232     |
| 1096 | Messenger                                     | 9211     |
| 1097 | Metal Dealer                                  | 1254     |
| 1098 | Metal Engineer                                | 2129     |
| 1099 | Metal Polisher                                | 8120     |
| 1100 | Metal Worker                                  | 5223     |
| 1101 | Metallurgist                                  | 2129     |
| 1102 | Meteorologist                                 | 2114     |
| 1103 | Meter Reader                                  | 7122     |
| 1104 | Microbiologist                                | 2112     |
| 1105 | Microfilm Operator                            | 9219     |
| 1106 | Microscopist                                  | 3111     |
| 1107 | Midwife                                       | 2231     |
| 1108 | Milklady                                      | 7123     |
| 1109 | Milkman                                       | 7123     |
| 1110 | Mill Operator                                 | 8111     |
| 1111 | Mill Worker                                   | 8111     |
| 1112 | Miller                                        | 8111     |
| 1113 | Milliner                                      | 7131     |
| 1114 | Millwright                                    | 5223     |
| 1115 | Miner                                         | 8132     |
| 1116 | Mineralogist                                  | 2114     |
| 1117 | Minibus Driver                                | 8212     |
| 1118 | Minicab Driver                                | 8213     |
| 1119 | Mining Consultant                             | 2121     |
| 1120 | Mining Engineer                               | 2121     |
| 1121 | Minister Of Religion                          | 2463     |
| 1122 | Minister Of The Crown                         | 1112     |
| 1123 | Missionary                                    | 3229     |
| 1124 | Mobile Caterer                                | 5436     |
| 1125 | Mobile Disc Jockey                            | 3413     |
| 1126 | Mobile Disc Owner                             | 5441     |
| 1127 | Mobile Hairdresser                            | 6221     |
| 1128 | Mobile Motor Mechanic                         | 5231     |
| 1129 | Mobile Service Engineer                       | 5225     |
| 1130 | Model                                         | 3413     |
| 1131 | Model Maker                                   | 5449     |
| 1132 | Moderator                                     | 2329     |
| 1133 | Money Broker                                  | 3531     |
| 1134 | Money Dealer                                  | 3531     |
| 1135 | Moneylender                                   | 7121     |
| 1136 | Monk                                          | 2314     |
| 1137 | Monumental Sculptor                           | 3411     |
| 1138 | Mooring Contractor                            | 9139     |
| 1139 | Mortage Broker                                | 3531     |
| 1140 | Mortage Consultant                            | 3531     |
| 1141 | Mortician                                     | 6138     |
| 1142 | Motor Dealer                                  | 7131     |
| 1143 | Motor Engineer                                | 5231     |
| 1144 | Motor Fitter                                  | 5241     |
| 1145 | Motor Mechanic                                | 5231     |
| 1146 | Motor Racing Driver                           | 3431     |
| 1147 | Motor Racing Organiser                        | 3557     |
| 1148 | Motor Trader                                  | 7131     |
| 1149 | Moulding Process Technician                   | 3116     |
| 1150 | Museum Assistant                              | 9231     |
| 1151 | Museum Attendant                              | 9267     |
| 1152 | Museum Consultant                             | 1224     |
| 1153 | Museum Technician                             | 6211     |
| 1154 | Music Producer                                | 3416     |
| 1155 | Music Teacher                                 | 2312     |
| 1156 | Music Therapist                               | 2229     |
| 1157 | Music Wholesaler                              | 3554     |
| 1158 | Musical Arranger                              | 3415     |
| 1159 | Musician                                      | 3415     |
| 1160 | Musician - Amateur                            | 3415     |
| 1161 | Nanny                                         | 6116     |
| 1162 | Naturalist                                    | 2112     |
| 1163 | Naturopath                                    | 3214     |
| 1164 | Navigator                                     | 3511     |
| 1165 | Negotiator                                    | 3555     |
| 1166 | Neurologist                                   | 2212     |
| 1167 | Newsagent                                     | 7131     |
| 1168 | Newsreader                                    | 3413     |
| 1169 | Night Porter                                  | 9253     |
| 1170 | Night Watchman                                | 9231     |
| 1171 | Non Comissioned Officer                       | 3311     |
| 1172 | Non Professional Footballer                   | 3431     |
| 1173 | Non Professional Sports Coach                 | 3432     |
| 1174 | Not Employed Due to Disability                | NULL     |
| 1175 | Not In Employment                             | NULL     |
| 1176 | Notary Public                                 | 2419     |
| 1177 | Nuclear Scientist                             | 2111     |
| 1178 | Nun                                           | 2314     |
| 1179 | Nurse                                         | 6131     |
| 1180 | Nursemaid                                     | 6114     |
| 1181 | Nursery Assistant                             | 5112     |
| 1182 | Nursery Nurse                                 | 6111     |
| 1183 | Nursery Worker                                | 6111     |
| 1184 | Nurseryman                                    | 5112     |
| 1185 | Nursing Assistant                             | 6131     |
| 1186 | Nursing Auxiliary                             | 6131     |
| 1187 | Nursing Manager                               | 1232     |
| 1188 | Nursing Officer                               | 2237     |
| 1189 | Nursing Sister                                | 2237     |
| 1190 | Nutritionist                                  | 2229     |
| 1191 | Obstetrician                                  | 2212     |
| 1192 | Occupational Health Consultant                | 2259     |
| 1193 | Occupational Health Nurse                     | 2237     |
| 1194 | Occupational Therapist                        | 2222     |
| 1195 | Oculist                                       | 2252     |
| 1196 | Off Shore Surveyor                            | 3541     |
| 1197 | Office Administrator                          | 4159     |
| 1198 | Office Manager                                | 4141     |
| 1199 | Oil Broker                                    | 3531     |
| 1200 | Oil Rig Crew                                  | 8132     |
| 1201 | Opera Singer                                  | 3413     |
| 1202 | Operations Director                           | 1171     |
| 1203 | Operations Engineer                           | 2122     |
| 1204 | Operations Manager                            | 1222     |
| 1205 | Operations Supervisor                         | 8160     |
| 1206 | Opthalmic Surgeon                             | 2212     |
| 1207 | Opthalmic Technician                          | 7114     |
| 1208 | Optical Advisor                               | 7114     |
| 1209 | Optical Assistant                             | 7114     |
| 1210 | Optical Technician                            | 5224     |
| 1211 | Optician                                      | 2252     |
| 1212 | Optometrist                                   | 2252     |
| 1213 | Orchestra Leader                              | 3415     |
| 1214 | Orchestral Violinist                          | 3415     |
| 1215 | Order Clerk                                   | 4133     |
| 1216 | Orderly                                       | 9211     |
| 1217 | Organist                                      | 3415     |
| 1218 | Ornamental Blacksmith                         | 5212     |
| 1219 | Ornithologist                                 | 2112     |
| 1220 | Orthopaedic Technician                        | 3213     |
| 1221 | Orthoptist                                    | 2229     |
| 1222 | Osteopath                                     | 2229     |
| 1223 | Ostler                                        | 6129     |
| 1224 | Outdoor Pursuits Instructor                   | 3432     |
| 1225 | Outfitter                                     | 7131     |
| 1226 | Outreach Worker                               | 3229     |
| 1227 | Overhead Line Instructor                      | 5250     |
| 1228 | Overhead Lineman                              | 5249     |
| 1229 | Overlocker                                    | 8146     |
| 1230 | Overseas Mailer                               | 9219     |
| 1231 | Overwriter                                    | 1131     |
| 1232 | Packaging Consultant                          | 3429     |
| 1233 | Packer                                        | 8132     |
| 1234 | Paediatrician                                 | 2212     |
| 1235 | Pager Operator                                | 9132     |
| 1236 | Paint Consultant                              | 5233     |
| 1237 | Paint Sprayer                                 | 8115     |
| 1238 | Paint Technologist                            | 5233     |
| 1239 | Painter                                       | 8149     |
| 1240 | Painter And Decorator                         | 5323     |
| 1241 | Palaeobotanist                                | 2112     |
| 1242 | Palaeobotologist                              | 2112     |
| 1243 | Pallet Maker                                  | 8131     |
| 1244 | Panel Beater                                  | 5211     |
| 1245 | Paper Mill Worker                             | 8131     |
| 1246 | Parachute Packer                              | 5449     |
| 1247 | Paramedic                                     | 2255     |
| 1248 | Park Attendant                                | 9112     |
| 1249 | Park Keeper                                   | 9112     |
| 1250 | Park Ranger                                   | 5119     |
| 1251 | Partition Erector                             | 5316     |
| 1252 | Parts Man                                     | 7132     |
| 1253 | Parts Manager                                 | 7132     |
| 1254 | Parts Supervisor                              | 4133     |
| 1255 | Party Planner                                 | 7129     |
| 1256 | Pasteuriser                                   | 8111     |
| 1257 | Pastry Chief                                  | 5434     |
| 1258 | Patent Agent                                  | 2129     |
| 1259 | Patent Attorney                               | 2482     |
| 1260 | Pathologist                                   | 2112     |
| 1261 | Patrol Person                                 | 9232     |
| 1262 | Patrolman                                     | 8132     |
| 1263 | Pattern Cutter                                | 5413     |
| 1264 | Pattern Maker                                 | 5419     |
| 1265 | Pattern Weaver                                | 5419     |
| 1266 | Paviour                                       | 8152     |
| 1267 | Pawnbroker                                    | 7122     |
| 1268 | Payment Officer                               | 4129     |
| 1269 | Payroll Assistant                             | 4122     |
| 1270 | Payroll Clerk                                 | 4122     |
| 1271 | Payroll Manager                               | 4122     |
| 1272 | Payroll Supervisor                            | 4122     |
| 1273 | Pearl Stringer                                | 8149     |
| 1274 | Pedicurist                                    | 6222     |
| 1275 | Pensions Consultant                           | 2422     |
| 1276 | Pensions Manager                              | 2422     |
| 1277 | Personal Assistant                            | 6135     |
| 1278 | Personnel Administrator                       | 4136     |
| 1279 | Personnel Manager                             | 1136     |
| 1280 | Personnel Officer                             | 3571     |
| 1281 | Pest Control                                  | 6121     |
| 1282 | Pest Controller                               | 6121     |
| 1283 | Pet Minder                                    | 6129     |
| 1284 | Petrol Station Attendant                      | 7112     |
| 1285 | Petroleum Engineer                            | 2121     |
| 1286 | Petty Officer                                 | 3311     |
| 1287 | Pharmaceutical Assistant                      | 7114     |
| 1288 | Pharmacist                                    | 2251     |
| 1289 | Pharmacy Manager                              | 2251     |
| 1290 | Pharmancy Technician                          | 3212     |
| 1291 | Philatelist                                   | 7131     |
| 1292 | Phlebotomist                                  | 6131     |
| 1293 | Photo Engraver                                | 5421     |
| 1294 | Photo Laboratory Assistant                    | 8135     |
| 1295 | Photo Technician                              | 8135     |
| 1296 | Photocopy Machine Technician                  | 8135     |
| 1297 | Photographer - Location                       | 3417     |
| 1298 | Photographer - Press                          | 3417     |
| 1299 | Photographer - Press (Local)                  | 3417     |
| 1300 | Photographer - Shop Owner                     | 3417     |
| 1301 | Photographer - Street                         | 3417     |
| 1302 | Photographer - Studio                         | 3417     |
| 1303 | Photographer Agent                            | 3417     |
| 1304 | Physician                                     | 2211     |
| 1305 | Physicist                                     | 2114     |
| 1306 | Physiologist                                  | 2259     |
| 1307 | Physiotherapist                               | 2221     |
| 1308 | Piano Teacher                                 | 2319     |
| 1309 | Piano Tuner                                   | 5449     |
| 1310 | Picker                                        | 8144     |
| 1311 | Picture Editor                                | 2491     |
| 1312 | Picture Framer                                | 5442     |
| 1313 | Picture Renovator                             | 5442     |
| 1314 | Picture Researcher                            | 2434     |
| 1315 | Pig Man                                       | 5111     |
| 1316 | Pig Manager                                   | 5111     |
| 1317 | Pilot                                         | 3511     |
| 1318 | Pilot Helicopter                              | 3511     |
| 1319 | Pilot Test                                    | 3511     |
| 1320 | Pipe Fitter                                   | 5214     |
| 1321 | Pipe Inspector                                | 5330     |
| 1322 | Pipe Insulator                                | 9129     |
| 1323 | Pipe Lawyer                                   | 8159     |
| 1324 | Planning Engineer                             | 2481     |
| 1325 | Planning Manager                              | 1139     |
| 1326 | Planning Officer                              | 2452     |
| 1327 | Planning Operator                             | 8120     |
| 1328 | Plasterer                                     | 8119     |
| 1329 | Plastics Consultants                          | 2125     |
| 1330 | Plastics Engineer                             | 2125     |
| 1331 | Plate Layer                                   | 8119     |
| 1332 | Plater                                        | 5423     |
| 1333 | Playgroup Assistant                           | 6111     |
| 1334 | Playgroup Leader                              | 6111     |
| 1335 | Playwright                                    | 3412     |
| 1336 | Plumber                                       | 5315     |
| 1337 | Plumbing & Heating Engineer                   | 5315     |
| 1338 | Podiatrist                                    | 2256     |
| 1339 | Pointsman                                     | 8239     |
| 1340 | Police Civillian                              | 3312     |
| 1341 | Police Comunity Support Officer               | 3132     |
| 1342 | Police Officer                                | 9231     |
| 1343 | Polisher                                      | 8120     |
| 1344 | Pool Attendant                                | 6211     |
| 1345 | Pools Collector                               | 7122     |
| 1346 | Port Officer                                  | 4134     |
| 1347 | Porter                                        | 9263     |
| 1348 | Portfolio Manager                             | 2494     |
| 1349 | Post Card Seller                              | 7131     |
| 1350 | Post Graduate Student - Living At Home        | 3572     |
| 1351 | Post Graduate Student - Living Away from Home | 3572     |
| 1352 | Post Office Counter Clerk                     | 4123     |
| 1353 | Post Sorter                                   | 9211     |
| 1354 | Post/Telegraph Officer                        | 7213     |
| 1355 | Postman                                       | 9211     |
| 1356 | Postmaster                                    | 4123     |
| 1357 | Postwoman                                     | 9211     |
| 1358 | Potato Merchant                               | 7131     |
| 1359 | Potter                                        | 8113     |
| 1360 | Poultry Worker                                | 9111     |
| 1361 | Practice Manager                              | 1231     |
| 1362 | Preacher                                      | 3229     |
| 1363 | Precious Metal Merchant                       | 7131     |
| 1364 | Precision Engineer                            | 5224     |
| 1365 | Premises Security Installers                  | 5245     |
| 1366 | Press Correspondent                           | 2492     |
| 1367 | Press Officer                                 | 2493     |
| 1368 | Press Operator                                | 8131     |
| 1369 | Press Photographer - Freelance                | 3417     |
| 1370 | Press Photographer - National                 | 3417     |
| 1371 | Press Producer                                | 2491     |
| 1372 | Press Setter                                  | 5221     |
| 1373 | Presser                                       | 8139     |
| 1374 | Priest                                        | 2463     |
| 1375 | Principal                                     | 2319     |
| 1376 | Print Finisher                                | 5423     |
| 1377 | Printer                                       | 9219     |
| 1378 | Printing Press Operator                       | 8135     |
| 1379 | Prison Chaplain                               | 2463     |
| 1380 | Prison Officer                                | 3314     |
| 1381 | Private Detective                             | 3319     |
| 1382 | Private Investigator                          | 3319     |
| 1383 | Privy Councillor                              | 1112     |
| 1384 | Probation Officer                             | 2462     |
| 1385 | Probation Worker                              | 2462     |
| 1386 | Process Engineer                              | 2125     |
| 1387 | Process Operator                              | 9132     |
| 1388 | Process Worker                                | 9132     |
| 1389 | Procurator Fiscal                             | 2411     |
| 1390 | Produce Supervisor                            | 7132     |
| 1391 | Producer                                      | 3416     |
| 1392 | Product Designer                              | 3429     |
| 1393 | Product Installer                             | 5317     |
| 1394 | Product Manager                               | 3556     |
| 1395 | Production Engineer                           | 2125     |
| 1396 | Production Hand                               | 9263     |
| 1397 | Production Manager                            | 2132     |
| 1398 | Production Planner                            | 3116     |
| 1399 | Professional Apprentice Footballer            | 3431     |
| 1400 | Professional Boxer                            | 3431     |
| 1401 | Professional Cricketer                        | 3431     |
| 1402 | Professional Cyclist                          | 3431     |
| 1403 | Professional Footballer                       | 3431     |
| 1404 | Professional Sports Coach                     | 3432     |
| 1405 | Professioanl Wrestler                         | 3431     |
| 1406 | Professor                                     | 2253     |
| 1407 | Progress Chaser                               | 4133     |
| 1408 | Progress Clerk                                | 4133     |
| 1409 | Project Co-ordinator                          | 2131     |
| 1410 | Project Engineer                              | 2127     |
| 1411 | Project Leader                                | 2131     |
| 1412 | Project Manager                               | 2131     |
| 1413 | Project Worker                                | 3229     |
| 1414 | Projectionist                                 | 3417     |
| 1415 | Promoter                                      | 3554     |
| 1416 | Promoter - Entertainments                     | 3554     |
| 1417 | Promoter - Racing                             | 3554     |
| 1418 | Promoter - Ring Sports                        | 3554     |
| 1419 | Promoter - Sports                             | 3554     |
| 1420 | Proof Reader                                  | 4159     |
| 1421 | Property Buyer                                | 1251     |
| 1422 | Property Dealer                               | 3555     |
| 1423 | Property Developer                            | 5319     |
| 1424 | Property Manager                              | 1251     |
| 1425 | Property Valuer                               | 3541     |
| 1426 | Proprietor                                    | 1241     |
| 1427 | Prosthetist                                   | 2259     |
| 1428 | Psychiatrist                                  | 2212     |
| 1429 | Psychoanalyst                                 | 2226     |
| 1430 | Psychodynamic Counsellor                      | 2224     |
| 1431 | Psychologist                                  | 2226     |
| 1432 | Psychotherapist                               | 2224     |
| 1433 | Public House Manager                          | 1223     |
| 1434 | Public Relations Officer                      | 2493     |
| 1435 | Publican                                      | 1223     |
| 1436 | Publicity Manager                             | 2493     |
| 1437 | Publisher                                     | 1255     |
| 1438 | Purchase Clerk                                | 4131     |
| 1439 | Purchase Ledger Clerk                         | 4122     |
| 1440 | Purchasing Manager                            | 1134     |
| 1441 | Purser                                        | 6213     |
| 1442 | Quality Controller                            | 2481     |
| 1443 | Quality Engineer                              | 2136     |
| 1444 | Quality Inspector                             | 8143     |
| 1445 | Quality Manager                               | 2132     |
| 1446 | Quality Technician                            | 3115     |
| 1447 | Quality Surveyor                              | 2453     |
| 1448 | Quarry Worker                                 | 8132     |
| 1449 | Queens Counsel                                | 2411     |
| 1450 | Rabbi                                         | 2463     |
| 1451 | Racehorse Groom                               | 6129     |
| 1452 | Racing Motorcyclist                           | 3431     |
| 1453 | Racing Organiser                              | 3554     |
| 1454 | Radar Mechanic                                | 5249     |
| 1455 | Radio Controller                              | 7213     |
| 1456 | Radio Director                                | 3416     |
| 1457 | Radio Engineer                                | 2124     |
| 1458 | Radio Helpline Coordinator                    | 3132     |
| 1459 | Radio Mechanic                                | 5243     |
| 1460 | Radio Operator                                | 7213     |
| 1461 | Radio Presenter                               | 3413     |
| 1462 | Radio Producer                                | 3416     |
| 1463 | Radiographer                                  | 3113     |
| 1464 | Radiologist                                   | 2481     |
| 1465 | Radiotherapist                                | 2212     |
| 1466 | Railman                                       | 8153     |
| 1467 | Rally Driver                                  | 3431     |
| 1468 | Ramp Agent                                    | 8233     |
| 1469 | Reactor Attendant                             | 2129     |
| 1470 | Reader                                        | 2253     |
| 1471 | Reader Compositor                             | 5421     |
| 1472 | Receptionist                                  | 9269     |
| 1473 | Records Supervisor                            | 4131     |
| 1474 | Recovery Vehicle Coordinator                  | 8219     |
| 1475 | Recruitment Consultant                        | 3571     |
| 1476 | Rector                                        | 2321     |
| 1477 | Referee                                       | 2211     |
| 1478 | Refit Merchandiser                            | 3553     |
| 1479 | Reflexologist                                 | 3214     |
| 1480 | Refractory Engineer                           | 5313     |
| 1481 | Refrigeration Engineer                        | 2129     |
| 1482 | Refuse Collector                              | 9225     |
| 1483 | Registered Disabled                           | NULL     |
| 1484 | Registrar                                     | 2322     |
| 1485 | Regulator                                     | 5449     |
| 1486 | Relocation Agent                              | 3555     |
| 1487 | Remedial Therapist                            | 2229     |
| 1488 | Rent Collector                                | 7122     |
| 1489 | Rent Officer                                  | 3541     |
| 1490 | Repair Man                                    | 8132     |
| 1491 | Reporter                                      | 3413     |
| 1492 | Reporter - Freelance                          | 3413     |
| 1493 | Reprographic Assistant                        | 9219     |
| 1494 | Rescue Worker                                 | 3229     |
| 1495 | Research Analyst                              | 3544     |
| 1496 | Research Consultant                           | 2434     |
| 1497 | Research Director                             | 2161     |
| 1498 | Research Scientist                            | 2112     |
| 1499 | Research Technician                           | 2434     |
| 1500 | Researcher                                    | 2112     |
| 1501 | Re-Settlement Officer                         | 4159     |
| 1502 | Resin Caster                                  | 8115     |
| 1503 | Restaurant Manager                            | 1222     |
| 1504 | Restaurateur                                  | 1222     |
| 1505 | Restorer                                      | 5442     |
| 1506 | Retired                                       | NULL     |
| 1507 | Revenue Clerk                                 | 4111     |
| 1508 | Revenue Officer                               | 4111     |
| 1509 | Rheumatologist                                | 2212     |
| 1510 | Rig Worker                                    | 8132     |
| 1511 | Rigger                                        | 3417     |
| 1512 | Riverman                                      | 9129     |
| 1513 | Riveter                                       | 8142     |
| 1514 | Road Safety Officer                           | 3582     |
| 1515 | Road Sweeper                                  | 9222     |
| 1516 | Road Worker                                   | 8152     |
| 1517 | Roof Tiler                                    | 5314     |
| 1518 | Roofer                                        | 5314     |
| 1519 | Rose Grower                                   | 9112     |
| 1520 | Royal Marine                                  | 1161     |
| 1521 | Rubber Worker                                 | 8119     |
| 1522 | Rug Maker                                     | 5419     |
| 1523 | Rugby Player - Amateur                        | 3431     |
| 1524 | Rugby Player - Professional                   | 3431     |
| 1525 | Saddler                                       | 5412     |
| 1526 | Safety Officer                                | 3582     |
| 1527 | Sail Maker                                    | 5419     |
| 1528 | Sales Administrator                           | 4151     |
| 1529 | Sales Assistant                               | 7112     |
| 1530 | Sales Director                                | 1132     |
| 1531 | Sales Engineer                                | 3552     |
| 1532 | Sales Executive                               | 7113     |
| 1533 | Sales Manager                                 | 1131     |
| 1534 | Sales Representative                          | 7129     |
| 1535 | Sales Support                                 | 4151     |
| 1536 | Sales Women                                   | 7111     |
| 1537 | Salesman                                      | 3552     |
| 1538 | Salvage Dealer                                | 8132     |
| 1539 | Salvage Hand                                  | 8132     |
| 1540 | Sample Hand                                   | 8146     |
| 1541 | Sand Blaster                                  | 5442     |
| 1542 | Sand Merchant                                 | 7131     |
| 1543 | Saw Miller                                    | 8131     |
| 1544 | Sawyer                                        | 5449     |
| 1545 | Scaffolder                                    | 8151     |
| 1546 | Scenehand                                     | 9269     |
| 1547 | School Counseller                             | 3224     |
| 1548 | School Cross Warden                           | 9232     |
| 1549 | School Inspector                              | 2323     |
| 1550 | School Student                                | NULL     |
| 1551 | Scientist - Atomic Energy                     | 2254     |
| 1552 | Scrap Dealer                                  | 1254     |
| 1553 | Screen Printer                                | 5422     |
| 1554 | Screen Writer                                 | 3412     |
| 1555 | Script Writer                                 | 3412     |
| 1556 | Sculptor                                      | 3411     |
| 1557 | Seaman                                        | 3311     |
| 1558 | Seamstress                                    | 8146     |
| 1559 | Second Hand Dealer                            | 8115     |
| 1560 | Secretary                                     | 4213     |
| 1561 | Scretary And PA                               | 4215     |
| 1562 | Security Consultant                           | 3319     |
| 1563 | Security Controller                           | 9231     |
| 1564 | Security Guard                                | 9231     |
| 1565 | Security Officer                              | 9231     |
| 1566 | Seedsman                                      | 7131     |
| 1567 | Servant                                       | 9223     |
| 1568 | Service Engineer                              | 5234     |
| 1569 | Service Engineer (Non-Mobile)                 | 5242     |
| 1570 | Service Manager                               | 2132     |
| 1571 | Setter                                        | 5449     |
| 1572 | Sewage Worker                                 | 8134     |
| 1573 | Share Dealer                                  | 3531     |
| 1574 | Sheet Metal Worker                            | 5211     |
| 1575 | Shelf Filler                                  | 9241     |
| 1576 | Shelter Warden                                | 6135     |
| 1577 | Shepherd                                      | 9111     |
| 1578 | Sheriff                                       | 2411     |
| 1579 | Sheriff Clerk                                 | 2411     |
| 1580 | Sheriff Principal                             | 2411     |
| 1581 | Sheriffs Officer                              | 9231     |
| 1582 | Shift Controller                              | 8134     |
| 1583 | Ship Broker                                   | 3531     |
| 1584 | Ship Builder                                  | 5235     |
| 1585 | Shipping Clerk                                | 4134     |
| 1586 | Shipwright                                    | 5235     |
| 1587 | Shipyard Worker                               | 9259     |
| 1588 | Shoe Maker                                    | 5412     |
| 1589 | Shoe Repair                                   | 5412     |
| 1590 | Shooting Instructor                           | 3432     |
| 1591 | Shop Assistant                                | 7111     |
| 1592 | Shop Fitter                                   | 5223     |
| 1593 | Shop Keeper                                   | 7131     |
| 1594 | Shop Manager                                  | 1150     |
| 1595 | Shorthand Writer                              | 4217     |
| 1596 | Shot Blaster                                  | 8120     |
| 1597 | Show Jumper                                   | 3431     |
| 1598 | Showman                                       | 3413     |
| 1599 | Shunter                                       | 8212     |
| 1600 | Sign Maker                                    | 5449     |
| 1601 | Signalman                                     | 8221     |
| 1602 | Signalwriter                                  | 5449     |
| 1603 | Silversmith                                   | 5449     |
| 1604 | Singer                                        | 3413     |
| 1605 | Site Agent                                    | 3555     |
| 1606 | Skipper                                       | 3512     |
| 1607 | Slater                                        | 5314     |
| 1608 | Slaughterman                                  | 5431     |
| 1609 | Smallholder                                   | 9112     |
| 1610 | Smelter                                       | 8119     |
| 1611 | Snooker Player                                | 3431     |
| 1612 | Social Worker                                 | 2461     |
| 1613 | Sociolagist                                   | 2115     |
| 1614 | Software Consultant                           | 2134     |
| 1615 | Software Engineer                             | 2134     |
| 1616 | Solar Panel Installer And Repairer            | 5241     |
| 1617 | Soldier                                       | 3311     |
| 1618 | Solicitor                                     | 2412     |
| 1619 | Song Writer                                   | 3415     |
| 1620 | Sound Editor                                  | 3417     |
| 1621 | Sound Engineer                                | 3417     |
| 1622 | Sound Mixer                                   | 3417     |
| 1623 | Sound Technician                              | 3417     |
| 1624 | Special Constable                             | 9231     |
| 1625 | Special Needs Assistant                       | 6113     |
| 1626 | Speech Therapist                              | 2223     |
| 1627 | Sports Administrator                          | 6211     |
| 1628 | Sports Agent                                  | 3432     |
| 1629 | Sports Centre Attendant                       | 6211     |
| 1630 | Sports Coach                                  | 3432     |
| 1631 | Sports Comentator                             | 2434     |
| 1632 | Sports Equipment Repairer                     | 5449     |
| 1633 | Sports Scout                                  | 3432     |
| 1634 | Sportsman                                     | 3431     |
| 1635 | Sportswomen                                   | 3431     |
| 1636 | Spring Maker                                  | 5212     |
| 1637 | Stable Hand                                   | 6129     |
| 1638 | Staff Nurse                                   | 2237     |
| 1639 | Stage Director                                | 3416     |
| 1640 | Stage Manager                                 | 3416     |
| 1641 | Stage Mover                                   | 9269     |
| 1642 | Stamp Dealer                                  | 7131     |
| 1643 | State Enrolled Nurse                          | 2237     |
| 1644 | State Registered Nurse                        | 2237     |
| 1645 | Station Manager                               | 1163     |
| 1646 | Stationer                                     | 5422     |
| 1647 | Statistician                                  | 2433     |
| 1648 | Steel Erector                                 | 5311     |
| 1649 | Steel Worker                                  | 5235     |
| 1650 | Steeplejack                                   | 5319     |
| 1651 | Stenographer                                  | 4217     |
| 1652 | Stevedore                                     | 9259     |
| 1653 | Steward                                       | 9264     |
| 1654 | Stewardess                                    | 6213     |
| 1655 | Stock Controller                              | 4133     |
| 1656 | Stock Exchange Dealer                         | 3531     |
| 1657 | Stock Manager                                 | 1211     |
| 1658 | Stockbroker                                   | 3531     |
| 1659 | Stockman                                      | 9111     |
| 1660 | Stocktaker                                    | 9252     |
| 1661 | Stone Cutter                                  | 5312     |
| 1662 | Stone Sawyer                                  | 5213     |
| 1663 | Stonemason                                    | 5312     |
| 1664 | Store Detective                               | 9231     |
| 1665 | Storeman                                      | 9252     |
| 1666 | Stores Controller                             | 4133     |
| 1667 | Storewomen                                    | 9252     |
| 1668 | Street Entertainer                            | 3413     |
| 1669 | Street Trader                                 | 7124     |
| 1670 | Stud Hand                                     | 6129     |
| 1671 | Student - Living At Home                      | NULL     |
| 1672 | Student - Living Away                         | NULL     |
| 1673 | Student Counseller                            | 3224     |
| 1674 | Student Nurse - Living At Home                | 2237     |
| 1675 | Student Nurse - Living Away                   | 2237     |
| 1676 | Student Teacher - Living At Home              | 6112     |
| 1677 | Student Teacher - Living Away                 | 6112     |
| 1678 | Studio Manager                                | 3416     |
| 1679 | Sub-postmaster                                | 4123     |
| 1680 | Superintendent                                | 3574     |
| 1681 | Supervisor                                    | 4122     |
| 1682 | Supply Teacher                                | 2312     |
| 1683 | Support Worker                                | 6113     |
| 1684 | Surgeon                                       | 2212     |
| 1685 | Surgeon - N.H.S                               | 2212     |
| 1686 | Surgeon - Non N.H.S                           | 2212     |
| 1687 | Surveyor                                      | 3541     |
| 1688 | Surveyor - Chartered                          | 2454     |
| 1689 | Systems Analyst                               | 2133     |
| 1690 | Systems Engineer                              | 2133     |
| 1691 | Systems Manager                               | 1121     |
| 1692 | Tachograph Analyst                            | 3544     |
| 1693 | Tacker                                        | 5419     |
| 1694 | Tailor                                        | 5413     |
| 1695 | Tank Farm Operative                           | 8112     |
| 1696 | Tanker Driver                                 | 8211     |
| 1697 | Tanner                                        | 8112     |
| 1698 | Tape Librarian                                | 2471     |
| 1699 | Tape Operator                                 | 3417     |
| 1700 | Tarmacer                                      | 8152     |
| 1701 | Tarot Reader/Palmistry Expert                 | 9269     |
| 1702 | Taster                                        | 8144     |
| 1703 | Tattooist                                     | 6222     |
| 1704 | Tax Advisor                                   | 2423     |
| 1705 | Tax Analyst                                   | 2423     |
| 1706 | Tax Assistant                                 | 4111     |
| 1707 | Tax Consultant                                | 2423     |
| 1708 | Tax Inspector                                 | 2423     |
| 1709 | Tax Manager                                   | 2423     |
| 1710 | Tax Officer                                   | 4111     |
| 1711 | Taxi Controller                               | 7213     |
| 1712 | Taxi Driver                                   | 8213     |
| 1713 | Taxidermist                                   | 6138     |
| 1714 | Tea Blender                                   | 8111     |
| 1715 | Tea Taster                                    | 8144     |
| 1716 | Teacher                                       | 2319     |
| 1717 | Teachers Assistant                            | 6112     |
| 1718 | Technical Advisor                             | 2421     |
| 1719 | Technical Analyst                             | 2133     |
| 1720 | Technical Assistant                           | 3132     |
| 1721 | Technical Author                              | 3412     |
| 1722 | Technical Clerk                               | 4131     |
| 1723 | Technical Co-ordinator                        | 3132     |
| 1724 | Technical Director                            | 1255     |
| 1725 | Technical Editor                              | 3412     |
| 1726 | Technical Engineer                            | 3132     |
| 1727 | Technical Illustrator                         | 2142     |
| 1728 | Technical Instructor                          | 6131     |
| 1729 | Technical Liaison Engineer                    | 2129     |
| 1730 | Technical Manager                             | 2132     |
| 1731 | Technician                                    | 5242     |
| 1732 | Technician - Performing Arts                  | 3417     |
| 1733 | Technologist                                  | 2129     |
| 1734 | Telecomunication Consultant                   | 2139     |
| 1735 | Telecomunications Engineer                    | 5242     |
| 1736 | Telecomunications Manager                     | 2132     |
| 1737 | Telegraphist                                  | 7213     |
| 1738 | Telemarketeer                                 | 7113     |
| 1739 | Telephone Engineer                            | 2123     |
| 1740 | Telesales Person                              | 7113     |
| 1741 | Television Director                           | 3416     |
| 1742 | Television Engineer                           | 2124     |
| 1743 | Television Presenter                          | 3413     |
| 1744 | Television Producer                           | 3416     |
| 1745 | Television Set Builder                        | 8141     |
| 1746 | Telex Operator                                | 7213     |
| 1747 | Temperature Time Recorder                     | 4159     |
| 1748 | Tennis Coach                                  | 3432     |
| 1749 | Terrier                                       | 6129     |
| 1750 | Test Driver                                   | 8219     |
| 1751 | Test Engineer                                 | 2136     |
| 1752 | Textile Consultant                            | 3119     |
| 1753 | Textile Engineer                              | 2122     |
| 1754 | Textile Technician                            | 3116     |
| 1755 | Textile Worker                                | 8112     |
| 1756 | Thatcher                                      | 5314     |
| 1757 | Theatre Director                              | 3416     |
| 1758 | Theatre Manager                               | 2233     |
| 1759 | Theatre Technician                            | 3417     |
| 1760 | Theatrical Agent                              | 1255     |
| 1761 | Theme Park Worker                             | 9267     |
| 1762 | Therapist                                     | 3219     |
| 1763 | Thermal Engineer                              | 2129     |
| 1764 | Thermal Insulator                             | 8139     |
| 1765 | Ticket Agent                                  | 4129     |
| 1766 | Ticket Collector                              | 6214     |
| 1767 | Ticket Inspector                              | 8239     |
| 1768 | Tiler                                         | 5322     |
| 1769 | Timber Inspector                              | 8143     |
| 1770 | Timber Worker                                 | 9139     |
| 1771 | Time and Motion Analyst                       | 3544     |
| 1772 | Tobacconist                                   | 7131     |
| 1773 | Toll Collector                                | 7122     |
| 1774 | Tool Maker                                    | 5222     |
| 1775 | Tool Setter                                   | 5221     |
| 1776 | Tour Agent                                    | 6212     |
| 1777 | Tour Guide                                    | 6219     |
| 1778 | Town Clerk                                    | 1139     |
| 1779 | Town Planner                                  | 2452     |
| 1780 | Toy Maker                                     | 5449     |
| 1781 | Toy Trader                                    | 7131     |
| 1782 | Track Worker                                  | 8142     |
| 1783 | Tractor Driver                                | 8229     |
| 1784 | Tractor Mechanic                              | 5223     |
| 1785 | Trade Mark Agent                              | 3549     |
| 1786 | Trade Union Official                          | 4113     |
| 1787 | Trading Standards Office                      | 3581     |
| 1788 | Traffic Clerk                                 | 4134     |
| 1789 | Traffic Despatcher                            | 8233     |
| 1790 | Traffic Engineer                              | 2129     |
| 1791 | Traffic Officer                               | 4134     |
| 1792 | Traffic Planner                               | 2455     |
| 1793 | Traffic Supervisor                            | 4134     |
| 1794 | Traffic Warden                                | 6312     |
| 1795 | Train Despatcher                              | 8144     |
| 1796 | Train Driver                                  | 8231     |
| 1797 | Train Motorman                                | 8133     |
| 1798 | Trainee Manager                               | 3574     |
| 1799 | Trainer                                       | 3432     |
| 1800 | Training Advisor                              | 3574     |
| 1801 | Training Assistant                            | 4136     |
| 1802 | Training Co-ordinator                         | 3574     |
| 1803 | Training Instructor                           | 3574     |
| 1804 | Training Manager                              | 3574     |
| 1805 | Training Officer                              | 3574     |
| 1806 | Transcriber                                   | 4217     |
| 1807 | Translator                                    | 3412     |
| 1808 | Transmission Controller                       | 2124     |
| 1809 | Transport Clerk                               | 4134     |
| 1810 | Transport Consultant                          | 3549     |
| 1811 | Transport Controller                          | 4134     |
| 1812 | Transport Engineer                            | 2129     |
| 1813 | Transport Manager                             | 1241     |
| 1814 | Transport Officer                             | 3113     |
| 1815 | Transport Planner                             | 2455     |
| 1816 | Transport Policeman                           | 3312     |
| 1817 | Travel Agent                                  | 6212     |
| 1818 | Travel Clerk                                  | 6212     |
| 1819 | Travel Consultant                             | 6212     |
| 1820 | Travel Courier                                | 6219     |
| 1821 | Travel Guide                                  | 6219     |
| 1822 | Travel Guide Writer                           | 3412     |
| 1823 | Travel Representative                         | 6219     |
| 1824 | Travelling Salesman/Women                     | 7121     |
| 1825 | Trawler Hand                                  | 9119     |
| 1826 | Treasurer                                     | 1131     |
| 1827 | Tree Feller                                   | 9112     |
| 1828 | Tree Surgeon                                  | 5119     |
| 1829 | Trichologist                                  | 3213     |
| 1830 | Trinity House Pilot                           | 3512     |
| 1831 | Trout Farmer                                  | 5111     |
| 1832 | Truck Driver                                  | 8211     |
| 1833 | T-Shirt Printer                               | 5422     |
| 1834 | Tug Skipper                                   | 3512     |
| 1835 | Tunneller                                     | 8132     |
| 1836 | Turf Accountant                               | 6211     |
| 1837 | Turkey Farmer                                 | 5111     |
| 1838 | Turner                                        | 5221     |
| 1839 | Tutor                                         | 2319     |
| 1840 | TV And Video Installer                        | 5243     |
| 1841 | TV And Video Repairer                         | 5246     |
| 1842 | TV Announcer                                  | 3413     |
| 1843 | TV Broadcasting Technician                    | 3417     |
| 1844 | TV Editor                                     | 3416     |
| 1845 | Typesetter                                    | 5421     |
| 1846 | Typewriter Engineer                           | 5246     |
| 1847 | Typist                                        | 4217     |
| 1848 | Typographer                                   | 5421     |
| 1849 | Tyre Builder                                  | 8119     |
| 1850 | Tyre Fitter                                   | 8145     |
| 1851 | Tyre Inspector                                | 8143     |
| 1852 | Tyre Technician                               | 8145     |
| 1853 | Tyre/Exhaust Fitter                           | 8145     |
| 1854 | Umpire                                        | 3432     |
| 1855 | Undergraduate Student - Living At Home        | NULL     |
| 1856 | Undergraduate Student -Living Away from Home  | NULL     |
| 1857 | Undertaker                                    | 6138     |
| 1858 | Underwriter                                   | 3532     |
| 1859 | Unemployed                                    | NULL     |
| 1860 | University Dean                               | 2322     |
| 1861 | University Reader                             | 2311     |
| 1862 | Upholsterer                                   | 5411     |
| 1863 | Usher                                         | 9267     |
| 1864 | Valet                                         | 9226     |
| 1865 | Valuer                                        | 3541     |
| 1866 | Valve Technician                              | 5223     |
| 1867 | Van Driver                                    | 8214     |
| 1868 | Van Salesman                                  | 3552     |
| 1869 | VDU Operator                                  | 4152     |
| 1870 | Vehicle Assessor                              | 3541     |
| 1871 | Vehicle Bodybuilder                           | 5232     |
| 1872 | Vehicle Delivery Agent                        | 8214     |
| 1873 | Vehicle Engineer                              | 5231     |
| 1874 | Vehicle Technician                            | 5231     |
| 1875 | Vending Machine Filler                        | 7122     |
| 1876 | Vending Machine Technician                    | 7122     |
| 1877 | Ventriloquist                                 | 3413     |
| 1878 | Verger                                        | 6232     |
| 1879 | Veterinary Assistant                          | 6129     |
| 1880 | Veterinary Nurse                              | 3240     |
| 1881 | Veterinary Surgeon                            | 2240     |
| 1882 | Vicar                                         | 2463     |
| 1883 | Video Supplier                                | 3417     |
| 1884 | Videotape Engineer                            | 5243     |
| 1885 | Violin Maker                                  | 5449     |
| 1886 | Vision Control Manager                        | 5243     |
| 1887 | Vision Mixer                                  | 3417     |
| 1888 | Voluntary Worker                              | 2339     |
| 1889 | Vulcanologist                                 | 2114     |
| 1890 | Wages Clerk                                   | 4122     |
| 1891 | Waiter                                        | 9211     |
| 1892 | Waitress                                      | 9264     |
| 1893 | Warden                                        | 6232     |
| 1894 | Wardrobe Mistress                             | 6211     |
| 1895 | Warehouse Manager                             | 1242     |
| 1896 | Warehouseman                                  | 9252     |
| 1897 | Warehousewomen                                | 9252     |
| 1898 | Waste Dealer                                  | 1254     |
| 1899 | Waste Disposal Worker                         | 8132     |
| 1900 | Waste Paper Merchant                          | 1254     |
| 1901 | Watchmaker                                    | 5224     |
| 1902 | Water Diviner                                 | 9269     |
| 1903 | Weaver                                        | 5449     |
| 1904 | Web Designer                                  | 2141     |
| 1905 | Web Developer                                 | 2134     |
| 1906 | Web Programmer                                | 2134     |
| 1907 | Weighbridge Clerk                             | 8144     |
| 1908 | Weighbridge Operator                          | 8144     |
| 1909 | Welder                                        | 5317     |
| 1910 | Welfare Assistant                             | 9232     |
| 1911 | Welfare Officer                               | 3229     |
| 1912 | Welfare Rights Officer                        | 3229     |
| 1913 | Wheel Clamper                                 | 6312     |
| 1914 | Wheelwright                                   | 5442     |
| 1915 | Wiskey Blender                                | 8111     |
| 1916 | Wholesale Newspaper Delivery Driver           | 8214     |
| 1917 | Will Writer                                   | 3520     |
| 1918 | Window Cleaner                                | 9221     |
| 1919 | Window Dresser                                | 7125     |
| 1920 | Windscreen Fitter                             | 8145     |
| 1921 | Wine Merchant                                 | 7131     |
| 1922 | Wood Carver                                   | 5442     |
| 1923 | Wood Cutter                                   | 9112     |
| 1924 | Wood Machinist                                | 8131     |
| 1925 | Wood Worker                                   | 5316     |
| 1926 | Word Processing Operator                      | 4217     |
| 1927 | Work Study Analyst                            | 3544     |
| 1928 | Works Manager                                 | 1122     |
| 1929 | Writer                                        | 4111     |
| 1930 | Yacht Master                                  | 3512     |
| 1931 | Yard Man                                      | 8144     |
| 1932 | Yard Manager                                  | 1212     |
| 1933 | Yoga Teacher                                  | 3433     |
| 1934 | Youth Hostel Warden                           | 9269     |
| 1935 | Youth Worker                                  | 2464     |
| 1936 | Zoo Keeper                                    | 6129     |
| 1937 | Zoo Manager                                   | 1224     |
| 1938 | Zoologist                                     | 2112     |
| 1939 | Zoology Consultant                            | 2112     |
