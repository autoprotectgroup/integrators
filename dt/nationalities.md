# Nationalities

| ID  | Nationality                      |
|-----|----------------------------------|
| 1   | United Kingdom                   |
| 2   | Ireland                          |
| 3   | Afghanistan                      |
| 4   | Albania                          |
| 5   | Algeria                          |
| 6   | American Samoa                   |
| 7   | Andorra                          |
| 8   | Angola                           |
| 9   | Anguilla                         |
| 10  | Antarctica                       |
| 11  | Antigua and Barbuda              |
| 12  | Argentina                        |
| 13  | Armenia                          |
| 14  | Aruba                            |
| 15  | Australia                        |
| 16  | Austria                          |
| 17  | Azerbaijan                       |
| 18  | Bahamas                          |
| 19  | Bahrain                          |
| 20  | Bangladesh                       |
| 21  | Barbados                         |
| 22  | Belarus                          |
| 23  | Belgium                          |
| 24  | Belize                           |
| 25  | Benin                            |
| 26  | Bermuda                          |
| 27  | Bhutan                           |
| 28  | Bolivia                          |
| 29  | Bosnia and Herzegowina           |
| 30  | Botswana                         |
| 31  | Bouvet Island                    |
| 32  | Brazil                           |
| 33  | British Indian Ocean Territory   |
| 34  | Brunei Darussalam                |
| 35  | Bulgaria                         |
| 36  | Burkina Faso                     |
| 37  | Burundi                          |
| 38  | Cambodia                         |
| 39  | Cameroon                         |
| 40  | Canada                           |
| 41  | Cape Verde                       |
| 42  | Cayman Islands                   |
| 43  | Central African Republic         |
| 44  | Chad                             |
| 45  | Chile                            |
| 46  | China                            |
| 47  | Christmas Island                 |
| 48  | Cocos (Keeling) Islands          |
| 49  | Colombia                         |
| 50  | Comoros                          |
| 51  | Congo, Democratic Republic of    |
| 52  | Congo, Peoples Republic of       |
| 53  | Cook Islands                     |
| 54  | Costa Rica                       |
| 55  | Cote Divoire                     |
| 56  | Croatia                          |
| 57  | Cuba                             |
| 58  | Cyprus                           |
| 59  | Czech Republic                   |
| 60  | Denmark                          |
| 61  | Djibouti                         |
| 62  | Dominica                         |
| 63  | Dominican Republic               |
| 64  | East Timor                       |
| 65  | Ecuador                          |
| 66  | Egypt                            |
| 67  | El Salvador                      |
| 68  | Equatorial Guinea                |
| 69  | Eritrea                          |
| 70  | Estonia                          |
| 71  | Ethiopia                         |
| 72  | Falkland Islands                 |
| 73  | Faroe Islands                    |
| 74  | Fiji                             |
| 75  | Finland                          |
| 76  | France                           |
| 77  | France, Metropolitan             |
| 78  | French Guiana                    |
| 79  | French Polynesia                 |
| 80  | French Southern Territories      |
| 81  | Gabon                            |
| 82  | Gambia                           |
| 83  | Georgia                          |
| 84  | Germany                          |
| 85  | Ghana                            |
| 86  | Gibraltar                        |
| 87  | Greece                           |
| 88  | Greenland                        |
| 89  | Grenada                          |
| 90  | Guadeloupe                       |
| 91  | Guam                             |
| 92  | Guatemala                        |
| 93  | Guinea                           |
| 94  | Guinea-Bissau                    |
| 95  | Guyana                           |
| 96  | Haiti                            |
| 97  | Heard and McDonald Islands       |
| 98  | Honduras                         |
| 99  | Hong Kong                        |
| 100 | Hungary                          |
| 101 | Iceland                          |
| 102 | India                            |
| 103 | Indonesia                        |
| 104 | Iran (Islamic Republic of)       |
| 105 | Iraq                             |
| 106 | Israel                           |
| 107 | Italy                            |
| 108 | Jamaica                          |
| 109 | Japan                            |
| 110 | Jordan                           |
| 111 | Kazakhstan                       |
| 112 | Kenya                            |
| 113 | Kiribati                         |
| 114 | Korea, Democratic Republic of    |
| 115 | Korea, Republic of               |
| 116 | Kuwait                           |
| 117 | Kyrgyzstan                       |
| 118 | Lao Peoples Democratic Republic  |
| 119 | Latvia                           |
| 120 | Lebanon                          |
| 121 | Lesotho                          |
| 122 | Liberia                          |
| 123 | Libyan Arab Jamahiriya           |
| 124 | Liechtenstein                    |
| 125 | Lithuania                        |
| 126 | Luxembourg                       |
| 127 | Macau                            |
| 128 | Macedonia, The Republic of       |
| 129 | Madagascar                       |
| 130 | Malawi                           |
| 131 | Malaysia                         |
| 132 | Maldives                         |
| 133 | Mali                             |
| 134 | Malta                            |
| 135 | Marshall Islands                 |
| 136 | Martinique                       |
| 137 | Mauritania                       |
| 138 | Mauritius                        |
| 139 | Mayotte                          |
| 140 | Mexico                           |
| 141 | Micronesia, Federated States of  |
| 142 | Moldova, Republic of             |
| 143 | Monaco                           |
| 144 | Mongolia                         |
| 145 | Montserrat                       |
| 146 | Morocco                          |
| 147 | Mozambique                       |
| 148 | Myanmar                          |
| 149 | Namibia                          |
| 150 | Nauru                            |
| 151 | Nepal                            |
| 152 | Netherlands                      |
| 153 | Netherlands Antilles             |
| 154 | New Caledonia                    |
| 155 | New Zealand                      |
| 156 | Nicaragua                        |
| 157 | Niger                            |
| 158 | Nigeria                          |
| 159 | Niue                             |
| 160 | Norfolk Island                   |
| 161 | Northern Mariana Islands         |
| 162 | Norway                           |
| 163 | Oman                             |
| 164 | Pakistan                         |
| 165 | Palau                            |
| 166 | Palestinian Territory, Occupied  |
| 167 | Panama                           |
| 168 | Papua New Guinea                 |
| 169 | Paraguay                         |
| 170 | Peru                             |
| 171 | Philippines                      |
| 172 | Pitcairn                         |
| 173 | Poland                           |
| 174 | Portugal                         |
| 175 | Puerto Rico                      |
| 176 | Qatar                            |
| 177 | Reunion                          |
| 178 | Romania                          |
| 179 | Russian Federation               |
| 180 | Rwanda                           |
| 181 | Saint Kitts and Nevis            |
| 182 | Saint Lucia                      |
| 183 | Saint Vincent and The Grenadines |
| 184 | Samoa                            |
| 185 | San Marino                       |
| 186 | Sao Tome and Principe            |
| 187 | Saudi Arabia                     |
| 188 | Senegal                          |
| 189 | Seychelles                       |
| 190 | Sierra Leone                     |
| 191 | Singapore                        |
| 192 | Slovakia (Slovak Republic)       |
| 193 | Slovenia                         |
| 194 | Solomon Islands                  |
| 195 | Somalia                          |
| 196 | South Africa                     |
| 197 | South Georgia and Sandwich Is    |
| 198 | Spain                            |
| 199 | Sri Lanka                        |
| 200 | St. Helena                       |
| 201 | St. Pierre and Miquelon          |
| 202 | Sudan                            |
| 203 | Suriname                         |
| 204 | Svalbard and Jan Mayen Islands   |
| 205 | Swaziland                        |
| 206 | Sweden                           |
| 207 | Switzerland                      |
| 208 | Syrian Arab Republic             |
| 209 | Taiwan                           |
| 210 | Tajikistan                       |
| 211 | Tanzania, United Republic of     |
| 212 | Thailand                         |
| 213 | Togo                             |
| 214 | Tokelau                          |
| 215 | Tonga                            |
| 216 | Trinidad and Tobago              |
| 217 | Tunisia                          |
| 218 | Turkey                           |
| 219 | Turkmenistan                     |
| 220 | Turks and Caicos Islands         |
| 221 | Tuvalu                           |
| 222 | Uganda                           |
| 223 | Ukraine                          |
| 224 | United Arab Emirates             |
| 225 | United States                    |
| 226 | United States Outlying Islands   |
| 227 | Uruguay                          |
| 228 | Uzbekistan                       |
| 229 | Vanuatu                          |
| 230 | Vatican City State (Holy See)    |
| 231 | Venezuela                        |
| 232 | Viet Nam                         |
| 233 | Virgin Islands (British)         |
| 234 | Virgin Islands (U.S.)            |
| 235 | Wallis and Futuna Islands        |
| 236 | Western Sahara                   |
| 237 | Yemen                            |
| 238 | Yugoslavia                       |
| 239 | Zambia                           |
| 240 | Zimbabwe                         |
