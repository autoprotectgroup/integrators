# DealTrak APIs

## For Dealers / Web Developers / DMS Providers

### Loading proposals into DealTrak

To load proposals into DealTrak, for example from a website application for or a DMS, you may use the Rosetta SOAP [Application](rosetta.md#application) module. Alternatively, a number of plain XML APIs are available, as well as a plugin for the WordPress CMS.

#### DealTrak enumerations

DealTrak uses a number of standard enumerations for values such as name title, residential status, finance type etc. The APIs will not accept the text equivalents, ie: you must send ''<name_title>1</name_title>'' and not ''<name_title>Mr</name_title>''.

[Full list of enumerations](enumerations.md)

#### Load Lead 2

Load lead is suitable for handling data received via lead capture forms or data transferred from 3rd party platforms such as dealer management or CRM systems.

[Load Lead 2](loadlead.md)

#### Load application

The load application service allows a full proposal to be loaded into DealTrak.

[Load Application](loadApplication.md)

#### 3rd party plugins

A [WordPress plugin](wordpress.md) is available for clients wishing to present a lead capture for on their websites.

#### Processing proposals

The Rosetta SOAP [Operations](rosetta.md#operations) module contains functionality to send proposals to lenders.

#### Send application

The send application service can be used to trigger sending of an existing proposal to a particular lender.

[Send Application](sendApplication.md)

#### List active applications

Allows a user to request a list of the proposals currently in the "Pending Deals" tab.

[List Active Applications](listApplications.md)

#### Proposal Status

Allows users to request the status of a proposal, complete with notes.

[Proposal Status](proposalStatus.md)

#### Check Auto-send

The check auto-send call allows 3rd party applications to trigger evaluation of a proposal against the auto-send rule set and determine the next auto-send action.

[Check Auto-send](checkAutosend.md)

### SOAP APIs

The Rosetta SOAP APIs provide an interface to load, manage and submit proposals within DealTrak.

To get started with development using these APIs, we recommend using a SOAP client such as [SoapUI](https://www.soapui.org/), which can automatically generate sample requests for all available calls. You can append ''?wsdl'' to the API endpoint when configuring to obtain a WSDL.

An auto-generated overview of the services is available here:

[https://www.dealtrak123.co.uk/rosetta/webservices/](https://www.dealtrak123.co.uk/rosetta/webservices/)

There are 7 modules:

| Module                                      | Description                                                                                                                                                         |
|---------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Application](rosetta.md#application)       | Create a finance application, with methods to added, edit, get and remove application sections such as applicants, residences, employment, vehicles and quotations. |
| [Credit Enquiry](rosetta.md#credit-enquiry) | Preform a soft foot print credit enquiry on a customer.                                                                                                             |
| [Finance](rosetta.md#finance)               | Collection of methods to retrieve finance information and perform calculations.                                                                                     |
| [Management](rosetta.md#management)         | Methods that allow the management of the finance proposal through out the sales process.                                                                            |
| [Operations](rosetta.md#operations)         | Methods to send finance proposal to lenders, retrieve decisions and documents.                                                                                      |
| [Validation](rosetta.md#validation)         | Base validate the finance application and also individual finance lender validation, includes finance validation.                                                   |
| [Vehicle Data](rosetta.md#vehicle-data)     | Retrieve vehicle details from a registration lookup.                                                                                                                |

### Webhook service

We can provide a webhook service for passing lender decisions to 3rd party integrators.

HTTP requests can be sent to and endpoint of your choosing whenever a decision notification
is received from a lender, or when our auto-polling detects a status change.

The format is documented [here](webhook.md).

## For lenders wishing to receive proposals from DealTrak

### Rosetta proposal format

We are able to send proposals into your underwriting platform in a standardised XML format which requires very little development at your end.

[View Example Requests](lenders.md#example-requests)

[View Example Response](lenders.md#example-response)


### Returning decisions/documents

#### Send decision

The send decision service enables lenders to provide proposal status updates back into our systems.

[Send Decision](lenders.md#send-decision)

#### Send documents

The send documents service enables lenders to Click to Sign URLs and base 64 encoded documents into DealTrak. The documents are then available via the DealTrak UI.

[Send Document](lenders.md#send-document)

## For insurers

Insurers may integrate with our [standard insurance integration](standardInsurance.md)
