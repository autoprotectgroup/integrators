# Lender decision webhook

When a lender decision is received, either by way of a lender pushing a status change back
into DealTrak, or as a result of our automated polling, we can generate webhook event in the
form of an HTTP POST request to an endpoint of your choice.

We are not currently able to generate webhook requests for changes made to the proposal
itself.

Messages are sent as HTTP POST with a content type of `application/json` and without authentication.
The requests will be sent from `185.108.170.210` in UAT and from `35.176.76.105` and `35.176.87.84` in live.

## Example request body

```json
{
  "lenderProposalReference": "ABCD1234",
  "proposalReference": "101ff086-eeb5-5469-984a-1c12028af137",
  "lenderStatus": 3,
  "destinationId": 101,
  "errors": [
    "A bad thing"
  ],
  "notes": [
    "3 months bank statements required",
    "Parental guarantor required at same address"
  ],
  "figures": {
    "APR": 19.9,
    "Commission": 878.30,
    "Monthly Payment": 202.20,
    "Tier": "2C"
  },
  "note": "A singular lender note",
  "tasks": [
    {
      "id": "22594fd0-066f-498c-b695-312f6378c76f",
      "title": "Proofs required",
      "description": "Upload copies of the customer's bank statements",
      "status" : "incomplete",
      "time": "2024-11-11T14:38:17Z"
    }
  ],
  "questions": [
    {
      "id": "dbc763d9-543b-4f5d-b6e7-03bda7c1d1ea",
      "title": "Replacement loan",
      "type": "options",
      "description": "Will this loan replace any of the following existing agreements?",
      "options": {
        "8723647": "Agreement 8723647",
        "621537251": "Agreement 621537251"
      }
    }
  ],
  "originalStatus": "GUARANTOR_REQUIRED"
}
```

| Node                      | Description                                                                           |
|---------------------------|---------------------------------------------------------------------------------------|
| `lenderProposalReference` | The lender's reference for this application.                                          |
| `proposalReference`       | The reference supplied by a 3rd party when the proposal was loaded.                   |
| `lenderStatus`            | The numeric DealTrak [lender status](enumerations.md#destination-status).             |
| `destinationId`           | The DealTrak [lender ID](destinations.md).                                            |
| `errors[]`                | An array of error messages, typically only seen in status 8 (Error sending).          |
| `notes[]`                 | Notes typically contain payout conditions or advice.                                  | 
| `figures[]`               | A set of key/value pairs of figures returned from the lender, which may include text. |
| `note`                    | A single text note/status from the underwriters.                                      |
| `tasks[].id`              | A reference for a task that the lender requires to be completed.                      |
| `tasks[].title`           | The title of the task.                                                                |
| `tasks[].description`     | The detailed description of the task.                                                 |
| `tasks[].status`          | The task status (complete/incomplete).                                                |
| `tasks[].time`            | The timestamp indicating when the task was received.                                  |
| `questions[].id`          | A reference for a question which the lender needs a response.                         |
| `questions[].title`       | The question title.                                                                   |
| `questions[].type`        | The response type, options or text.                                                   |
| `questions[].description` | A detailed description of the lender question.                                        |
| `questions[].options`     | An array of multiple choice answers.                                                  |
| `originalStatus`          | The original lender status as received from the lender.                               |

## Figures

Lenders are able to return arbitrary key/value pairs which are displayed in the UI.
No formatting is applied, and "figures" are not necessarily numeric.

## Tasks

Some lenders require various tasks to be complete before the deal can be paid out.

Completing tasks is undertaken from the lender conditions window within the DealTrak web UI.
It is not currently possible to complete tasks via the API, although this is being considered.

## Questions

Lenders may require confirmation of specific aspects of the deal.

If this is necessary, lenders may return arbitrary questions, which may be text or multiple
choice, with their response.

Answering questions is undertaken from the lender conditions window within the DealTrak web UI.
It is not currently possible to answer questions via the API, although this is being considered.
