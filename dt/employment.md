# Employment Types

| ID  | Type                                            |
|-----|-------------------------------------------------|
|   1 | Accountant                                      |
|   2 | Administrator                                   |
|   3 | Air Traffic Controller                          |
|   4 | Analyst                                         |
|   5 | Architect                                       |
|   6 | Assistant Manager                               |
|   7 | Auditor                                         |
|   8 | Baggage Handler                                 |
|   9 | Baker                                           |
|  10 | Bank Staff                                      |
|  11 | Bar Worker                                      |
|  12 | Barber/Hairdresser                              |
|  13 | Book Keeper                                     |
|  14 | Bricklayer                                      |
|  15 | Building Engineer                               |
|  16 | Butcher                                         |
|  17 | Buyer                                           |
|  18 | Carpenter                                       |
|  19 | Cashier                                         |
|  20 | Chemist                                         |
|  21 | Cleaning Person                                 |
|  22 | Clergy                                          |
|  23 | Clerical Worker                                 |
|  24 | Commissioned Officer/Warrant Officer            |
|  25 | Computer Operator                               |
|  26 | Cook Service                                    |
|  27 | Creative                                        |
|  28 | Croupier                                        |
|  29 | Dental Assistant                                |
|  30 | Dentist                                         |
|  31 | Designer                                        |
|  32 | Director                                        |
|  33 | Doctor                                          |
|  34 | Driver                                          |
|  35 | Driving Instructor                              |
|  36 | Electrician                                     |
|  37 | Engineer                                        |
|  38 | Executive                                       |
|  39 | Factory and Outside Manager                     |
|  40 | Factory; worker production                      |
|  41 | Farmer                                          |
|  42 | Firefighter                                     |
|  43 | General Assistant                               |
|  44 | Glazier                                         |
|  45 | Golf Instructor                                 |
|  46 | Homemaker/Housewife                             |
|  47 | Hospital Attendant                              |
|  48 | Insurance Adjuster                              |
|  49 | Insurance Agent                                 |
|  50 | Interior Decorator                              |
|  51 | Jeweler                                         |
|  52 | Laborer                                         |
|  53 | Lawyer                                          |
|  54 | Lecturer                                        |
|  55 | Librarian                                       |
|  56 | Machine Operator                                |
|  57 | Maintenance Mechanic                            |
|  58 | Manager                                         |
|  59 | Mechanic                                        |
|  60 | Mechanical Engineer                             |
|  61 | Military; enlisted, non-comm'd, or rank unknown |
|  62 | Miscellaneous                                   |
|  63 | Nurse                                           |
|  64 | Nursery School Teacher                          |
|  65 | Office Manager                                  |
|  66 | Office Staff                                    |
|  67 | Optician                                        |
|  68 | Osteopath                                       |
|  69 | Outside Worker                                  |
|  70 | Packer                                          |
|  71 | Painter & Decorator                             |
|  72 | Partnership 4 Plus                              |
|  73 | Partnership up to 3                             |
|  74 | Personnel Consultant                            |
|  75 | Photographer                                    |
|  76 | Pilot                                           |
|  77 | Plumber                                         |
|  78 | Police                                          |
|  79 | Production Controller                           |
|  80 | Production; other                               |
|  81 | Professional; licensed                          |
|  82 | Professional; other                             |
|  83 | Receptionist                                    |
|  84 | Repairer/Installer production                   |
|  85 | Research Assistant                              |
|  86 | Restaurant Host/Hostess                         |
|  87 | Retired                                         |
|  88 | Roofer                                          |
|  89 | Sales                                           |
|  90 | Sales Representative                            |
|  91 | School Head Teacher                             |
|  92 | Secretary                                       |
|  93 | Security Guard                                  |
|  94 | Self-employed                                   |
|  95 | Semi-professional                               |
|  96 | Service Manager                                 |
|  97 | Service; food-handler                           |
|  98 | Service; other                                  |
|  99 | Ship Crew                                       |
| 100 | Social Worker                                   |
| 101 | Steel Worker                                    |
| 102 | Steward/Stewardess                              |
| 103 | Stock Broker                                    |
| 104 | Student                                         |
| 105 | Supervisor                                      |
| 106 | Surveyor                                        |
| 107 | Systems Analyst                                 |
| 108 | Teacher                                         |
| 109 | Technician                                      |
| 110 | Telephone Operator                              |
| 111 | Trades                                          |
| 112 | TV Repair                                       |
| 113 | Underwriter                                     |
| 114 | Unemployed with Income                          |
| 115 | Unemployed with no Income                       |
| 116 | Uniformed Civil Service                         |
| 117 | Uniformed Postal Service                        |
| 118 | Veterinarian                                    |
| 119 | Waiter/Waitress                                 |
| 120 | Warehouse Worker                                |
