# AutoProtect Group Integrators Guide


Welcome to the AutoProtect Group integrators guide. This collection of documentation includes:

- [The Navigate Platform](nav/README.md)
- [AutoProcess](ap/README.md)
- [DealTrak](dt/README.md)

## Getting started

Contact your account manager to obtain test credentials and identifiers.

## Staying up to date

If you would like to receive updates on changes and new functionality in our API library, enter your email address
below to be added to the integrators mailing list.

<form method="POST" action="https://response.pure360.com/interface/list.php">
<input type="hidden" name="accName" value="AutoProtectLimited"/>
<input type="hidden" name="listName" value="Integrator Email Database"/>
<input type="hidden" name="successUrl" value="NO-REDIRECT"/>
<input type="hidden" name="errorUrl" value=""/>
Your email: <input type="email" name="email"/>
<br/>
<br/>
<script src="https://www.google.com/recaptcha/api.js"></script>
<div class="g-recaptcha" data-sitekey="6Lda1BAUAAAAABeemGvQod8rVNQQUSM2y9pFK_gS"></div>
<br/>
<input type="submit" value="Subscribe" />
</form>                                                                                                                                                                      




_This version was published on {{ honkit.time | date('dddd Do MMMM YYYY h:mm a') }}_
